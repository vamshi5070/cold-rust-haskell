use crate::my_lib::*;
use crate::row::*;
use crossterm::Result;

use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

pub struct Document {
    text_rows: Vec<Row>,
    file_path: PathBuf,
}

fn get_lines(abs_path: &PathBuf) -> Vec<String> {
    let binding = std::fs::read_to_string(abs_path.clone()).expect("Unable to open file");
    let lines = binding
        .split("\n")
        .map(|x| x.into())
        .collect::<Vec<String>>();
    lines
}

impl Document {
    pub fn new(abs_path: PathBuf) -> Self {
        let mut rows = Vec::new();

        let file_extension = if let Some(file_extension) = abs_path.extension() {
            match file_extension.to_str().unwrap() {
                "c" => Language::C,
                "rs" => Language::C,
                _ => Language::Fundemental,
            }
        } else {
            Language::Fundemental
        };
        for row in get_lines(&abs_path) {
            rows.push(Row::new_with_lang(row, file_extension))
        }
        Self {
            text_rows: rows,
            file_path: abs_path.to_path_buf(),
        }
    }

    pub fn new_with_lines(rows: Vec<Row>, abs_path: PathBuf) -> Self {
        Self {
            text_rows: rows,
            file_path: abs_path.to_path_buf(),
        }
    }

    pub fn document_len(&self) -> usize {
        return self.text_rows.len();
    }

    pub fn get_row_at(&self, at: u16) -> &Row {
        &self.text_rows[at as usize]
    }

    pub fn insert_char(&mut self, ch: char, cursor: &mut Position) {
        let (pos_x, pos_y) = cursor.get_pos();
        self.text_rows[pos_y as usize].insert_char(pos_x as usize, ch);
        cursor.set_pos((pos_x + 1, pos_y));
    }

    pub fn backward_word_line(&mut self, cursor: &mut Position) {
        let cur_line = &self.text_rows[cursor.get_y() as usize];
        let len = self.cur_row_len(cursor);
        for (index, ch) in cur_line.get_chars().chars().rev().enumerate() {
            if !ch.is_alphabetic() && len as usize - index < cursor.get_x() as usize {
                cursor.set_x(len - index as u16);
                break;
            }
        }
    }

    pub fn operation_of_return_key(&mut self, cursor: &mut Position) {
        let (pos_x, pos_y) = cursor.get_pos();
        let splitted_row = self.text_rows[pos_y as usize].split_row(pos_x as usize);
        self.text_rows.insert((pos_y + 1).into(), splitted_row);

        cursor.set_pos((0, pos_y.saturating_add(1)))
    }

    pub fn paste_process(&mut self, cursor: &Position, copied: Vec<String>) {
        let (pos_x, pos_y) = cursor.get_pos();

        let rest_row = self.text_rows[pos_y as usize].split_row(pos_x as usize);

        let first_row = Row::new(copied[0].clone());
        self.text_rows[pos_y as usize].split_row(pos_x as usize);
        self.text_rows[pos_y as usize].add_row(&first_row);
        let mut current = 1;
        loop {
            if current >= copied.len() {
                break;
            }
            let cur_row = Row::new(copied[current].clone());
            self.text_rows.insert(pos_y as usize + current, cur_row);
            current += 1;
        }
        self.text_rows[pos_y as usize + current - 1].add_row(&rest_row.clone());
    }

    pub fn command_kill_line(&mut self, cursor: &Position) -> String {
        if self.text_rows.len() <= 1 {
            return String::from("");
        }

        let (pos_x, pos_y) = cursor.get_pos();
        if self.cur_row_len(cursor) == 0 {
            let ret_str = self.text_rows[pos_y as usize]
                .get_chars()
                .to_owned()
                .clone();
            self.text_rows.remove(pos_y as usize);
            return ret_str;
        }
        let ret_row = self.text_rows[pos_y as usize].split_row(pos_x as usize);
        return ret_row.get_chars().to_owned().clone();
    }

    pub fn handle_backspace(&mut self, cursor: &mut Position) -> char {
        let (pos_x, pos_y) = cursor.get_pos();

        if pos_x == 0 && pos_y > 0 && usize::from(pos_y) <= self.text_rows.len() {
            let current_row = self.text_rows.remove((pos_y) as usize);
            let prev_row = &mut self.text_rows[(pos_y - 1) as usize];
            let length = prev_row.len;

            prev_row.add_row(&current_row);
            cursor.set_pos((length.try_into().unwrap(), pos_y - 1));
            return '\n';
        } else {
            let current_row = &mut self.text_rows[pos_y as usize];
            let ch = current_row.backspace((pos_x - 1) as usize);
            cursor.set_pos((pos_x - 1, pos_y));
            return ch;
        }
    }

    pub fn update_syntax_none(&mut self) {
        for row in self.text_rows.iter_mut() {
            row.update_syntax();
        }
    }

    // current row length
    pub fn cur_row_len(&self, cursor: &Position) -> u16 {
        let total_len = self.text_rows.len();
        let (_, pos_y) = cursor.get_pos();
        if !cursor.above(total_len) {
            0
        } else {
            self.text_rows[pos_y as usize].len as u16
        }
    }

    // prev row length
    pub fn prev_row_len(&self, cursor: &Position) -> u16 {
        let total_len = self.text_rows.len();
        let pos_y = cursor.get_y();
        if !cursor.above(total_len) {
            0
        } else {
            self.text_rows[pos_y.saturating_sub(1) as usize].len as u16
        }
    }

    // next row length
    pub fn next_row_len(&self, cursor: &Position) -> u16 {
        let total_len = self.text_rows.len();
        let (_, pos_y) = cursor.get_pos();
        if !cursor.above(total_len) {
            0
        } else if pos_y.saturating_add(1) as usize == total_len {
            0
        } else {
            self.text_rows[pos_y.saturating_add(1) as usize].len as u16
        }
    }

    pub fn write_to_file_autosave(&mut self) -> Result<()> {
        if self.file_path.extension().unwrap() == "swp" {
            return self.write_to_file();
        }
        let str_file = self.file_path.to_str().unwrap();
        let file_path_swp = str_file.to_owned() + ".swp";
        // l
        let mut file = File::create(file_path_swp)?;
        for (line_num, line) in self.text_rows.iter().enumerate() {
            if line_num != self.document_len() - 1 {
                file.write_all(line.get_string().as_bytes())?;
                file.write_all(b"\n")?;
            } else {
                file.write_all(line.get_string().as_bytes())?;
            }
        }
        Ok(())
    }

    pub fn write_to_file(&mut self) -> Result<()> {
        let mut file = File::create(&self.file_path)?;
        for (line_num, line) in self.text_rows.iter().enumerate() {
            if line_num != self.document_len() - 1 {
                file.write_all(line.get_string().as_bytes())?;
                file.write_all(b"\n")?;
            } else {
                file.write_all(line.get_string().as_bytes())?;
            }
        }
        Ok(())
    }

    pub fn get_text_rows_imm_ref(&self) -> &[Row] {
        return &self.text_rows;
    }

    pub fn forward_word_line(&mut self, cursor: &mut Position) {
        // if cursor.get_y() as usize >= self.text_rows.len() {
        //     return;
        // }

        let cur_x = cursor.get_x() as usize;

        let cur_line = &self.text_rows[cursor.get_y() as usize];
        let cur_line_remaining = &cur_line.get_chars()[cur_x..];
        if cur_line_remaining.is_empty() {
            return;
        }

        let mut current = 0;
        let cur_char = cur_line_remaining.chars().nth(0).unwrap();

        if cur_char == ' ' {
            for (_index, ch) in cur_line_remaining.chars().enumerate() {
                if ch.is_alphabetic() {
                    break;
                }
                current += 1;
            }
        } else {
            for (_index, ch) in cur_line_remaining.chars().enumerate() {
                if !ch.is_alphabetic() {
                    break;
                }
                current += 1;
            }
        }

        if current == 0 {
            current = 1;
        }

        cursor.set_x(cur_x as u16 + current);
    }

    pub fn viewport(&mut self, start: usize, height: usize) -> String {
        let min_height = std::cmp::min(start + height, self.text_rows.len());
        let row_range = &self.text_rows[start..min_height];
        let mut ret_str = String::new();
        for row in row_range {
            ret_str.push_str(row.get_chars());
        }
        return ret_str;
    }

    pub fn get_as_strings(&self) -> Vec<String> {
        let strings: Vec<String> = self
            .text_rows
            .iter()
            .map(|row| row.get_chars().to_string())
            .collect();
        strings
    }

    pub fn forward_delete_word(&mut self, cursor: &Position) -> String {
        let pos_x = cursor.get_x() as usize;
        let pos_y = cursor.get_y() as usize;
        let cur_line = &mut self.text_rows[pos_y as usize];
        let cur_line_slice = &cur_line.get_chars()[pos_x..];
        let mut current = 0;
        for (_index, ch) in cur_line_slice.chars().enumerate() {
            // cur_line.get_chars().chars().enumerate() {
            if !ch.is_alphabetic() {
                // || index > pos_x as usize {
                break;
            }
            current += 1;
        }
        // if cur_line.get_chars().is_empty() {
        //     self.command_kill_line(cursor);
        //     return "".to_string();
        // }
        cur_line.remove_range(pos_x, pos_x + current)
    }

    pub fn remove_range_delete(
        &mut self,
        start_cursor: &Position,
        end_cursor: &Position,
        current_cursor: &mut Position,
    ) -> Vec<String> {
        let mut mod_last_row = false;
        let start_x = start_cursor.get_x() as usize;
        let start_y = start_cursor.get_y() as usize;
        let end_x = end_cursor.get_x() as usize;
        let end_y = end_cursor.get_y() as usize;

        let mut vec_str = Vec::new();
        let mut current_y; // = 0;
        let to_be_deleted_index_y; // = start_y;

        if start_y == end_y {
            let cur_line = &mut self.text_rows[start_y];
            current_cursor.set_pos((start_x as u16, start_y as u16));
            vec_str.push(cur_line.remove_range(start_x, end_x));
        } else {
            if start_x == 0 {
                vec_str.push(self.text_rows[start_y].get_string());
                to_be_deleted_index_y = start_y;
                self.text_rows.remove(start_y);
            } else {
                // let cur_row = &mut self.text_rows[start_y];
                let cur_row_len = self.text_rows[start_y].len;
                mod_last_row = true;
                vec_str.push(self.text_rows[start_y].remove_range(start_x, cur_row_len));
                to_be_deleted_index_y = start_y + 1;
            }
            current_y = start_y + 1;

            loop {
                if current_y >= end_y || self.text_rows.len() == 1 {
                    break;
                }

                let mid_temp_row = &self.text_rows[to_be_deleted_index_y];

                vec_str.push(mid_temp_row.get_chars().to_owned().clone());
                self.text_rows.remove(to_be_deleted_index_y);
                current_y += 1;
            }
            let end_row = if self.text_rows.len() == 1 {
                &mut self.text_rows[start_y]
            } else {
                &mut self.text_rows[to_be_deleted_index_y]
            };
            vec_str.push(end_row.remove_range(0, end_x));
        }
        if !mod_last_row {
            current_cursor.set_pos((start_x as u16, start_y as u16));
        } else {
            let new_row = self.text_rows[start_y + 1].clone();
            self.text_rows[start_y].add_row(&new_row);
            self.text_rows.remove(start_y + 1);
            // let (left, right) = self.text_rows.split_at_mut(start_y + 1);
            // let end_y_index = end_y - start_y - 1;
            // left[start_y].add_row(&right[end_y_index]);
            // self.text_rows.remove(end_y);
            current_cursor.set_pos((start_x as u16, start_y as u16));
        }
        return vec_str;
    }

    pub fn get_row_range(&self, start: usize, end: usize) -> &[Row] {
        return &self.text_rows[start..=end];
    }
}

#[cfg(test)]
mod tests {
    macro_rules! create_vec_row{
        ($($str:expr),*) => {
            {
                let mut v = Vec::new();
                $(v.push(Row::new_str_ref($str));)*
                    v
            }
        };
        }

    use super::*;
    // use crate::search::*;
    #[test]
    fn test_read_file() {
        let fresh_path: PathBuf =
            "/home/vamshi/projects/rust/cold-rust-haskell/test-src/test-save.rs".into();
        let vectors = get_lines(&fresh_path);
        assert_eq!("", vectors[0]);
    }

    #[test]
    fn test_remove_range_inline() {
        let dummy_path = ".".into();

        let vec_row = create_vec_row!("emacs is cool", "", "", "emacs is cool");
        // let cursor = Position::new((13, 0));
        let start_cursor = Position::new((0, 0));
        let end_cursor = Position::new((13, 0));
        let mut current_cursor = Position::new((0, 0));

        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.remove_range_delete(&start_cursor, &end_cursor, &mut current_cursor);
        let lhs = create_vec_row!("", "", "", "emacs is cool");
        assert_eq!(lhs, document.text_rows);
    }

    #[test]
    fn test_remove_range_whole_buffer() {
        let dummy_path = ".".into();

        let vec_row = create_vec_row!(
            "emacs is cool",             // 0
            "xmonad is good",            // 1
            "nixos is best os",          // 2
            "emacs is cool",             // 3
            "haskell is life, eternity", // 4
            "emacs is pascal"            // 5
        );

        let start_cursor = Position::new((0, 0));
        let end_cursor = Position::new((0, 5));
        let mut current_cursor = Position::new((0, 0));

        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.remove_range_delete(&start_cursor, &end_cursor, &mut current_cursor);

        let lhs = create_vec_row!("emacs is pascal");

        // for i in 0..lhs.len() {
        assert_eq!(lhs, document.text_rows);
        // }

        assert_eq!(current_cursor, Position::new((0, 0)));
    }

    #[test]
    fn test_remove_range() {
        let dummy_path = ".".into();

        let vec_row = create_vec_row!(
            "emacs is cool",
            "xmonad is good",
            "nixos is best os",
            "emacs is cool",
            "haskell is life, eternity"
        );

        let start_cursor = Position::new((6, 0));
        let end_cursor = Position::new((6, 1));
        let mut current_cursor = Position::new((6, 1));

        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.remove_range_delete(&start_cursor, &end_cursor, &mut current_cursor);

        let lhs = create_vec_row!(
            "emacs  is good",
            "nixos is best os",
            "emacs is cool",
            "haskell is life, eternity"
        );

        // for i in 0..lhs.len() {
        assert_eq!(lhs, document.text_rows);
        // }

        assert_eq!(current_cursor, Position::new((6, 0)));
    }

    #[test]
    fn test_remove_empty_rows_range() {
        let dummy_path = ".".into();

        let vec_row = create_vec_row!(
            "emacs is cool",             // 0
            "",                          // 1
            "",                          // 2
            "xmonad is good",            // 3 // 1
            "nixos is best os",          // 4 // 2
            "cold is cool",              // 5 // 3
            "haskell is life, eternity"  //6
        );

        let start_cursor = Position::new((6, 0));
        let end_cursor = Position::new((6, 3));
        let mut current_cursor = Position::new((6, 3));

        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.remove_range_delete(&start_cursor, &end_cursor, &mut current_cursor);

        let lhs = create_vec_row!(
            "emacs  is good",
            "nixos is best os",
            "cold is cool",
            "haskell is life, eternity"
        );

        assert_eq!(lhs, document.text_rows);
        // assert_eq!(current_cursor, Position::new((6, 0)));
    }

    #[test]
    fn test_remove_range_problem() {
        let dummy_path = ".".into();
        let vec_row = create_vec_row!(
            "        macro_rules! create_vec{",                // 0
            "        ($($str:expr),*) => {",                   // 1
            "            {",                                   // 2
            "                let mut v = Vec::new();",         // 3
            "                $(v.push(String::from($str));)*", // 4
            "                    v",                           // 5
            "            }",                                   // 6
            "        };",                                      // 7
            "    }"                                            // 8
        );
        let start_cursor = Position::new((5, 1));
        let end_cursor = Position::new((5, 6));
        let mut current_cursor = Position::new((5, 6));
        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.remove_range_delete(&start_cursor, &end_cursor, &mut current_cursor);
        let strings: Vec<String> = document.get_as_strings();
        let lhs: Vec<String> = vec![
            "        macro_rules! create_vec{".to_string(),
            "            }".to_string(),
            "        };".to_string(),
            "    }".to_string(),
        ];
        assert_eq!(lhs, strings);
        // for row in document.text_rows.iter() {
        // assert_eq!("", row.get_chars());
        // }
    }

    #[test]
    fn test_command_kill_line() {
        let dummy_path = ".".into();

        let vec_row = create_vec_row!("emacs is cool", "", "", "emacs is cool");
        let cursor = Position::default();
        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.command_kill_line(&cursor);
        let lhs = create_vec_row!("", "", "", "emacs is cool");
        assert_eq!(lhs, document.text_rows);
    }

    #[test]
    fn test_command_delete_word_forward() {
        let dummy_path = "/home/vamshi/cold-rust-haskell/test-src/".into();

        let vec_row = create_vec_row!("emacs is cool", "", "", "emacs is cool");
        let cursor = Position::default();
        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.forward_delete_word(&cursor);
        let lhs = create_vec_row!(" is cool", "", "", "emacs is cool");
        assert_eq!(lhs[0], document.text_rows[0]);
    }

    #[test]
    fn test_command_delete_word_forward_empty() {
        let dummy_path = "/home/vamshi/cold-rust-haskell/test-src/".into();

        let vec_row = create_vec_row!("", "", "", "emacs is cool");
        let cursor = Position::default();
        let mut document = Document::new_with_lines(vec_row, dummy_path);
        document.forward_delete_word(&cursor);
        let lhs = create_vec_row!("", "", "", "emacs is cool");
        assert_eq!(lhs.len(), document.text_rows.len());
    }

    #[test]
    fn test_command_save_file() {
        let fresh_path: PathBuf =
            "/home/vamshi/projects/rust/cold-rust-haskell/test-src/test-save.rs".into();

        let vec_row = create_vec_row!("", "", "emacs is cool");
        let cursor = Position::default();
        let mut document = Document::new_with_lines(vec_row, fresh_path.clone());
        //document.write_to_file().unwrap();

        //document.command_kill_line(&cursor);
        //document.command_kill_line(&cursor);

        //document.write_to_file().unwrap();

        let rhs_document = Document::new(fresh_path.clone());
        // for i in 0..document.get_text_rows_imm_ref().len() {
        //     assert_eq!(
        //         document.get_text_rows_imm_ref()[i],
        //         rhs_document.get_text_rows_imm_ref()[i]
        //     );
        // }
        assert_eq!(
            document.get_text_rows_imm_ref().len(),
            rhs_document.get_text_rows_imm_ref().len()
        );
        // let lhs = create_vec_row!(" is cool", "", "", "emacs is cool");
    }
}
//     fn test_search() {
//         let dummy_path = ".".into();
//         let row = Row::new("emacs is cool".to_string());
//         let mut document = Document::new_with_lines(vec![row], dummy_path);
//         let search_word = "emacs";
//         let positions = search(document.get_text_rows_imm_ref(), search_word);
//         document.update_search(search_word.len(), positions);

//         // let positions = Hashmap
//     }
// }
