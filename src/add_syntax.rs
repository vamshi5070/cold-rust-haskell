use crate::my_lib::Highlight;
use crate::my_lib::Highlight::*;

pub struct Syntax<'a> {
    source: &'a str,
    start: usize,
    current: usize,
    highlights: Vec<Highlight>,
    keywords: Vec<String>,
}

impl<'a> Syntax<'a> {
    pub fn new(input: &'a str) -> Self {
        let rust_keywords = vec![
            "and", "fn", "true", "false", "mut", "self", "while", "loop", "char", "if", "else",
            "let", "pub", "impl", "struct", "enum", "mod", "use", "for", "println", "break",
            "continue", "match",
        ];
        Self {
            source: input,
            start: 0,
            current: 0,
            highlights: vec![],
            keywords: rust_keywords
                .into_iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .to_vec(),
        }
    }

    fn peek(&self) -> Result<char, String> {
        if self.is_at_end() {
            Err(format!(
                "Unable to peek str:{} at {}",
                self.source, self.current
            ))
        } else {
            Ok(self.source.as_bytes()[self.current] as char)
        }
    }

    fn peek_prev(&self) -> Result<char, String> {
        if self.current <= 0 {
            Err(format!(
                "Unable to peek str:{} at {}",
                self.source, self.current
            ))
        } else {
            Ok(self.source.as_bytes()[self.current - 1] as char)
        }
    }

    // self.is_at_end() {
    fn peek_next(&self) -> Result<char, String> {
        if self.current + 1 >= self.source.len() {
            Err(format!(
                "Unable to peek str:{} at {}",
                self.source,
                self.current + 1
            ))
        } else {
            Ok(self.source.as_bytes()[self.current + 1] as char)
        }
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.source.len()
    }

    fn advance(&mut self) -> char {
        let ch = self.source.as_bytes()[self.current] as char;
        self.current += 1;
        ch
    }

    pub fn scan_tokens(&mut self) -> Result<Vec<Highlight>, String> {
        let mut errors = vec![];
        while !self.is_at_end() {
            self.start = self.current;
            match self.scan_token() {
                Ok(_) => (),
                Err(msg) => errors.push(msg),
            }
        }

        if errors.is_empty() {
            Ok(self.highlights.clone())
        } else {
            let mut joined = String::new();
            for error in errors.iter() {
                joined.push_str(&error);
                joined.push_str("\n");
            }
            return Err(joined);
        }
    }

    fn char_match(&mut self, ch: char) -> bool {
        if self.is_at_end() {
            return false;
        }
        if self.source.as_bytes()[self.current] as char == ch {
            self.current += 1;
            return true;
        } else {
            return false;
        }
    }

    fn scan_token(&mut self) -> Result<(), String> {
        let c = self.advance();
        match c {
            '(' => self.highlights.push(Highlight::NonAlphaNum),
            ')' => self.highlights.push(Highlight::NonAlphaNum),
            '[' => self.highlights.push(Highlight::NonAlphaNum),
            ']' => self.highlights.push(Highlight::NonAlphaNum),
            '{' => self.highlights.push(Highlight::NonAlphaNum),
            '}' => self.highlights.push(Highlight::NonAlphaNum),
            ',' => self.highlights.push(Highlight::NonAlphaNum),
            '.' => self.highlights.push(Highlight::NonAlphaNum),
            '-' => self.highlights.push(Highlight::NonAlphaNum),
            '+' => self.highlights.push(Highlight::NonAlphaNum),
            ';' => self.highlights.push(Highlight::NonAlphaNum),
            ':' => self.highlights.push(Highlight::NonAlphaNum),
            '*' => self.highlights.push(Highlight::NonAlphaNum),
            '!' => self.highlights.push(Highlight::NonAlphaNum),
            '=' => self.highlights.push(Highlight::NonAlphaNum),
            '<' => self.highlights.push(Highlight::NonAlphaNum),
            '>' => self.highlights.push(Highlight::NonAlphaNum),
            '/' => {
                if self.char_match('/') {
                    self.highlights.push(Highlight::Comment);
                    self.highlights.push(Highlight::Comment);
                    loop {
                        if self.is_at_end() {
                            break;
                        } else {
                            self.highlights.push(Highlight::Comment);
                            self.advance();
                        }
                    }
                } else {
                    self.highlights.push(Highlight::NonAlphaNum);
                }
            }
            ' ' | '\r' | '\t' | '\n' => self.highlights.push(Highlight::Normal),
            '"' => self.lex_string()?,
            '\'' => self.lex_quote()?,
            c => {
                if c.is_numeric() {
                    self.lex_number()?;
                } else if c.is_alphabetic() {
                    self.identifier()?;
                } else {
                    self.highlights.push(Highlight::Normal);
                }
            }
        }
        Ok(())
    }

    fn lex_quote(&mut self) -> Result<(), String> {
        // self.highlights.push(Highlight::HlString);
        // let already_backslash = false;
        loop {
            if self.is_at_end() {
                break;
                // return Ok(());
            }
            if self.peek()? == '\\' {
                if self.peek_next()? == '\'' {
                    // self.highlights.push(Highlight::HlString);
                    // self.highlights.push(Highlight::HlString);
                    self.advance();
                    self.advance();
                    continue;
                }
                if self.peek_next()? == '\\' {
                    // self.highlights.push(Highlight::HlString);
                    // self.highlights.push(Highlight::HlString);
                    self.advance();
                    self.advance();
                    continue;
                }
            }
            if self.peek()? == '\'' {
                // self.highlights.push(Highlight::HlString);
                self.advance();
                break;
                // return Ok(());
            }
            // self.highlights.push(Highlight::HlString);
            self.advance();
        }
        for _ in self.start..self.current {
            self.highlights.push(Highlight::HlString);
        }
        return Ok(());
    }

    fn lex_string(&mut self) -> Result<(), String> {
        self.highlights.push(Highlight::HlString);
        // let already_backslash = false;
        loop {
            if self.is_at_end() {
                return Ok(());
            }
            if self.peek()? == '\\' {
                if self.peek_next()? == '"' {
                    self.highlights.push(Highlight::HlString);
                    self.highlights.push(Highlight::HlString);
                    self.advance();
                    self.advance();
                    continue;
                }
            }
            if self.peek()? == '"' {
                self.highlights.push(Highlight::HlString);
                self.advance();
                return Ok(());
            }
            self.highlights.push(Highlight::HlString);
            self.advance();
        }
    }

    fn identifier(self: &mut Self) -> Result<(), String> {
        let is_type = self.peek_prev()?.is_uppercase();

        loop {
            if self.is_at_end() || !self.peek()?.is_alphanumeric() {
                break;
            } else {
                self.advance();
            }
        }

        let identifier = &self.source[self.start..self.current];

        let mut is_keyword = false;
        for keyword in self.keywords.clone() {
            if keyword == identifier {
                is_keyword = true;
            }
        }
        if is_type {
            for _ in 0..identifier.len() {
                self.highlights.push(Highlight::Types);
            }
        } else if is_keyword {
            for _ in 0..identifier.len() {
                self.highlights.push(Highlight::Keyword);
            }
        } else {
            for _ in 0..identifier.len() {
                self.highlights.push(Highlight::Normal);
            }
        }
        return Ok(());
    }

    // self.highlights.push(Highlight::Normal);
    // self.highlights.push(Highlight::Normal);
    // if let Some(keyword) = self.keywords.get(&identifier) {
    //     self.add_token(keyword.clone())
    // } else {
    // self.add_token_lit(
    //     TokenType::Identifier,
    //     Some(LiteralValue::IdentifierVal(identifier.to_string())),
    // )
    // }

    /*
                let ch = self.peek().unwrap();
               if ch == '"' {
                   self.highlights.push(HlString);
                   self.current += 1;
                   break;
               }
               if ch == '\\' {
                   self.current += 1;
                   self.highlights.push(HlString);
                   if self.peek().unwrap() == '"' {
                       self.current += 1;
                       self.highlights.push(HlString);
                       continue;
                   }
                   continue;
               }
               self.highlights.push(HlString);
               self.current += 1;
    */

    fn lex_number(&mut self) -> Result<(), String> {
        self.highlights.push(Highlight::Number);
        loop {
            if self.is_at_end() {
                break Ok(());
            }
            let ch = self.peek()?;
            if !ch.is_numeric() && ch != '.' {
                break Ok(());
            }
            self.highlights.push(Highlight::Number);
            self.current += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_number_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1234");
        let lhs = vec![Number, Number, Number, Number];
        let rhs = syntax.scan_tokens().unwrap();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_number_normal_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("34use");
        let lhs = vec![Number, Number, Keyword, Keyword, Keyword];
        let rhs = syntax.scan_tokens().unwrap();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_fractional_number_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1234.53");
        let lhs = vec![Number, Number, Number, Number, Number, Number, Number];
        let rhs = syntax.scan_tokens().unwrap();
        //get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_spaces_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1   ");
        let lhs = vec![Number, Normal, Normal, Normal];
        let rhs = syntax.scan_tokens().unwrap();
        // get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_paren_string_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("(\"\")");
        let lhs = vec![NonAlphaNum, HlString, HlString, NonAlphaNum];
        let rhs = syntax.scan_tokens().unwrap();
        //get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_quote_string_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("\'editor: \\\'emacs\\\'");

        // "editor: \"emacs\""
        let lhs = vec![
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString,
        ];
        let rhs = syntax.scan_tokens().unwrap();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_whitespace() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("   ");

        let lhs: Vec<Highlight> = vec![Normal, Normal, Normal];
        let rhs = syntax.scan_tokens().unwrap();

        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_backquotes_string_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("\"editor: \\\"emacs\\\"");

        // "editor: \"emacs\""
        let lhs = vec![
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString,
        ];
        let rhs = syntax.scan_tokens().unwrap();

        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_string_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("\"emacs\"");
        let lhs = vec![
            HlString, HlString, HlString, HlString, HlString, HlString, HlString,
        ];
        let rhs = syntax.scan_tokens().unwrap();
        //get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_comment_hl() {
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("//");
        let lhs = vec![Comment, Comment];
        let rhs = syntax.scan_tokens().unwrap();
        //get_hl_syntax();
        assert_eq!(lhs, rhs);
    }
}
