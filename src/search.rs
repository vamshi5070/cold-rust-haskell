// use crate::my_lib::*;
use crate::row::*;

pub fn search(
    text_rows: &[Row],
    searching_str: &str,
    // _cursor: &Position,
) -> Vec<(usize, Vec<usize>)> {
    let mut positions: Vec<(usize, Vec<usize>)> = Vec::new();

    if searching_str.is_empty() {
        return positions;
    }

    for (pos_y, row) in text_rows.iter().enumerate() {
        let row_string = row.render_str();
        if let Some(cur_row) = line_search(&row_string, searching_str) {
            positions.push((pos_y, cur_row));
        } else {
            continue;
        }
    }
    positions
}

fn line_search<'a>(refernce: &str, searching_str: &str) -> Option<Vec<usize>> {
    let mut main_string = refernce; //.clone();
    let mut positions = vec![];
    let search_len = searching_str.len();
    let mut prev = 0;
    loop {
        if main_string.is_empty() {
            break;
        }

        if searching_str.is_empty() {
            break;
        }

        if let Some(new_pos) = main_string.find(&searching_str) {
            positions.push(new_pos + prev);
            prev = new_pos + prev + search_len;
            main_string = &main_string[(new_pos + search_len)..];
        } else {
            break;
        }
    }
    if positions.is_empty() {
        None
    } else {
        Some(positions)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    // use std::collections::HashMap;

    #[test]
    fn line_search_test() {
        if let Some(result) = line_search("emacs is cool", "emacs") {
            assert_eq!(result, vec![0]);
        }
    }

    #[test]
    fn line_search_test_2() {
        if let Some(result) = line_search("emacs is emacs", "emacs") {
            assert_eq!(result, vec![0, 9]);
        }
    }

    #[test]
    fn line_search_test_empty() {
        let result = line_search("", "xmonad");
        assert_eq!(result, None);
    }

    #[test]
    fn line_search_test_one_len() {
        if let Some(result) = line_search("1 2 3 1 1 1 ", "1") {
            assert_eq!(result, vec![0, 6, 8, 10]);
        }
    }

    #[test]
    fn search_test_row() {
        let row1 = Row::new("1 2 3 1 1 1 ".to_string());
        let row2 = Row::new("1emacs is 1emacs".to_string());
        let vec_row = vec![row1, row2];
        let lhs = search(&vec_row, "1");
        // , &Position::new((0, 0)));
        // let mut rhs: HashMap<usize, Vec<usize>> = HashMap::new();
        let mut rhs = Vec::new();
        rhs.push((0, vec![0, 6, 8, 10]));
        rhs.push((1, vec![0, 10]));
        assert_eq!(lhs, rhs);
    }
}
