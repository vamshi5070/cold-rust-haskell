use crate::my_lib::*;
use crossterm::{
    event::{read, Event, Event::*, KeyEvent},
    Result,
};
pub struct Keyboard;

impl Keyboard {
    pub fn read(&self) -> StdResult<KeyEvent, ResultEditor> {
        let c: Result<Event> = read();

        loop {
            match c {
                Ok(Key(event)) => {
                    return Ok(event);
                }
                _ => {
                    return Err(ResultEditor::KeyReadFail);
                }
            }
        }
    }
}
