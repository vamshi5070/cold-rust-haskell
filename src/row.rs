use crate::add_syntax::Syntax;
use crate::my_lib::*;
use std::fmt;

#[derive(Default, Clone, PartialEq)]
pub struct Row {
    string: String,
    pub len: usize,
    pub hl: Vec<Highlight>,
    theme: Theme,
    major_mode: Language,
}

impl fmt::Debug for Row {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.string)
    }
}

impl Row {
    pub fn new(line: String) -> Self {
        let mut result = Self {
            string: line.clone(),
            len: line.len(),
            hl: Vec::new(),
            theme: Theme,
            major_mode: Language::C,
        };

        result.update_syntax();
        result
    }

    pub fn new_str_ref(line: &str) -> Self {
        let mut result = Self {
            string: line.to_owned().clone(),
            len: line.len(),
            hl: Vec::new(),
            theme: Theme,
            major_mode: Language::C,
        };

        result.update_syntax();
        result
    }

    pub fn update_render_row(&mut self) {
        let mut rend_str = String::new();
        // let mut tabs_count: usize = 0;
        // for ch in self.string.chars() {
        //     if ch == '\t' {
        //         tabs_count += 1;
        //     }
        // }

        for ch in self.string.chars() {
            if ch == '\t' {
                rend_str.push(' ');
                let mut idx = 1;
                loop {
                    if idx % 8 == 0 {
                        break;
                    }
                    rend_str.push(' ');
                    idx += 1;
                }
            } else {
                rend_str.push(ch);
            }
        }

        self.string = rend_str.clone();
        self.len = rend_str.len();
    }

    pub fn new_with_lang(line: String, lang: Language) -> Self {
        let mut result = Self {
            string: line.clone(),
            len: line.len(),
            hl: Vec::new(),
            theme: Theme,
            major_mode: lang,
        };

        result.update_syntax();
        result
    }

    pub fn get_string(&self) -> String {
        self.string.clone()
    }

    pub fn get_chars(&self) -> &str {
        &self.string
    }

    pub fn get_hl(&self, at: usize) -> Option<Highlight> {
        self.hl.get(at).clone().cloned()
    }

    pub fn insert_char(&mut self, at: usize, c: char) {
        if at >= self.string.len() {
            self.string.push(c);
        } else {
            self.string.insert(at, c);
        }
        self.len = self.len + 1;
        self.update_syntax();
    }

    pub fn render_str(&self) -> String {
        let mut result = String::new();
        for ch in self.string.chars() {
            result.push(ch);
        }
        result
    }

    pub fn split_row(&mut self, at: usize) -> Self {
        let current_row = self.string.clone();
        let lang = self.major_mode.clone();
        let position_to_split = at;
        self.string = (&current_row[0..position_to_split]).to_string();
        self.len = self.string.len();
        self.update_syntax();
        let second_part = &current_row[position_to_split..];
        Row::new_with_lang(second_part.to_string(), lang)
    }

    pub fn remove_range(&mut self, start: usize, end: usize) -> String {
        let removed: String = self.string.drain(start..end).collect();
        self.len -= end - start;
        self.update_syntax();
        return removed;
    }

    pub fn backspace(&mut self, at: usize) -> char {
        let current_row = self.string.clone();
        let ch = current_row.chars().nth(at).unwrap();
        let index = at;
        let mut result = String::with_capacity(self.len - 1);
        if index > 0 {
            result.push_str(&current_row[0..index]);
        }

        if index < self.len - 1 {
            result.push_str(&current_row[index + 1..]);
        }
        self.string = result.clone();
        self.len = result.len();
        self.update_syntax();
        return ch;
    }

    pub fn add_row(&mut self, new_row: &Row) {
        let new_str = &new_row.string;
        self.string.push_str(&new_str);
        self.len = self.string.len();
        self.update_syntax();
    }

    fn update_dired(&mut self) {
        if self.string.ends_with("/") {
            for _ in self.string.chars() {
                self.hl.push(Highlight::Directory)
            }
        } else {
            for _ in self.string.chars() {
                self.hl.push(Highlight::Normal)
            }
        }
    }

    fn update_c_or_rust(&mut self) {
        self.hl = Syntax::new(&self.string).scan_tokens().unwrap();
    }

    /*
    fn update_c_or_rust_treesitter(&mut self) {
        // _maybe_locations: Option<(Vec<usize>, usize)>) {
        // fn update_c_or_rust(&mut self, _maybe_locations: Option<(Vec<usize>, usize)>) {
        // let default_style = Style::default();
        // return;
        // self.styles = Vec::new();
        if self.len == 0 {
            return;
        }
        let mut new_highlight = Highlighter::new(Theme);

        let style_infos = new_highlight.highlights(self.get_chars());

        let mut prev_end: usize = 0;

        for style_i in &style_infos {
            if prev_end == style_i.end {
                continue;
            }

            for _ in prev_end..style_i.start {
                self.styles.push(self.theme.default());
            }

            for _ in style_i.start..style_i.end {
                self.styles.push(style_i.style.clone());
            }
            prev_end = style_i.end;
        }

        for _ in self.styles.len()..self.len {
            self.styles.push(self.theme.default());
        }
    }
     */

    pub fn update_syntax(&mut self) {
        // let default_style = self.theme.default();
        self.update_render_row();
        match self.major_mode {
            Language::C => self.update_c_or_rust(),
            Language::Rust => self.update_c_or_rust(),
            Language::Dired => self.update_dired(),
            Language::Fundemental => {
                for _ in self.string.chars() {
                    self.hl.push(Highlight::Normal)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn empty_row_styles() {
        let new_row = Row::new_str_ref("");
        // new
        assert_eq!(new_row.hl, vec![]);
    }

    #[test]
    fn test_remove_range() {
        let mut new_row = Row::new_str_ref("emacs is cool");
        let removed_str = new_row.remove_range(4, 7);

        assert_eq!("s i", removed_str);
    }
}
