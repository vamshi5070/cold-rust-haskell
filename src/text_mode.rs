use crate::display_window::PassDisplay;
use crate::document::Document;
// use crate::highlight::*;
use crate::my_lib::*;
use crate::row::*;
// use crate::my::*;
use crate::screen::Screen;
use crossterm::Result;
use std::path::PathBuf;

pub struct TextMode {
    cursor: Position,
    document: Document,
    pub file_path: PathBuf,
    pub recent_change: Change,
    // rowoff: u16,
    // coloff: u16,
    dirty: bool,
    comb_key: Option<Key>,
    is_focus: bool,
    pub highlight: Option<(Position, Position)>,
    pub search_locations: Option<(Vec<(usize, Vec<usize>)>, usize)>,
    pub message: MessageStatus,
    pending: Option<Status>,
    minibuffer_input: String,
    lang: Language,
    editor_op: Vec<EditorOperation>,
    copied: Option<Vec<String>>,
    is_mark_set: bool,
}

impl TextMode {
    pub fn new_with_lines(rows: Vec<Row>, abs_path: &PathBuf) -> Self {
        Self {
            cursor: Position::default(),
            document: Document::new_with_lines(rows, abs_path.to_path_buf()),
            recent_change: Change::First,
            // rowoff: 0,
            // coloff: 0,
            dirty: false,
            comb_key: None,
            is_focus: true,
            highlight: None,
            file_path: abs_path.to_path_buf(),
            message: MessageStatus::NoChange,
            minibuffer_input: String::from(""),
            pending: None,
            lang: Language::C,
            editor_op: Vec::new(),
            search_locations: None,
            copied: None,
            is_mark_set: false,
        }
    }

    pub fn get_file_path(&self) -> &PathBuf {
        &self.file_path
    }

    pub fn get_row_at(&self, at: u16) -> &Row {
        return self.document.get_row_at(at);
    }

    // pub fn get_text_rows(&self) -> &[Row] {
    // return self.document.text_rows;
    // }

    pub fn new_with_file(abs_path: PathBuf) -> Self {
        Self {
            cursor: Position::default(),
            document: Document::new(abs_path.clone()),
            recent_change: Change::First,
            // rowoff: 0,
            // coloff: 0,
            dirty: false,
            comb_key: None,
            is_focus: true,
            highlight: None,
            file_path: abs_path,
            message: MessageStatus::NoChange,
            minibuffer_input: String::from(""),
            pending: None,
            lang: Language::C,
            editor_op: Vec::new(),
            search_locations: None,
            copied: None,
            is_mark_set: false,
        }
    }

    pub fn get_help(&self) -> Result<()> {
        return Ok(());
    }

    pub fn get_lang_ext(&self) -> String {
        self.lang.show().to_string()
    }

    pub fn set_minibuffer_input(&mut self, input: String) {
        self.minibuffer_input = input;
    }

    pub fn get_text_rows_imm_ref(&self) -> &[Row] {
        return self.document.get_text_rows_imm_ref();
    }

    pub fn adjust_search_cursor(&mut self, input: String, positions: Vec<(usize, Vec<usize>)>) {
        if input.is_empty() {
            self.search_locations = None;
            return;
        }

        if positions.is_empty() {
            self.pending = None;
            self.message = MessageStatus::DrawMessage(String::from("No result found"));
            self.search_locations = None;
        } else {
            self.search_locations = Some((positions, input.len()));
        }
    }

    // pub fn accept_input(&mut self, input: String) -> Windows {
    //     self.minibuffer_input = input;
    //     match self.pending {
    //         Some(Status::Search) => {
    //             let positions: Vec<usize, Vec<usize>> = search(
    //                 &self.document.get_text_rows_imm_ref(),
    //                 &self.minibuffer_input,
    //                 // &self.cursor,
    //             );

    //             if positions.is_empty() {
    //                 self.pending = None;
    //                 self.message = MessageStatus::DrawMessage(String::from("No result found"));
    //                 return Windows::Current;
    //             } else {
    //                 // update syntax for match
    //                 // self.document
    //                 // .update_search(self.minibuffer_input.len());
    //                 // , positions);
    //                 return Windows::Current;
    //             }
    //         }
    //         // Some(Status::FileSave) => {
    //         //     if self.minibuffer_input == "y" {
    //         //         self.exit()
    //         //     }
    //         // }
    //         None => {
    //             return Windows::Current;
    //         } // Some(Status::FileSave) => {
    //           //     if self.minibuffer_input == "y" {
    //           //         Windows::Exit
    //           //     } else if self.minibuffer_input == "n" {
    //           //         self.message = MessageStatus::ClearMessage;
    //           //         self.pending = None;
    //           //         Windows::Current
    //           //     } else {
    //           //         // self.minibuffer_input = String::from("");
    //           //         self.message = MessageStatus::ClearMessage;
    //           //         self.pending = Some(Status::FileSave);
    //           //         Windows::Minibuffer(MiniResult::Prompt(vec![
    //           //             "Are you sure, Quit without saving(y or n)(n): ".to_string(),
    //           //         ]))
    //           //
    //           // self.process_ctrl_x_keypress(&Key::Ctrl(RegularKey::Char('c')))
    //           // }
    //           // } //Windows::Current,
    //     }
    // }

    pub fn get_focus(&self) -> bool {
        self.is_focus
    }

    pub fn set_status_none(&mut self) {
        self.pending = None;
    }

    pub fn set_position(&mut self) {
        self.cursor.set_pos((0, 0));
    }

    pub fn set_focus(&mut self) {
        self.is_focus = true;
    }

    pub fn set_focus_not(&mut self) {
        self.is_focus = false;
    }

    pub fn doc_len(&self) -> usize {
        return self.document.document_len();
    }

    pub fn get_doc_range(&self, start: usize, end: usize) -> &[Row] {
        return self.document.get_row_range(start, end);
    }

    pub fn display_status_bar(
        &self,
        screen: &mut Screen,
        pass_display: &PassDisplay,
    ) -> Result<()> {
        let status_cursor = pass_display.origin.get_y() + pass_display.height;
        let std_msg = format!(
            "{} ({},{}) {} {}",
            if self.highlight.is_some() {
                "<hl>"
            } else {
                "<nm>"
            },
            self.cursor.get_y(),
            self.cursor.get_x(),
            if self.dirty { "[+]" } else { "" },
            self.file_path.to_str().unwrap(),
        );
        screen.draw_status_bar_vertical(
            pass_display.origin.get_x(),
            status_cursor,
            pass_display.width,
        )?;
        screen.draw_on_status_vertical(&std_msg, status_cursor, pass_display.origin.get_x())
    }

    pub fn get_status_bar_content(
        &self,
        // screen: &mut Screen,
        // pass_display: &PassDisplay,
    ) -> String {
        // let status_cursor = pass_display.origin.get_y() + pass_display.height;
        let std_msg = format!(
            "{} ({},{}) {} {}",
            if self.highlight.is_some() {
                "<hl>"
            } else {
                "<nm>"
            },
            self.cursor.get_y(),
            self.cursor.get_x(),
            if self.dirty { "[+]" } else { "" },
            self.file_path.to_str().unwrap(),
        );
        // screen.draw_status_bar_vertical(
        // pass_display.origin.get_x(),
        // status_cursor,
        // pass_display.width,
        // )?;
        // screen.draw_on_status_vertical(&std_msg, status_cursor, pass_display.origin.get_x())
        return std_msg;
    }

    // pub fn draw_cursor(&mut self, _screen: &mut Screen, _pass_display: &PassDisplay) -> Result<()> {
    // Ok(())
    //     self.rowoff = pass_display.rowoff;
    //     self.coloff = pass_display.coloff;
    //     self.cursor = pass_display.cursor;

    //     let (pos_x, pos_y) = self.cursor.get_pos();
    //     let (og_x, og_y) = pass_display.origin.get_pos();

    //     let new_pos_y = og_y + pos_y - self.rowoff;
    //     let new_pos_x = og_x + pos_x - self.coloff;
    //     let custom_pos = Position::new((new_pos_x, new_pos_y));

    //     screen.render_cursor(&custom_pos)?;
    //     screen.flush()
    // }

    pub fn clear_message(&mut self) {
        self.message = MessageStatus::DrawMessage(String::from(""));
    }

    pub fn backward_word(&mut self) {
        // if self.cursor.get_y() as usize <= 0 {
        // self.document.document_len() {
        // return;
        // }

        let prev_x = self.cursor.get_x();
        self.document.backward_word_line(&mut self.cursor);
        if prev_x == self.cursor.get_x() {
            self.handle_arrow_keys(&RegularKey::Left);
            // if self.cursor.get_y() != 0 && self.cursor.get_x() == 0 {
            //     self.handle_arrow_keys(&RegularKey::Up);
            //     self.cursor.set_x(self.document.cur_row_len(&self.cursor));
            // } else {
            //     self.cursor.set_x(0);
            // }
        }
    }

    pub fn forward_word(&mut self) {
        // if self.cursor.get_y() as usize >= self.document.document_len() - 1 {
        // return;
        // }
        let prev_x = self.cursor.get_x();
        self.document.forward_word_line(&mut self.cursor);
        if prev_x == self.cursor.get_x() {
            self.handle_arrow_keys(&RegularKey::Right);
            // self.cursor.set_x(0);
            // self.cursor.set_y(self.cursor.get_y() + 1);
        }
    }

    pub fn go_2_prev_nearest_search(&mut self) {
        if let Some((loc, _)) = &self.search_locations {
            let mut current = loc.len();
            loop {
                if current <= 0 {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current - 1];
                if self.cursor.get_y() > *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[pos_xs.len() - 1] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs.into_iter().rev() {
                        if self.cursor.get_x() > *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current -= 1;
            }
        }
    }

    pub fn go_2_next_nearest_search(&mut self) {
        if let Some((loc, _)) = &self.search_locations {
            let mut current = 0;
            loop {
                if current >= loc.len() {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current];
                if self.cursor.get_y() < *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[0] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs {
                        if self.cursor.get_x() < *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current += 1;
            }
        }
    }

    pub fn go_2_current_or_next_nearest_search(&mut self) {
        if let Some((loc, _size)) = &self.search_locations {
            let mut current = 0;
            loop {
                if current >= loc.len() {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current];
                if self.cursor.get_y() < *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[0] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs {
                        if self.cursor.get_x() == *pos_x as u16 {
                            return;
                        }
                        if self.cursor.get_x() < *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current += 1;
            }
        }
    }

    pub fn go_2_current_or_prev_nearest_search(&mut self) {
        if let Some((loc, _size)) = &self.search_locations {
            let mut current = loc.len();
            loop {
                if current <= 0 {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current - 1];
                if self.cursor.get_y() > *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[pos_xs.len() - 1] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs.into_iter().rev() {
                        if self.cursor.get_x() == *pos_x as u16 {
                            return;
                        }
                        if self.cursor.get_x() > *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current -= 1;
            }
        }
    }

    // pub fn get_text_str_slices(&self) -> &[str] {
    // return &[];
    // }
    /*
        pub fn display(
            &mut self,
            screen: &mut Screen,
            focus: bool,
            pass_display: &PassDisplay,
            highlight: &mut Highlighter,
            buffer: &mut RenderBuffer,
        ) -> Result<()> {
            self.is_focus = focus;
            self.rowoff = pass_display.rowoff;
            self.coloff = pass_display.coloff;
            self.cursor = pass_display.cursor;
            let default_style = highlight.theme.default();

            let start = self.rowoff as usize;
            let end = if (self.rowoff + pass_display.height - 1) as usize
                > self.document.document_len() - 1
            {
                self.document.document_len() - 1
            } else {
                (self.rowoff + pass_display.height - 1) as usize
            };

            let mut x = 0;
            let mut y = 0;
            let my_str = self.document.viewport(start, end - start);
            let row_compressed = self.document.get_row_range(start, end);
            let style_infos = highlight.highlights(&my_str);
            let mut style_index = 0;
            let mut row_index = 0;
            let mut prev_end = 0;
            for (row_index, row) in row_compressed.iter().enumerate() {
                buffer.set_text(0, row_index, row.get_chars(), &default_style);
                let fill_space_str =
                    " ".repeat(pass_display.width.saturating_sub(row.len as u16) as usize);
                buffer.set_text(row.len, row_index, &fill_space_str, &default_style);
            }
            /*
                loop {
                    if row_index >= row_compressed.len() {
                        break;
                    }
                    if style_index >= style_infos.len() {
                        break;
                    }
                    let style_i = style_infos[style_index].clone();
                    if prev_end == style_i.end {
                        continue;
                    }
                    let row_len = row_compressed[row_index].len;
                    if style_i.end >= row_len {
                        row_index += 1;
                        x = 0;
                        y += 1;
                        continue;
                    }
                    let row_str_slice = &row_compressed[row_index].get_string()[style_i.start..style_i.end];
                    buffer.set_text(x, y, row_str_slice, &style_i.style);
                    x += style_i.end - style_i.start;
                    style_index += 1;
            }
             */
            screen.clear_all()?;
            screen.render(buffer)?;
            // for row in row_compressed {
            // row.len
            // }
            // let mut iter = my_str.chars().enumerate().peekable();

            // while let Some((pos, c)) = iter.next() {
            //     if c == '\n' || iter.peek().is_none() {
            //         if c != '\n' {
            //             buffer.set_char(x as usize, y, c, &default_style);
            //             x += 1;
            //         }
            //     }
            // }
            // let

            // for
            // screen.render()
            // screen.draw_lines(
            //     self.document.get_row_range(start, end),
            //     pass_display,
            //     self.highlight,
            //     self.search_locations.clone(),
            // )?;
            self.display_status_bar(screen, pass_display)?;
            screen.flush().unwrap();
            return Ok(());
        }
    */

    pub fn is_empty_cursor_positions_search_status(&self) -> bool {
        self.search_locations.is_none()
    }

    pub fn keypress_handling(
        &mut self,
        key: &Key,
        comb_key: Option<Key>,
        cursor: Position,
    ) -> Windows {
        self.cursor = cursor;
        self.comb_key = comb_key;
        self.recent_change = Change::NoChange;
        match self.comb_key {
            Some(Key::Ctrl(RegularKey::Char(' '))) => Windows::Exit, //self.process_ctrl_space_keypress(key_event),

            Some(Key::Ctrl(RegularKey::Char('x'))) => self.process_ctrl_x_keypress(key),
            _ => self.process_keypress(key),
        }
    }

    pub fn get_pos_as_pos(&self) -> Position {
        self.cursor.get_pos_as_pos()
    }

    pub fn set_pos_as_pos(&mut self, cursor: Position) {
        self.cursor.set_pos_as_pos(cursor);
    }

    fn select_whole_buffer(&mut self) {
        let text_rows = self.document.get_text_rows_imm_ref();
        let start_cursor = Position::new((0, 0));
        let end_y = text_rows.len() as u16 - 1;
        let end_x = text_rows[end_y as usize].len as u16;
        let end_cursor = Position::new((end_x, end_y));
        self.highlight = Some((start_cursor, end_cursor));
    }

    pub fn handle_shift_arrow_keys(&mut self, reg_key: &RegularKey) {
        let current_cursor = self.cursor.get_pos_as_pos();
        self.handle_arrow_keys(reg_key);
        let after_cur_pos = self.cursor.get_pos_as_pos();
        self.highlight = match self.highlight {
            Some((start_hl, end_hl)) => {
                if current_cursor.is_equal(start_hl) {
                    let (first, last) = after_cur_pos.compare(end_hl);
                    Some((first, last))
                } else if current_cursor.is_equal(end_hl) {
                    let (first, last) = after_cur_pos.compare(start_hl);
                    Some((first, last))
                } else {
                    None
                }
            }
            None => match reg_key {
                RegularKey::Down | RegularKey::Right => Some((current_cursor, after_cur_pos)),
                RegularKey::Left | RegularKey::Up => Some((after_cur_pos, current_cursor)),
                _ => None,
            },
        };
        let (beg_cursor, end_cursor) = self.highlight.unwrap();
        if beg_cursor.is_equal(end_cursor) {
            self.highlight = None;
            self.is_mark_set = false;
        }
    }

    pub fn handle_arrow_keys(&mut self, reg_key: &RegularKey) {
        let rowlen = self.document.cur_row_len(&self.cursor);
        match reg_key {
            RegularKey::Up => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let prev_rowlen = self.document.prev_row_len(&self.cursor);
                if pos_x > prev_rowlen {
                    self.cursor.set_pos((prev_rowlen, pos_y.saturating_sub(1)))
                } else {
                    self.cursor.set_pos((pos_x, pos_y.saturating_sub(1)))
                }
            }
            RegularKey::Down => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let next_rowlen = self.document.next_row_len(&self.cursor);

                if usize::from(pos_y) >= self.document.document_len() - 1 {
                } else if pos_x > next_rowlen {
                    self.cursor.set_pos((next_rowlen, pos_y.saturating_add(1)))
                } else {
                    self.cursor.set_pos((pos_x, pos_y.saturating_add(1)))
                }
            }
            RegularKey::Right => {
                let (pos_x, pos_y) = self.cursor.get_pos();

                if pos_x >= rowlen {
                    if usize::from(pos_y) >= self.document.document_len() - 1 {
                    } else {
                        self.cursor.set_pos((0, pos_y + 1))
                    }
                } else {
                    self.cursor.set_pos((pos_x + 1, pos_y))
                }
            }
            RegularKey::Left => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let prev_rowlen = self.document.prev_row_len(&self.cursor);
                if pos_x == 0 {
                    if pos_y > 0 {
                        self.cursor.set_pos((prev_rowlen, pos_y.saturating_sub(1)))
                    } else {
                    }
                } else {
                    self.cursor.set_pos((pos_x - 1, pos_y))
                }
            }
            _ => (),
        }
    }

    pub fn get_message(&self) -> MessageStatus {
        self.message.clone()
    }

    fn _process_ctrl_space_keypress(&mut self, key: &Key) -> Windows {
        match key {
            Key::Ctrl(RegularKey::Char('f')) => {
                self.handle_shift_arrow_keys(&RegularKey::Right);
                Windows::Current
            }
            _ => Windows::Current,
        }
    }

    pub fn autosave(&mut self) -> Result<()> {
        self.document.write_to_file_autosave()
    }

    pub fn process_ctrl_x_keypress(&mut self, key: &Key) -> Windows {
        match key {
            // Key::Ctrl(RegularKey::Char('c')) => {
            //     if self.dirty == false {
            //         self.write_to_file().unwrap();
            //         Windows::Exit
            //     } else {
            //         self.pending = Some(Status::FileSave);
            //         Windows::Minibuffer(MiniResult::PromptWithKind {
            //             prompt: vec!["Are you sure, Quit without saving(y or n)(n): ".to_string()],
            //             kind: MinibufferType::Single,
            //         })
            //         // self.message = MessageStatus::DrawMessage("Saved".to_string());
            //         // Windows::Current
            //     }
            // }
            Key::Ctrl(RegularKey::Char('s')) => {
                self.message = MessageStatus::DrawMessage("Saved".to_string());
                if self.dirty == true {
                    self.document.write_to_file().unwrap();
                }
                self.dirty = false;
                self.comb_key = None;
                return Windows::Current;
            }

            Key::Normal(RegularKey::Char('h')) => {
                if self.message == MessageStatus::ClearMessage {
                    self.message = MessageStatus::NoChange;
                }
                self.select_whole_buffer();
                return Windows::Current;
            }

            Key::Ctrl(RegularKey::Char('o')) => {
                self.comb_key = None;
                self.highlight = None;
                self.search_locations = None;
                if self.message == MessageStatus::ClearMessage {
                    self.message = MessageStatus::NoChange;
                }

                // self.message = MessageStatus::ClearMessage;
                loop {
                    if self.cursor.get_y() >= self.document.get_text_rows_imm_ref().len() as u16
                        || self.document.cur_row_len(&self.cursor) != 0
                    {
                        break;
                    }
                    self.process_keypress(&Key::Ctrl(RegularKey::Char('k')));
                }
                self.dirty = true;
                Windows::Current
            }
            Key::Ctrl(RegularKey::Char('g')) => {
                self.comb_key = None;
                self.highlight = None;
                self.search_locations = None;
                self.message = MessageStatus::ClearMessage;
                Windows::Current
            }
            // Key::Ctrl(RegularKey::Char('f')) => {
            //     self.comb_key = None;
            //     self.pending = Some(Status::Search);
            //     self.is_focus = false;
            //     Windows::Minibuffer(MiniResult::PromptWithKind({
            //         prompt: vec!["Find something: ".to_string()],
            //         kind: MinibufferType::Multiple,
            //     }))
            // }
            Key::Normal(RegularKey::Char('j')) => {
                // return Windows::Exit;
                self.message = MessageStatus::ClearMessage;
                let parent_dir = get_directory_from_file_path(&self.file_path).unwrap();
                self.comb_key = None;
                Windows::FileManager(parent_dir)
            } /*
            Key::Ctrl(RegularKey::Char('h')) => {
            self.message = MessageStatus::ClearMessage;
            // let parent_dir = get_directory_from_file_path(&self.file_path).unwrap();
            self.comb_key = None;
            let file_name = "help.org";
            let abs_path = fs::canonicalize(file_name).unwrap();
            Windows::TextEditor(abs_path)
            }*/
            _ => {
                self.comb_key = None;
                Windows::Current
            }
        }
    }

    pub fn apply_operation(&mut self, head_editor_op: EditorOperation) {
        match head_editor_op {
            EditorOperation::Insert(cursor, ch) => {
                self.cursor = cursor;
                self.document.insert_char(ch, &mut self.cursor);
            }
            EditorOperation::Delete(cursor) => {
                self.cursor = cursor;
                self.document.handle_backspace(&mut self.cursor);
            }
        }
    }

    pub fn handle_ctrl_keys(&mut self, reg_key: &RegularKey) -> Windows {
        match reg_key {
            RegularKey::Char('7') => {
                self.message = MessageStatus::ClearMessage;
                if self.editor_op.len() == 0 {
                    self.message =
                        MessageStatus::DrawMessage(String::from("No previous operation"));
                    return Windows::Current;
                }
                if let Some(prev_op) = self.editor_op.pop() {
                    self.apply_operation(prev_op);
                }
                Windows::Current
            }
            RegularKey::Char(' ') => {
                self.is_mark_set = !self.is_mark_set;
                Windows::Current
            }
            RegularKey::Char('o') => {
                self.dirty = true;
                self.process_keypress(&Key::Normal(RegularKey::Enter));
                self.handle_arrow_keys(&RegularKey::Up);
                Windows::Current
            }

            RegularKey::Char('y') => {
                self.dirty = true;
                if let Some(copy_rows) = &self.copied {
                    self.document
                        .paste_process(&self.cursor, copy_rows.to_vec());
                }
                Windows::Current
            }
            RegularKey::Char('d') => {
                self.message = MessageStatus::ClearMessage;
                self.command_delete();
                Windows::Current
            }

            RegularKey::Char('p') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    self.handle_shift_arrow_keys(&RegularKey::Up);
                } else {
                    self.handle_arrow_keys(&RegularKey::Up);
                }
                Windows::Current
            }
            RegularKey::Char('n') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    self.handle_shift_arrow_keys(&RegularKey::Down);
                } else {
                    self.handle_arrow_keys(&RegularKey::Down);
                }
                Windows::Current
            }
            RegularKey::Char('f') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    self.handle_shift_arrow_keys(&RegularKey::Right);
                } else {
                    self.handle_arrow_keys(&RegularKey::Right);
                }
                Windows::Current
            }

            RegularKey::Char('w') => {
                self.message = MessageStatus::ClearMessage;
                self.dirty = true;
                self.copied = self.delete_selected();
                Windows::Current
            }

            RegularKey::Char('k') => {
                self.message = MessageStatus::ClearMessage;
                self.copied = Some(vec![self.document.command_kill_line(&mut self.cursor)]);
                self.dirty = true;
                Windows::Current
            }

            RegularKey::Char('b') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    self.handle_shift_arrow_keys(&RegularKey::Left);
                } else {
                    self.handle_arrow_keys(&RegularKey::Left);
                }
                Windows::Current
            }

            RegularKey::Char('e') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    let first = self.cursor.clone();
                    self.cursor.set_x(self.document.cur_row_len(&self.cursor));
                    let last = self.cursor.clone();
                    if let Some((start_hl, _)) = self.highlight {
                        self.highlight = Some((start_hl, last));
                    } else {
                        self.highlight = Some((first, last));
                    }
                } else {
                    self.cursor.set_x(self.document.cur_row_len(&self.cursor));
                    self.highlight = None;
                    self.is_mark_set = false;
                }

                Windows::Current
            }
            RegularKey::Char('a') => {
                self.message = MessageStatus::ClearMessage;
                if self.is_mark_set {
                    let last = self.cursor.clone();
                    self.cursor.set_x(0);
                    // self.cursor.set_x(self.document.cur_row_len(&self.cursor));
                    let first = self.cursor.clone();
                    if let Some((_, end_hl)) = self.highlight {
                        self.highlight = Some((first, end_hl));
                    } else {
                        self.highlight = Some((first, last));
                    }
                } else {
                    // self.cursor.set_x(self.document.cur_row_len(&self.cursor));
                    self.cursor.set_x(0);
                    self.highlight = None;
                    self.is_mark_set = false;
                }
                // self.highlight = None;
                Windows::Current
            }
            RegularKey::Char('g') => {
                self.is_mark_set = false;
                self.highlight = None;
                self.search_locations = None;
                self.message = MessageStatus::ClearMessage;
                Windows::Current
            }
            RegularKey::Char('q') => Windows::Exit,
            // RegularKey::Char('s') => {
            // self.dirty = false;
            // self.pending = Some(Status::Search);
            // self.search();
            // self.is_focus = false;
            // Windows::Minibuffer(MiniResult::Prompt(vec!["Find something: ".to_string()]))
            // }
            // RegularKey::Char(' ') => {
            // self.message = MessageStatus::DrawMessage(String::from("C-x- "));
            // self.comb_key = Some(Key::Ctrl(RegularKey::Char(' ')));
            // Windows::Current
            // }
            RegularKey::Char('x') => {
                self.message = MessageStatus::DrawMessage(String::from("C-x- "));
                self.comb_key = Some(Key::Ctrl(RegularKey::Char('x')));
                Windows::Current
            }

            _ => Windows::Current,
        }
    }

    fn delete_selected(&mut self) -> Option<Vec<String>> {
        if let Some((start_cursor, end_cursor)) = self.highlight {
            self.highlight = None;
            self.is_mark_set = false;
            return Some(self.document.remove_range_delete(
                &start_cursor,
                &end_cursor,
                &mut self.cursor,
            ));
        } else {
            None
        }
    }

    // fn copy_selected(&mut self) -> Option<Vec<String>> {
    //     if let Some((start_cursor, end_cursor)) = self.highlight {
    //         let text_rows = self.document.get_text_rows_imm_ref();
    //         let (start_x, start_y) = start_cursor.get_pos();
    //         let (end_x, end_y) = end_cursor.get_pos();

    //         let mut ret_rows = Vec::new();
    //         if start_y == end_y {
    //             let only_str = text_rows
    //                 .get(start_y as usize)
    //                 .unwrap()
    //                 .get_chars()
    //                 .to_owned();
    //             return Some(vec![only_str[start_x as usize..end_x as usize]
    //                 .to_owned()
    //                 .clone()]);
    //         } else {
    //             let start_str = text_rows
    //                 .get(start_y as usize)
    //                 .unwrap()
    //                 .get_chars()
    //                 .to_owned();
    //             ret_rows.push(start_str[start_x as usize..].to_owned().clone());
    //             let mut current_y = start_y + 1;
    //             loop {
    //                 if current_y >= end_y {
    //                     break;
    //                 }
    //                 let mid_temp_str = text_rows
    //                     .get(current_y as usize)
    //                     .unwrap()
    //                     .get_chars()
    //                     .to_owned();
    //                 ret_rows.push(mid_temp_str.to_owned().clone());
    //                 current_y += 1;
    //             }
    //             let end_str = text_rows
    //                 .get(end_y as usize)
    //                 .unwrap()
    //                 .get_chars()
    //                 .to_owned();
    //             ret_rows.push(end_str[0..end_x as usize].to_owned().clone());
    //             return Some(ret_rows);
    //         }
    //     }
    //     None
    // }

    fn copy_process(&mut self) -> Option<Vec<String>> {
        if let Some((start_cursor, end_cursor)) = self.highlight {
            let text_rows = self.document.get_text_rows_imm_ref();
            let (start_x, start_y) = start_cursor.get_pos();
            let (end_x, end_y) = end_cursor.get_pos();

            let mut ret_rows = Vec::new();
            if start_y == end_y {
                let only_str = text_rows
                    .get(start_y as usize)
                    .unwrap()
                    .get_chars()
                    .to_owned();
                return Some(vec![only_str[start_x as usize..end_x as usize]
                    .to_owned()
                    .clone()]);
            } else {
                let start_str = text_rows
                    .get(start_y as usize)
                    .unwrap()
                    .get_chars()
                    .to_owned();
                ret_rows.push(start_str[start_x as usize..].to_owned().clone());
                let mut current_y = start_y + 1;
                loop {
                    if current_y >= end_y {
                        break;
                    }
                    let mid_temp_str = text_rows
                        .get(current_y as usize)
                        .unwrap()
                        .get_chars()
                        .to_owned();
                    ret_rows.push(mid_temp_str.to_owned().clone());
                    current_y += 1;
                }
                let end_str = text_rows
                    .get(end_y as usize)
                    .unwrap()
                    .get_chars()
                    .to_owned();
                ret_rows.push(end_str[0..end_x as usize].to_owned().clone());
                return Some(ret_rows);
            }
        }
        None
    }

    pub fn process_keypress(&mut self, key: &Key) -> Windows {
        // let prev_clear_message = self.message == MessageStatus::ClearMessage;
        let mut window = Windows::Current;
        // self.document.set_cursor(self.cursor);
        match key {
            Key::Ctrl(reg_key) => {
                window = self.handle_ctrl_keys(reg_key);
            }
            Key::Meta(RegularKey::Char('f')) => self.forward_word(),
            Key::Meta(RegularKey::Char('d')) => {
                let _ = self.document.forward_delete_word(&self.cursor);
                self.dirty = true;
            } //self.forward_word(),
            Key::Meta(RegularKey::Char('w')) => {
                self.copied = self.copy_process();
            }
            Key::Meta(RegularKey::Char('b')) => {
                self.backward_word();
            }
            Key::Meta(RegularKey::Char('<')) => {
                self.goto_start();
            }
            Key::Meta(RegularKey::Char('>')) => {
                self.goto_end_of_file();
            }
            Key::Normal(RegularKey::Char(ch)) | Key::Shift(RegularKey::Char(ch)) => {
                self.document.insert_char(*ch, &mut self.cursor);
                self.recent_change = Change::ChangedLine(self.cursor.get_y());
                self.dirty = true;
                self.highlight = None;
                self.is_mark_set = false;
                let head_editor_op = EditorOperation::Delete(self.cursor);
                self.editor_op.push(head_editor_op);
            }
            Key::Normal(RegularKey::Enter) => {
                self.recent_change = Change::Enter(self.cursor.get_y());
                self.document.operation_of_return_key(&mut self.cursor);
                self.highlight = None;
                self.is_mark_set = false;
                self.dirty = true;
                self.editor_op.push(EditorOperation::Delete(self.cursor));
            }
            Key::Normal(RegularKey::Backspace) => {
                if self.cursor.get_x() == 0 && self.cursor.get_y() == 0 {
                } else {
                    self.dirty = true;
                    let ch = self.document.handle_backspace(&mut self.cursor);
                    let head_editor_op = EditorOperation::Insert(self.cursor, ch);
                    self.editor_op.push(head_editor_op);
                    self.highlight = None;
                    self.is_mark_set = false;
                }
            }
            Key::Shift(reg_key) => {
                // return Windows::Exit;
                self.handle_shift_arrow_keys(reg_key);
            }
            Key::Normal(reg_key) => {
                if self.is_mark_set {
                    self.handle_shift_arrow_keys(reg_key);
                } else {
                    let _ = self.handle_arrow_keys(reg_key);
                    self.highlight = None;
                    self.is_mark_set = false;
                }
            }
            _ => {}
        }
        // if prev_clear_message && self.message == MessageStatus::ClearMessage {
        // self.message == MessageStatus::NoChange;
        // }
        return window;
    }

    pub fn goto_end(&mut self) {
        let cur_row_length = self.document.cur_row_len(&self.cursor);
        if (cur_row_length) > 0 {
            self.cursor.set_x(cur_row_length);
        }
    }

    pub fn goto_beginning(&mut self) {
        let pos_y = self.cursor.get_y();
        let total_len = self.document.document_len();
        if !self.cursor.above(total_len) {
        } else {
            self.cursor.set_pos((0, pos_y))
        }
    }

    pub fn goto_start(&mut self) {
        self.cursor.set_pos((0, 0))
    }

    pub fn goto_end_of_file(&mut self) {
        self.cursor
            .set_pos((0, (self.document.document_len() - 1) as u16))
    }

    pub fn get_cursor_pos(&self) -> Position {
        self.cursor
    }

    fn command_delete(&mut self) -> char {
        let _ = self.process_keypress(&Key::Normal(RegularKey::Right));
        self.dirty = true;
        self.document.handle_backspace(&mut self.cursor)
    }
}

#[cfg(test)]
mod tests {
    macro_rules! create_vec_row{
        ($($str:expr),*) => {
            {
                let mut v = Vec::new();
                $(v.push(Row::new_str_ref($str));)*
                    v
            }
        };
        }

    use super::*;
    // use crossterm::terminal;
    use std::fs;
    #[test]
    fn render_buffer() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!(
            "emacsskdskdjskdsjdksjdsdkjd",
            " rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            ""
        );

        let _text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        // for _ in 0..5 {
        // text_editor.process_keypress(&Key::Shift(RegularKey::Down));
        // }
    }

    #[test]
    fn test_copy_process_whole() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!(
            "emacsskdskdjskdsjdksjdsdkjd",
            " rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            ""
        );

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        for _ in 0..5 {
            text_editor.process_keypress(&Key::Shift(RegularKey::Down));
        }

        // if let Some(vec_str) = text_editor.copy_process() {
        // assert_eq!(vec_str, vec!["emacs".to_string()]);
        // }

        assert_eq!(Position::new((0, 5)), text_editor.get_pos_as_pos());
        text_editor.process_keypress(&Key::Shift(RegularKey::Down));

        let lhs = vec![
            "emacsskdskdjskdsjdksjdsdkjd".to_string(),
            " rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "".to_string(),
        ];

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(lhs, vec_str); //, vec!["emacs".to_string()]);
        }
    }

    #[test]
    fn test_ctrl_d() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!(
            "emacsskdskdjskdsjdksjdsdkjd",
            " rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            ""
        );

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        for _ in 0..5 {
            text_editor.process_keypress(&Key::Shift(RegularKey::Down));
        }

        // if let Some(vec_str) = text_editor.copy_process() {
        // assert_eq!(vec_str, vec!["emacs".to_string()]);
        // }

        assert_eq!(Position::new((0, 5)), text_editor.get_pos_as_pos());
        text_editor.process_keypress(&Key::Shift(RegularKey::Down));

        let lhs = vec![
            "emacsskdskdjskdsjdksjdsdkjd".to_string(),
            " rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "rowmeacsjfkdfjdkfjdkfjdkfd".to_string(),
            "".to_string(),
        ];

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(lhs, vec_str); //, vec!["emacs".to_string()]);
        }
    }

    #[test]
    fn test_copy_empty_process() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!("");

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        for _ in 0..5 {
            text_editor.process_keypress(&Key::Shift(RegularKey::Right));
        }

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(vec_str, vec!["".to_string()]);
        }

        assert_eq!(Position::new((0, 0)), text_editor.get_pos_as_pos());
        text_editor.process_keypress(&Key::Shift(RegularKey::Down));

        let lhs = vec![""];

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(lhs, vec_str);
        }
    }

    #[test]
    fn test_copy_process_paste() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!("emacs is cool", "xmonad is greatest");

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);
        for _ in 0..6 {
            text_editor.process_keypress(&Key::Shift(RegularKey::Right));
        }
        text_editor.process_keypress(&Key::Shift(RegularKey::Down));
        text_editor.process_keypress(&Key::Meta(RegularKey::Char('w')));

        text_editor.process_keypress(&Key::Ctrl(RegularKey::Char('y')));

        let rows = create_vec_row!("emacs is cool", "xmonademacs is cool", "xmonad is greatest");

        for (row_index, row) in rows.iter().enumerate() {
            let cur_row = &text_editor.document.get_text_rows_imm_ref()[row_index];
            assert_eq!(row.get_chars(), cur_row.get_chars());
        }
        // assert_eq!(rows, text_editor.document.get_text_rows_imm_ref());
    }

    #[test]
    fn test_copy_process() {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!(
            "emacsskdskdjskdsjdksjdsdkjd",
            " rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd"
        );

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        for _ in 0..5 {
            text_editor.process_keypress(&Key::Shift(RegularKey::Right));
        }

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(vec_str, vec!["emacs".to_string()]);
        }

        assert_eq!(Position::new((5, 0)), text_editor.get_pos_as_pos());
        text_editor.process_keypress(&Key::Shift(RegularKey::Down));

        let lhs = vec![
            "emacsskdskdjskdsjdksjdsdkjd".to_string(),
            " rowm".to_string(),
        ];

        if let Some(vec_str) = text_editor.copy_process() {
            assert_eq!(lhs, vec_str); //, vec!["emacs".to_string()]);
        }
    }

    #[test]
    fn test_go_2_cursor_movement() {
        use crate::search::search;
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = create_vec_row!(
            "emacsskdskdjskdsjdksjdsdkjd",
            " rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd",
            "rowmeacsjfkdfjdkfjdkfjdkfd"
        );

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);

        let input = String::from("row");
        let positions = search(rows.as_ref(), &input);
        assert_eq!(
            vec![(1, vec![1]), (2, vec![0]), (3, vec![0]), (4, vec![0])],
            positions
        );

        text_editor.adjust_search_cursor(input, positions);
        text_editor.go_2_next_nearest_search();
        let rhs = text_editor.get_pos_as_pos();
        assert_eq!(Position::new((1, 1)), rhs);
    }

    #[test]
    fn test_go_2_cursor_movement_2() {
        use crate::search::search;
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = vec![
            Row::new("emacsskdskdjskdsjdksjdsdkjd".to_string()),
            Row::new(" rowmeacsjfkd row jdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
        ];

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);
        let _lhs = text_editor.get_pos_as_pos();

        let input = String::from("row");
        let positions = search(rows.as_ref(), &input);
        assert_eq!(
            vec![(1, vec![1, 14]), (2, vec![0]), (3, vec![0]), (4, vec![0])],
            positions
        );
        text_editor.adjust_search_cursor(input, positions);
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        let rhs = text_editor.get_pos_as_pos();
        assert_eq!(Position::new((14, 1)), rhs);
    }

    #[test]
    fn test_go_2_cursor_movement_3() {
        use crate::search::search;
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = vec![
            Row::new("emacsskdskdjskdsjdksjdsdkjd".to_string()),
            Row::new(" rowmeacsjfkd row jdkf row jdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
        ];

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);
        let _lhs = text_editor.get_pos_as_pos();
        text_editor.go_2_next_nearest_search();

        let input = String::from("row");
        let positions = search(rows.as_ref(), &input);
        assert_eq!(
            vec![
                (1, vec![1, 14, 23]),
                (2, vec![0]),
                (3, vec![0]),
                (4, vec![0])
            ],
            positions
        );
        text_editor.adjust_search_cursor(input, positions);
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        let rhs = text_editor.get_pos_as_pos();
        assert_eq!(Position::new((0, 2)), rhs);
    }

    #[test]
    fn test_go_2_cursor_movement_4() {
        use crate::search::search;
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let rows = vec![
            Row::new("emacsskdskdjskdsjdksjdsdkjd".to_string()),
            Row::new("rowmeacsjfkd row jdkf row jdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
        ];

        let mut text_editor = TextMode::new_with_lines(rows.clone(), &dummy_abs_path);
        let _lhs = text_editor.get_pos_as_pos();
        text_editor.go_2_next_nearest_search();

        let input = String::from("row");
        let positions = search(rows.as_ref(), &input);
        assert_eq!(
            vec![
                (1, vec![0, 13, 22]),
                (2, vec![0]),
                (3, vec![0]),
                (4, vec![0])
            ],
            positions
        );
        text_editor.adjust_search_cursor(input, positions);
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        text_editor.go_2_next_nearest_search();
        let rhs = text_editor.get_pos_as_pos();
        assert_eq!(Position::new((0, 3)), rhs);
    }

    fn get_dummy() -> PathBuf {
        let test_src_main = "test-src/editor.rs";
        let dummy_abs_path = fs::canonicalize(test_src_main).unwrap();
        dummy_abs_path
    }
    #[test]
    fn test_ctrl_x_ctrl_o() {
        let test_src_main = "test-src/editor.rs";
        let _dummy_abs_path = fs::canonicalize(test_src_main).unwrap();

        let _rows = vec![
            Row::new("emacsskdskdjskdsjdksjdsdkjd".to_string()),
            Row::new("rowmeacsjfkd row jdkf row jdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
        ];

        let rhs = Position::new((0, 3));
        assert_eq!(Position::new((0, 3)), rhs);
    }

    #[test]
    fn test_enter() {
        let text_rows = create_vec_row!["emacs", "haskell"];
        let mut text_mode = TextMode::new_with_lines(text_rows, &get_dummy());
        text_mode.keypress_handling(&Key::Normal(RegularKey::Enter), None, Position::default());
        let lhs = create_vec_row!["", "emacs", "haskell"];
        assert_eq!(lhs.len(), text_mode.get_text_rows_imm_ref().len());
        assert_eq!(lhs, text_mode.get_text_rows_imm_ref());
    }
}
// }
/*
#[cfg(test)]
mod tests {
    use super::*;
    use crossterm::terminal;
    #[test]
    fn test_cursor_movement() {
        let (size_x, size_y) = terminal::size().unwrap();
        let test_src_main = "test-src/editor.rs";
        let abs_path = fs::canonicalize(test_src_main).unwrap();
        let mut text_mode = TextMode::new_with_file(abs_path, size_y, size_x);
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Right));
        let cursor = Position::new((3, 0));
        let lhs = text_mode.cursor;
        assert_eq!(lhs, cursor);
    }

    #[test]
    fn test_save() {
        let (size_x, size_y) = terminal::size().unwrap();
        // let test_src_main = "test-src/editor.rs";
        let test_src_main = "text.rs";
        let abs_path1 = fs::canonicalize(test_src_main).unwrap();
        let abs_path2 = fs::canonicalize(test_src_main).unwrap();
        let mut text_mode1 = TextMode::new_with_file(abs_path1, size_y, size_x);
        let before_len = text_mode1.text_rows.len();
        text_mode1.document.write_to_file().unwrap();
        let text_mode2 = TextMode::new_with_file(abs_path2, size_y, size_x);
        let after_len = text_mode2.text_rows.len();
        assert_eq!(before_len, after_len);
    }

    #[test]
    fn test_hl_cursor_movement() {
        let (size_x, size_y) = terminal::size().unwrap();
        let test_src_main = "test-src/editor.rs";
        let abs_path = fs::canonicalize(test_src_main).unwrap();
        let rows = vec![
            Row::new("emacsskdskdjskdsjdksjdsdkjd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
            Row::new("rowmeacsjfkdfjdkfjdkfjdkfd".to_string()),
        ];
        let mut text_mode = TextMode::new_with_lines(rows, &abs_path, size_y, size_x);
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Down));
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Down));
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Down));
        let _ = text_mode.process_keypress(&Key::Normal(RegularKey::Down));
        let _ = text_mode.process_keypress(&Key::Shift(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Shift(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Shift(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Shift(RegularKey::Right));
        let _ = text_mode.process_keypress(&Key::Shift(RegularKey::Up));
        let left_pos = Position::new((4, 3));
        let right_pos: Position = Position::new((0, 4));
        let hl_cursor: Option<(Position, Position)> = Some((left_pos, right_pos));
        assert_eq!(text_mode.highlight, hl_cursor);
    }

    #[test]
    fn test_forward_word() {
        let (size_x, size_y) = terminal::size().unwrap();
        let test_src_main = "test-src/editor.rs";
        let abs_path = fs::canonicalize(test_src_main).unwrap();
        let rows = vec![
            Row::new("#include <stdio.h>".to_string()),
            Row::new("".to_string()),
            Row::new("int main(){".to_string()),
            Row::new("printf(\"emacs\");".to_string()),
            Row::new("return 0;".to_string()),
            Row::new("}".to_string()),
        ];
        let mut text_mode = TextMode::new_with_lines(rows, &abs_path, size_y, size_x);
        let rhs = Position::new((8, 0));
        for _ in 0..1 {
            text_mode.process_keypress(&Key::Meta(RegularKey::Char('f')));
        }
        assert_eq!(text_mode.cursor, rhs);
    }

    #[test]
    fn test_backward_word() {
        let (size_x, size_y) = terminal::size().unwrap();
        let test_src_main = "test-src/editor.rs";
        let abs_path = fs::canonicalize(test_src_main).unwrap();
        let rows = vec![
            Row::new("#include <stdio.h>".to_string()),
            Row::new("".to_string()),
            Row::new("int main(){".to_string()),
            Row::new("printf(\"emacs\");".to_string()),
            Row::new("return 0;".to_string()),
            Row::new("}".to_string()),
        ];
        let mut text_mode = TextMode::new_with_lines(rows, &abs_path, size_y, size_x);
        text_mode.goto_end();
        let rhs = Position::new((16, 0));
        for _ in 0..2 {
            text_mode.process_keypress(&Key::Meta(RegularKey::Char('b')));
        }
        assert_eq!(text_mode.cursor, rhs);
    }
}
*/
