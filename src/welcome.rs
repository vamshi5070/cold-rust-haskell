use crate::keyboard::Keyboard;
use crate::my_lib::cross_key_to_cold;
// use crate::my_lib::Position;
use crate::my_lib::{Key, RegularKey};
use indoc::indoc;
use itertools::izip;

use crossterm::Result;

use ratatui::prelude::Rect;
use ratatui::text::Text;
use ratatui::widgets::Paragraph;
// use ratatui::TerminalOptions;
// use ratatui::Viewport;

pub struct Welcome {
    // cursor: Position,
    // main_logo:
}

impl Welcome {
    pub fn new() -> Self {
        Self {
            // cursor: Position::default(),
        }
    }

    #[allow(clippy::many_single_char_names)]
    fn cold() -> String {
        let c = indoc! {"
 ██████
██
██
██
 ██████
        "};
        let o = indoc! {"
 ██████  
██    ██ 
██    ██ 
██    ██ 
 ██████  
       "};
        let l = indoc! {"
██      
██      
██      
██      
███████ 
       "};
        let d = indoc! {"
██████  
██   ██ 
██   ██ 
██   ██ 
██████  
        "};
        izip!(c.lines(), o.lines(), l.lines(), d.lines()) //i.lines())
            .map(|(c, o, l, d)| format!("{c:10}{o:10}{l:10}{d:10}"))
            //{t:4}{u:5}{i:5}"))
            .collect::<Vec<_>>()
            .join("\n")
    }

    /// A fun example of using half block characters to draw a logo
    #[allow(clippy::many_single_char_names)]
    fn _logo() -> String {
        let r = indoc! {"
            ▄▄▄
            █▄▄▀
            █  █
        "};
        let a = indoc! {"
             ▄▄
            █▄▄█
            █  █
        "};
        let t = indoc! {"
            ▄▄▄
             █
             █
        "};
        let u = indoc! {"
            ▄  ▄
            █  █
            ▀▄▄▀
        "};
        let i = indoc! {"
            ▄
            █
            █
        "};
        izip!(r.lines(), a.lines(), t.lines(), u.lines(), i.lines())
            .map(|(r, a, t, u, i)| format!("{r:5}{a:5}{t:4}{a:5}{t:4}{u:5}{i:5}"))
            .collect::<Vec<_>>()
            .join("\n")
    }

    pub fn start(&mut self, keyboard: &Keyboard) -> Result<bool> {
        loop {
            let mut terminal = ratatui::init();
            //ratatui::init_with_options(TerminalOptions {
            //     viewport: Viewport::Inline(3),
            // });
            // Self::logo()
            terminal.draw(|frame| {
                frame.set_cursor_position(ratatui::prelude::Position::new(0, 0));
                let frame_area = frame.area();
                let ascii_title_area =
                    Rect::new(frame_area.width / 2 - 15, frame_area.height / 2 - 4, 70, 10);
                let caption_area = Rect::new(
                    frame_area.width / 2 - 15,
                    frame_area.height / 2 - 4 + 10,
                    70,
                    10,
                );
                frame.render_widget(ratatui::widgets::Clear, frame.area());
                frame.render_widget(Paragraph::new(Self::cold()), ascii_title_area);
                frame.render_widget(
                    Text::from("press enter to continue/ CTRL-'q' to quit"),
                    caption_area,
                );
                //("press enter to continue")
            })?;

            let key = keyboard.read();
            match key {
                Ok(key_event) => {
                    let key = cross_key_to_cold(key_event);
                    match key {
                        Key::Normal(RegularKey::Enter) => break,
                        Key::Ctrl(RegularKey::Char('q')) => return Ok(false),
                        _ => {}
                    }
                }
                Err(_) => {}
            }
        }
        Ok(true)
    }
}
