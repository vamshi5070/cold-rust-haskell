use crate::my_lib::*;
use crate::screen::Screen;

use crossterm::Result;
use std::rc::Rc;

pub struct DisplayWindow {
    pub buffer: WindManager,
    is_focus: bool,
    renderbuffer: RenderBuffer,
    status_bar: LineCell,
    pub pass_display: PassDisplay,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct PassDisplay {
    pub rowoff: u16,
    pub coloff: u16,
    pub width: u16,
    pub height: u16,
    pub origin: Position,
    pub cursor: Position,
}

impl Clone for DisplayWindow {
    fn clone(&self) -> Self {
        let cloned_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };

        Self {
            buffer: cloned_buffer,
            is_focus: self.is_focus,
            renderbuffer: self.renderbuffer.clone(),
            status_bar: self.status_bar.clone(),
            pass_display: self.pass_display,
        }
    }
}

impl DisplayWindow {
    pub fn new(buffer: WindManager, is_focus: bool, pass_display: PassDisplay) -> Result<Self> {
        let new_render_buffer = RenderBuffer::new(
            Theme,
            pass_display.width as usize,
            pass_display.height as usize,
        );
        let new_status_bar = LineCell::new("", &Theme.status_line(), pass_display.width as usize);
        Ok(Self {
            buffer,
            is_focus,
            renderbuffer: new_render_buffer,
            status_bar: new_status_bar,
            pass_display,
        })
    }

    pub fn split_horizontally(&mut self) -> Result<DisplayWindow> {
        let og_width = self.pass_display.width;
        let half = og_width / 2;
        let (one_width, sec_width) = (half + (og_width % 2), half - 1);
        self.pass_display.width = one_width;
        let (og_x, og_y) = self.pass_display.origin.get_pos();
        let shared_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };
        let new_display_origin = Position::new((og_x, self.pass_display.width + og_y + 1));
        DisplayWindow::new(
            shared_buffer,
            false,
            PassDisplay {
                origin: new_display_origin,
                cursor: self.pass_display.cursor,
                width: sec_width,
                height: self.pass_display.height,
                rowoff: self.pass_display.rowoff,
                coloff: self.pass_display.coloff,
            },
        )
    }

    pub fn split_vertically(&mut self) -> Result<DisplayWindow> {
        let prev_height = self.pass_display.height;
        let original_display_height = prev_height / 2 + prev_height % 2;
        self.pass_display.height = original_display_height;
        let split_display_height = prev_height / 2;
        let (og_x, og_y) = self.pass_display.origin.get_pos();
        let shared_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };

        let new_display_origin = Position::new((og_x + original_display_height, og_y));

        DisplayWindow::new(
            shared_buffer,
            false,
            PassDisplay {
                origin: new_display_origin,
                cursor: self.pass_display.cursor,
                width: self.pass_display.width,
                height: split_display_height,
                rowoff: self.pass_display.rowoff,
                coloff: self.pass_display.coloff,
            },
        )
    }

    pub fn get_cursor(&self) -> Position {
        self.pass_display.cursor
    }

    pub fn get_limit(&self) -> (u16, u16) {
        (self.pass_display.height, self.pass_display.width)
    }

    pub fn replace_buffer(&mut self, new_buffer: &WindManager) -> Result<()> {
        let shared_buffer = match new_buffer {
            WindManager::TextEditorMode(ref tm) => {
                self.pass_display.cursor = tm.borrow().get_pos_as_pos();
                WindManager::TextEditorMode(Rc::clone(&tm))
            }

            WindManager::FileManagerMode(ref fm) => {
                self.pass_display.cursor = fm.borrow().get_pos_as_pos();
                WindManager::FileManagerMode(Rc::clone(&fm))
            }
            _ => WindManager::Null,
        };
        self.buffer = shared_buffer;
        self.is_focus = true;

        self.pass_display.rowoff = 0;
        self.pass_display.coloff = 0;

        Ok(())
    }

    pub fn get_buffer(&self) -> &WindManager {
        &self.buffer
    }

    pub fn display_cursor(&mut self, screen: &mut Screen, cursor: Position) -> Result<()> {
        match &self.buffer {
            WindManager::TextEditorMode(_tm) => {
                self.pass_display.cursor = cursor;
                let (pos_x, pos_y) = self.pass_display.cursor.get_pos();
                let (og_x, og_y) = self.pass_display.origin.get_pos();

                let new_pos_y = og_y + pos_y - self.pass_display.rowoff;
                let new_pos_x = og_x + pos_x - self.pass_display.coloff;
                let custom_pos = Position::new((new_pos_x, new_pos_y));

                screen.render_cursor(&custom_pos)?;
                screen.flush()
            }
            WindManager::FileManagerMode(fm) => {
                let mut head_value = fm.borrow_mut();
                head_value.draw_cursor(screen, &self.pass_display)
            }
            _ => Ok(()),
        }
    }

    fn write_status_bar(&mut self, _theme: &Theme) {
        let (terminal_size_x, _) = crossterm::terminal::size().unwrap();
        match &self.buffer {
            WindManager::TextEditorMode(tm) => {
                let head_value = tm.borrow();
                let status = head_value.get_status_bar_content();

                let remaining_len = terminal_size_x - status.len() as u16;
                let space = " ".repeat(remaining_len as usize);
                let status_with_space = status.clone() + &space;

                self.status_bar = LineCell::new(
                    &status_with_space,
                    &Theme.status_line(),
                    self.pass_display.width as usize,
                );
            }
            WindManager::FileManagerMode(fm) => {
                let head_value = fm.borrow();
                let status = head_value.get_status_bar_content();

                let remaining_len = terminal_size_x - status.len() as u16;
                let space = " ".repeat(remaining_len as usize);
                let status_with_space = status.clone() + &space;

                self.status_bar = LineCell::new(
                    &status_with_space,
                    &Theme.status_line(),
                    self.pass_display.width as usize,
                );
            }

            _ => {}
        }
    }

    fn loop_same(&self, hl_vec: &[Highlight], hl: Highlight, start: usize) -> usize {
        let mut end = start + 1;
        loop {
            if end >= hl_vec.len() {
                break;
            }
            if hl != hl_vec[end].clone() {
                break;
            }
            end += 1;
        }
        return end;
    }

    pub fn refresh(
        &mut self,
        temp_cursor: Option<Position>,
        theme: &Theme,
    ) -> (RenderBuffer, LineCell, PassDisplay) {
        let default_style = Theme::default(theme);

        self.renderbuffer = RenderBuffer::new(
            Theme,
            self.pass_display.width as usize,
            self.pass_display.height as usize,
        );

        let search_style = theme.search();

        self.pass_display.cursor = if let Some(new_cursor) = temp_cursor {
            new_cursor
        } else {
            self.pass_display.cursor
        };

        let (new_rowoff, new_coloff) = scroll(&self.pass_display);
        self.pass_display.rowoff = new_rowoff;
        self.pass_display.coloff = new_coloff;

        match &self.buffer {
            WindManager::TextEditorMode(tm) => {
                let head_value = tm.borrow();

                let buffer_doc_len = head_value.doc_len();
                let start = self.pass_display.rowoff as usize;
                let end = if (self.pass_display.rowoff + self.pass_display.height - 1) as usize
                    > buffer_doc_len - 1
                {
                    buffer_doc_len - 1
                } else {
                    (self.pass_display.rowoff + self.pass_display.height - 1) as usize
                };

                let mut current_search_row_index = 0;
                let row_compressed = head_value.get_doc_range(start, end);
                let maybe_search_locations = head_value.search_locations.clone();

                if let Some((ref tup_vec, _)) = maybe_search_locations {
                    loop {
                        if current_search_row_index >= tup_vec.len() {
                            break;
                        }
                        if self.pass_display.rowoff > tup_vec[current_search_row_index].0 as u16 {
                            current_search_row_index += 1;
                        } else {
                            break;
                        }
                    }
                }

                let highlight = head_value.highlight;

                let (new_rowoff, new_coloff) = scroll(&self.pass_display);
                let coloff_usize = self.pass_display.coloff as usize;
                self.pass_display.rowoff = new_rowoff;
                self.pass_display.coloff = new_coloff;

                for (row_index, row) in row_compressed.iter().enumerate() {
                    if row.len == 0 {
                        self.renderbuffer.new_line("", &default_style);
                        continue;
                    }

                    if self.pass_display.coloff as usize >= row.len {
                        self.renderbuffer.new_line("", &default_style);
                        continue;
                    }

                    let col_end = std::cmp::min(
                        self.pass_display.coloff as usize + self.pass_display.width as usize,
                        row.len,
                    );

                    let render_str = &row.get_chars()[self.pass_display.coloff as usize..col_end];

                    let render_hl = &row.hl[self.pass_display.coloff as usize..col_end];
                    let highlight_style = theme.transparent_bg();
                    // (shift) select highlight text
                    let text_rendered_hl = if let Some((start_cursor, end_cursor)) = highlight {
                        let pos_y = self.pass_display.rowoff + row_index as u16;
                        let (first_x, first_y) = start_cursor.get_pos();
                        let (second_x, second_y) = end_cursor.get_pos();

                        if pos_y == first_y && pos_y == second_y {
                            self.renderbuffer.new_line("", &default_style);
                            if first_x < self.pass_display.coloff {
                                self.renderbuffer.push_str_last(
                                    &render_str[0..(second_x - self.pass_display.coloff) as usize],
                                    &highlight_style,
                                );
                                self.renderbuffer.push_str_last(
                                    &render_str[(second_x - self.pass_display.coloff) as usize
                                        ..col_end - self.pass_display.coloff as usize],
                                    &default_style,
                                );
                            } else if second_x as usize
                                > render_str.len() + self.pass_display.coloff as usize
                            {
                                self.renderbuffer.push_str_last(
                                    &render_str[0..(first_x - self.pass_display.coloff) as usize],
                                    &default_style,
                                );
                                self.renderbuffer.push_str_last(
                                    &render_str[(first_x - self.pass_display.coloff) as usize
                                        ..render_str.len()],
                                    &highlight_style,
                                );
                            } else {
                                self.renderbuffer.push_str_last(
                                    &render_str[0..(first_x - self.pass_display.coloff) as usize],
                                    &default_style,
                                );
                                self.renderbuffer.push_str_last(
                                    &render_str[(first_x - self.pass_display.coloff) as usize
                                        ..(second_x - self.pass_display.coloff) as usize],
                                    &highlight_style,
                                );
                                self.renderbuffer.push_str_last(
                                    &render_str[(second_x - self.pass_display.coloff) as usize
                                        ..col_end - self.pass_display.coloff as usize],
                                    &default_style,
                                );
                            }
                            true
                        } else if pos_y == first_y {
                            self.renderbuffer.new_line("", &default_style);
                            if first_x < self.pass_display.coloff {
                                self.renderbuffer.push_str_last(
                                    &render_str[0..col_end - self.pass_display.coloff as usize],
                                    &highlight_style,
                                );
                            } else {
                                self.renderbuffer.push_str_last(
                                    &render_str[0..(first_x - self.pass_display.coloff) as usize],
                                    &default_style,
                                );
                                self.renderbuffer.push_str_last(
                                    &render_str[(first_x - self.pass_display.coloff) as usize
                                        ..col_end - self.pass_display.coloff as usize],
                                    &highlight_style,
                                );
                            }
                            true
                        } else if pos_y == second_y {
                            self.renderbuffer.new_line("", &default_style);
                            self.renderbuffer.push_str_last(
                                &render_str[0..(second_x - self.pass_display.coloff) as usize],
                                &highlight_style,
                            );
                            self.renderbuffer.push_str_last(
                                &render_str[(second_x - self.pass_display.coloff) as usize
                                    ..render_str.len()],
                                &default_style,
                            );
                            true
                        } else if pos_y > first_y && pos_y < second_y {
                            self.renderbuffer.new_line("", &default_style);
                            self.renderbuffer
                                .push_str_last(&render_str, &highlight_style);
                            true
                        } else {
                            false
                        }
                    } else {
                        false
                    };

                    // normal text
                    let mut current = 0;
                    if !text_rendered_hl {
                        self.renderbuffer.new_line("", &default_style);
                        loop {
                            if current >= render_hl.len() {
                                break;
                            }
                            let end = self.loop_same(render_hl, render_hl[current], current);
                            self.renderbuffer.push_str_last(
                                &render_str[current..end],
                                &theme.hl_to_style(render_hl[current]),
                            );
                            current = end;
                        }
                        // self.renderbuffer.push_str_last(
                        //         "$",
                        //         &theme.hl_to_style(Highlight::Normal),
                        // );
                    }

                    // search text
                    if let Some((ref tup_vec, length_search)) = maybe_search_locations {
                        let pos_y = row_index + self.pass_display.rowoff as usize;
                        if current_search_row_index >= tup_vec.len() {
                        } else {
                            let (search_y, search_xs) = &tup_vec[current_search_row_index];
                            if *search_y == pos_y {
                                for search_x_ref in search_xs {
                                    if coloff_usize > *search_x_ref {
                                        continue;
                                    }
                                    let search_x = *search_x_ref;
                                    if render_str.len() < search_x - coloff_usize {
                                        break;
                                    }

                                    self.renderbuffer.replace_last_style_only_at(
                                        &vec![search_style.clone(); length_search],
                                        search_x - coloff_usize,
                                    );
                                }
                                current_search_row_index += 1;
                            }
                        }
                    }
                }
            }

            WindManager::FileManagerMode(tm) => {
                let head_value = tm.borrow();

                let buffer_doc_len = head_value.doc_len();
                let start = self.pass_display.rowoff as usize;
                let end = if (self.pass_display.rowoff + self.pass_display.height - 1) as usize
                    > buffer_doc_len - 1
                {
                    buffer_doc_len - 1
                } else {
                    (self.pass_display.rowoff + self.pass_display.height - 1) as usize
                };
                let mut current_search_row_index = 0;
                let row_compressed = head_value.get_doc_range(start, end);

                let maybe_search_locations = head_value.search_locations.clone();

                if let Some((ref tup_vec, _length_search)) = maybe_search_locations {
                    loop {
                        if current_search_row_index >= tup_vec.len() {
                            break;
                        }
                        if self.pass_display.rowoff > tup_vec[current_search_row_index].0 as u16 {
                            current_search_row_index += 1;
                        } else {
                            break;
                        }
                    }
                }

                for (row_index, row) in row_compressed.iter().enumerate() {
                    if row.len == 0 {
                        self.renderbuffer.new_line("", &default_style);
                        continue;
                    }

                    if self.pass_display.coloff as usize >= row.len {
                        self.renderbuffer.new_line("", &default_style);
                        continue;
                    }

                    let col_end = std::cmp::min(
                        self.pass_display.coloff as usize + self.pass_display.width as usize,
                        row.len,
                    );

                    let _render_str = &row.get_chars()[self.pass_display.coloff as usize..col_end];
                    let _render_hl = &row.hl[self.pass_display.coloff as usize..col_end];

                    let text_rendered =
                        if let Some((ref search_res, search_length)) = maybe_search_locations {
                            let pos_y = row_index + self.pass_display.rowoff as usize;
                            if current_search_row_index >= search_res.len() {
                                false
                            } else {
                                // self.renderbuffer.new_line("", &default_style);
                                let (search_y, search_xs) = &search_res[current_search_row_index];
                                if *search_y == pos_y {
                                    let mut prev_x_loc = 0;
                                    self.renderbuffer.new_line("", &default_style);
                                    for search_x_ref in search_xs {
                                        let search_x = *search_x_ref;
                                        self.renderbuffer.push_str_last(
                                            &row.get_chars()[prev_x_loc..search_x],
                                            &default_style,
                                        );
                                        self.renderbuffer.push_str_last(
                                            &row.get_chars()[search_x..search_x + search_length],
                                            &search_style,
                                        );
                                        prev_x_loc = search_x + search_length;
                                    }
                                    if prev_x_loc < row.len {
                                        self.renderbuffer.push_str_last(
                                            &row.get_chars()[prev_x_loc..row.len],
                                            &default_style,
                                        );
                                    }
                                    current_search_row_index += 1;
                                    true
                                } else {
                                    false
                                }
                            }
                        } else {
                            false
                        };

                    let mut current = 0;

                    if !text_rendered {
                        self.renderbuffer.new_line(
                            "",
                            &default_style,
                            // &row.get_chars()[current..end],
                            // &theme.hl_to_style(row.hl[current]), //
                        );

                        loop {
                            if current >= row.len {
                                break;
                            }
                            let end = self.loop_same(&row.hl, row.hl[current], current);
                            self.renderbuffer.push_str_last(
                                &row.get_chars()[current..end],
                                &theme.hl_to_style(row.hl[current]), // &default_style,
                            );
                            current = end;
                        }
                    }
                    // let fill_space_str =
                    //     " ".repeat(self.pass_display.width.saturating_sub(row.len as u16) as usize);
                    // renderbuffer.replace_text(row.len, row_index, &fill_space_str, &default_style);
                    /*
                            for i in 0..self.pass_display.height as usize - row_compressed.len() {
                                let fill_space_str = " ".repeat(self.pass_display.width as usize);
                                // self.pass_display.width.saturating_sub(row.len as u16) as usize);
                                renderbuffer.replace_text(
                                    0,
                                    row_compressed.len() + i,
                                    &fill_space_str,
                                    &default_style,
                                );
                        }
                    */
                }
            }
            _ => {}
        }
        self.write_status_bar(theme);
        return (
            self.renderbuffer.clone(),
            self.status_bar.clone(),
            self.pass_display.clone(),
        );
    }

    // let rest_len = self.renderbuffer.height - self.renderbuffer.cells.len();
    // let empty_line = " ".to_string(); //.repeat(self.renderbuffer.width - 1);
    // for i in 0..rest_len {
    //     self.renderbuffer.new_line(&empty_line, &default_style);
    // self.renderbuffer.new_line(&format!("{i}"), &default_style);
    // }

    // screen.grand_render(&self.renderbuffer, &self.status_bar, &self.pass_display);
    // screen.render(&self.renderbuffer, &self.pass_display);
    // screen.render_status_bar(&self.status_bar, &self.pass_display);

    /*
        pub fn display(
            &mut self,
            screen: &mut Screen,
            temp_cursor: Option<Position>,
            highlight: &mut Highlighter,
        ) -> Result<()> {
            self.pass_display.cursor = if let Some(new_cursor) = temp_cursor {
                new_cursor
            } else {
                self.pass_display.cursor
            };

            let (new_rowoff, new_coloff) = scroll(&self.pass_display);

            self.pass_display.rowoff = new_rowoff;
            self.pass_display.coloff = new_coloff;

            match &self.buffer {
                WindManager::TextEditorMode(tm) => {
                    let mut head_value = tm.borrow_mut();
                    if head_value.recent_change == Change::NoChange {
                        return Ok(());
                    }
                    let default_style = highlight.theme.default();
                    let buffer_doc_len = head_value.doc_len();
                    // head_value.display(
                    //     screen,
                    //     self.is_focus,
                    //     &self.pass_display,
                    //     highlight,
                    //     &mut self.render_buffer,
                    // )?;
                    // self.is_focus = focus;
                    // self.rowoff = pass_display.rowoff;
                    // self.coloff = pass_display.coloff;
                    // self.cursor = pass_display.cursor;

                    match &head_value.recent_change {
                        Change::NoChange => {}
                        Change::First => {
                            let start = self.pass_display.rowoff as usize;
                            let end = if (self.pass_display.rowoff + self.pass_display.height - 1)
                                as usize
                                > buffer_doc_len - 1
                            {
                                buffer_doc_len - 1
                            } else {
                                (self.pass_display.rowoff + self.pass_display.height - 1) as usize
                            };

                            let mut x = 0;
                            let mut y = 0;
                            // let my_str = self.document.viewport(start, end - start);
                            let row_compressed = head_value.get_doc_range(start, end);
                            // self.document.get_row_range(start, end);
                            // let style_infos = highlight.highlights(&my_str);
                            let mut style_index = 0;
                            let mut row_index = 0;
                            let mut prev_end = 0;
                            for (row_index, row) in row_compressed.iter().enumerate() {
                                self.render_buffer.set_text(
                                    0,
                                    row_index,
                                    row.get_chars(),
                                    &default_style,
                                );
                                let fill_space_str = " ".repeat(
                                    self.pass_display.width.saturating_sub(row.len as u16) as usize,
                                );
                                self.render_buffer.set_text(
                                    row.len,
                                    row_index,
                                    &fill_space_str,
                                    &default_style,
                                );
                            }
                        }
                        Change::ChangedLine(pos_y_pointer) => {
                            let pos_y = *pos_y_pointer;
                            if !(pos_y < self.pass_display.rowoff)
                                && !(pos_y > self.pass_display.rowoff + self.pass_display.height)
                            {
                                let local_y: usize = (pos_y - self.pass_display.rowoff).into();
                                let row = head_value.get_row_at(pos_y);
                                self.render_buffer
                                    .replace_at(local_y, row.get_chars(), &default_style);
                                let space_fill = " ".repeat(
                                    self.pass_display
                                        .width
                                        .saturating_sub((row.len - 1) as u16)
                                        .into(),
                                );
                                // fill space from row_len till width
                                self.render_buffer.set_text(
                                    row.len - 1,
                                    local_y as usize,
                                    &space_fill,
                                    &default_style,
                                );
                            }
                        }
                    }
                    // screen.clear_all()?;
                    // screen.render(&self.render_buffer)?;
                    // screen.flush().unwrap();
                    return Ok(());
                    /*
                        loop {
                            if row_index >= row_compressed.len() {
                                break;
                            }
                            if style_index >= style_infos.len() {
                                break;
                            }
                            let style_i = style_infos[style_index].clone();
                            if prev_end == style_i.end {
                                continue;
                            }
                            let row_len = row_compressed[row_index].len;
                            if style_i.end >= row_len {
                                row_index += 1;
                                x = 0;
                                y += 1;
                                continue;
                            }
                            let row_str_slice = &row_compressed[row_index].get_string()[style_i.start..style_i.end];
                            buffer.set_text(x, y, row_str_slice, &style_i.style);
                            x += style_i.end - style_i.start;
                            style_index += 1;
                    }
                     */
                    // for row in row_compressed {
                    // row.len
                    // }
                    // let mut iter = my_str.chars().enumerate().peekable();

                    // while let Some((pos, c)) = iter.next() {
                    //     if c == '\n' || iter.peek().is_none() {
                    //         if c != '\n' {
                    //             buffer.set_char(x as usize, y, c, &default_style);
                    //             x += 1;
                    //         }
                    //     }
                    // }
                    // let

                    // for
                    // screen.render()
                    // screen.draw_lines(
                    //     self.document.get_row_range(start, end),
                    //     pass_display,
                    //     self.highlight,
                    //     self.search_locations.clone(),
                    // )?;
                    // self.display_status_bar(screen, pass_display)?;
                }
                WindManager::FileManagerMode(fm) => {
                    // let mut head_value = fm.borrow_mut();
                    // head_value.display(screen, self.is_focus, &self.pass_display, highlight)?;
                }
                _ => {}
            }
            Ok(())
        }
    */
    pub fn set_width(&mut self, width: u16) {
        self.pass_display.width = width;
        // Ok(())
    }
}

/*
            loop {
                    if row_index >= row_compressed.len() {
                        break;
                    }
                    if style_index >= style_infos.len() {
                        break;
                    }
                    let style_i = style_infos[style_index].clone();
                    if prev_end == style_i.end {
                        continue;
                    }
                    let row_len = row_compressed[row_index].len;
                    if style_i.end >= row_len {
                        row_index += 1;
                        x = 0;
                        y += 1;
                        continue;
                    }
                    let row_str_slice =
                        &row_compressed[row_index].get_string()[style_i.start..style_i.end];
                    buffer.set_text(x, y, row_str_slice, &style_i.style);
                    x += style_i.end - style_i.start;
                    style_index += 1;
                }
*/
