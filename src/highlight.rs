use tree_sitter::{Parser, Query, QueryCursor};
use tree_sitter_rust::HIGHLIGHTS_QUERY;

use crate::my_lib::Style;
use crate::my_lib::StyleInfo;
use crate::my_lib::Theme;

pub struct Highlighter {
    parser: Parser,
    query: Query,
    pub theme: Theme,
}

impl Highlighter {
    pub fn new(theme: Theme) -> Self {
        let mut parser = Parser::new();
        let language = tree_sitter_rust::LANGUAGE;
        let language2 = tree_sitter_rust::LANGUAGE;

        parser
            .set_language(&language.into())
            .expect("Error loading Rust grammar");
        let query = Query::new(&language2.into(), HIGHLIGHTS_QUERY).unwrap();

        Self {
            parser,
            query,
            theme,
        }
    }

    pub fn highlights(&mut self, code: &str) -> Vec<StyleInfo> {
        let tree = self.parser.parse(&code, None).expect("parse works");

        let mut colors = Vec::new();
        let mut cursor = QueryCursor::new();

        let matches = cursor.matches(&self.query, tree.root_node(), code.as_bytes());

        for mat in matches {
            for cap in mat.captures {
                let node = cap.node;
                let start = node.start_byte();
                let end = node.end_byte();

                let scope = self.query.capture_names()[cap.index as usize];

                let style: Style = self.theme.get_style(scope);

                colors.push(StyleInfo { start, end, style });
            }
        }
        return colors;
    }
}
