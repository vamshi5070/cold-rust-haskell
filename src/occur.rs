use crate::document::Document;
// use crate::my_lib::create_vec_row;
use crate::my_lib::Position;
use crate::row::Row;
// use std::fs::File;
use std::path::PathBuf;

macro_rules! create_vec_row{
	($($str:expr),*) => {
	    {
		let mut v = Vec::new();
		$(v.push(Row::new_str_ref($str));)*
		    v
	    }
	};
        }

pub struct OccurMode {
    // _document: Document,
    // _cursor: Position,
}

impl OccurMode {
    pub fn new(abs_path: &PathBuf) -> Self {
        let row = create_vec_row!("TODO: Occur mode");
        OccurMode {
            // document: Document::new_with_lines(row, abs_path.clone()),
            // cursor: Position::default(),
        }
    }
}
