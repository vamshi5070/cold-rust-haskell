use crate::my_lib::*;
use crate::row::*;
use crate::screen::Screen;
// use crate::search::*;
use crossterm::Result;
use std::collections::HashMap;
use std::fs;

use crate::display_window::PassDisplay;
use std::path::PathBuf;
// macro_rules! normal {
//     ($ch:literal) => {
//         Key::Normal(RegularKey::Char($ch))
//     };
//     ($key:ident) => {
//         Key::Normal(RegularKey::$key)
//     };
// }
macro_rules! meta {
    ($ch:literal) => {
        Key::Meta(RegularKey::Char($ch))
    };
    ($key:ident) => {
        Key::Meta(RegularKey::$key)
    };
}
macro_rules! ctrl {
    ($ch:literal) => {
        Key::Ctrl(RegularKey::Char($ch))
    };
    ($key:ident) => {
        Key::Ctrl(RegularKey::$key)
    };
}

pub struct FileSystem {
    files: Vec<Row>,
    pub dir: PathBuf,
    cursor: Position,
    is_focus: bool,
    rowoff: u16,
    coloff: u16,
    comb_key: Option<Key>,
    highlight: Option<(Position, Position)>,
    minibuffer_input: String,
    message: MessageStatus,
    // height: u16,
    // width: u16,
    pending: Option<Status>,
    pub search_locations: Option<(Vec<(usize, Vec<usize>)>, usize)>,
}

impl FileSystem {
    pub fn get_status_bar_content(
        &self,
        // screen: &mut Screen,
        // pass_display: &PassDisplay,
    ) -> String {
        // let status_cursor = pass_display.origin.get_y() + pass_display.height;
        let std_msg = format!(
            "{} ({},{}) {} {}",
            if self.highlight.is_some() {
                "<hl>"
            } else {
                "<nm>"
            },
            self.cursor.get_y(),
            self.cursor.get_x(),
            "-%%-",
            // if self.dirty { "unsaved" } else { "saved" },
            self.dir.to_str().unwrap(),
        );
        // screen.draw_status_bar_vertical(
        // pass_display.origin.get_x(),
        // status_cursor,
        // pass_display.width,
        // )?;
        // screen.draw_on_status_vertical(&std_msg, status_cursor, pass_display.origin.get_x())
        return std_msg;
    }

    pub fn get_file_path(&self) -> &PathBuf {
        &self.dir
    }
    pub fn new(cur_dir: PathBuf) -> Result<Self> {
        let mut ls_files = path_buf_list_files_in_directory(&cur_dir)?;
        let mandatory_elem = vec![
            cur_dir.display().to_string(),
            String::from("."),
            String::from(".."),
        ];
        ls_files.splice(0..0, mandatory_elem);
        let mut rows = Vec::new();
        for row in ls_files {
            rows.push(Row::new_with_lang(row, Language::Dired));
        }
        Ok(Self {
            files: rows,
            dir: cur_dir,
            cursor: Position::new((0, 3)),
            is_focus: true,
            rowoff: 0,
            coloff: 0,
            comb_key: None,
            highlight: None,
            minibuffer_input: String::from(""),
            message: MessageStatus::NoChange,
            // height: height,
            // width: width,
            pending: None,
            search_locations: None,
        })
    }

    pub fn get_pos_as_pos(&self) -> Position {
        self.cursor.get_pos_as_pos()
    }

    pub fn get_message(&self) -> MessageStatus {
        self.message.clone()
    }

    pub fn set_focus(&mut self) {
        self.is_focus = true;
    }

    pub fn set_minibuffer_input(&mut self, input: String) {
        self.minibuffer_input = input;
    }

    pub fn get_cursor_pos(&self) -> Position {
        self.cursor
    }

    pub fn go_2_prev_nearest_search(&mut self) {
        if let Some((loc, _)) = &self.search_locations {
            let mut current = loc.len();
            loop {
                if current <= 0 {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current - 1];
                if self.cursor.get_y() > *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[pos_xs.len() - 1] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs.into_iter().rev() {
                        if self.cursor.get_x() > *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current -= 1;
            }
        }
    }

    pub fn go_2_next_nearest_search(&mut self) {
        if let Some((loc, _)) = &self.search_locations {
            let mut current = 0;
            loop {
                if current >= loc.len() {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current];
                if self.cursor.get_y() < *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[0] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs {
                        if self.cursor.get_x() < *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current += 1;
            }
        }
    }

    pub fn go_2_current_or_next_nearest_search(&mut self) {
        if let Some((loc, _size)) = &self.search_locations {
            let mut current = 0;
            loop {
                if current >= loc.len() {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current];
                if self.cursor.get_y() < *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[0] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs {
                        if self.cursor.get_x() == *pos_x as u16 {
                            return;
                        }
                        if self.cursor.get_x() < *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current += 1;
            }
        }
    }

    pub fn go_2_current_or_prev_nearest_search(&mut self) {
        if let Some((loc, _size)) = &self.search_locations {
            let mut current = loc.len();
            loop {
                if current <= 0 {
                    break;
                }
                let (pos_y, pos_xs) = &loc[current - 1];
                if self.cursor.get_y() > *pos_y as u16 {
                    self.cursor.set_y(*pos_y as u16);
                    self.cursor.set_x(pos_xs[pos_xs.len() - 1] as u16);
                    break;
                }
                if self.cursor.get_y() == *pos_y as u16 {
                    for pos_x in pos_xs.into_iter().rev() {
                        if self.cursor.get_x() == *pos_x as u16 {
                            return;
                        }
                        if self.cursor.get_x() > *pos_x as u16 {
                            self.cursor.set_x(*pos_x as u16);
                            return;
                        }
                    }
                }
                current -= 1;
            }
        }
    }

    pub fn display_status_bar(
        &self,
        screen: &mut Screen,
        pass_display: &PassDisplay,
    ) -> Result<()> {
        let status_cursor = pass_display.origin.get_y() + pass_display.height;
        let std_msg = format!(
            "{} ({},{}) {} {}",
            if self.highlight.is_some() {
                "<hl>"
            } else {
                "<nm>"
            },
            self.cursor.get_y(),
            self.cursor.get_x(),
            "%%",
            // if self.dirty { "unsaved" } else { "saved" },
            self.dir.to_str().unwrap(),
        );
        screen.draw_status_bar_vertical(
            pass_display.origin.get_x(),
            status_cursor,
            pass_display.width,
        )?;
        screen.draw_on_status_vertical(&std_msg, status_cursor, pass_display.origin.get_x())
    }
    /*
       pub fn display(
           &mut self,
           screen: &mut Screen,
           focus: bool,
           pass_display: &PassDisplay,
       ) -> Result<()> {
           self.is_focus = focus;
           self.width = pass_display.width;
           self.height = pass_display.height;
           self.rowoff = pass_display.rowoff;
           self.coloff = pass_display.coloff;
           self.cursor = pass_display.cursor;

           let size_y = self.height;
           let start = self.rowoff as usize;
           let end = if (self.rowoff + size_y - 1) as usize > self.files.len() - 1 {
               self.files.len() - 1
           } else {
               (self.rowoff + size_y - 1) as usize
           };

           screen.draw_lines(
               &self.files[start..=end],
               pass_display,
               self.highlight,
               // None,
               self.search_locations.clone(), // None,
           )?;

           if self.is_focus {
               let (pos_x, pos_y) = self.cursor.get_pos();
               let new_pos_y = pos_y - self.rowoff;
               let new_pos_x = pos_x - self.coloff;
               let custom_pos = Position::new((new_pos_x, new_pos_y));
               screen.render_cursor(&custom_pos)?;
           }
           self.display_status_bar(screen, pass_display)?;
           screen.flush().unwrap();
           Ok(())
       }
    */

    pub fn get_text_rows_imm_ref(&self) -> &[Row] {
        return &self.files;
    }

    pub fn adjust_search_cursor(&mut self, input: String, positions: Vec<(usize, Vec<usize>)>) {
        if input.is_empty() {
            self.search_locations = None;
            return;
        }

        if positions.is_empty() {
            self.pending = None;
            self.message = MessageStatus::DrawMessage(String::from("No result found"));
            self.search_locations = None;
        } else {
            self.search_locations = Some((positions, input.len()));
        }
    }

    /*
    pub fn accept_input(&mut self, input: String) -> Result<()> {
        self.minibuffer_input = input;
        // let positions: HashMap<usize, Vec<usize>> = search(&self.files, &self.minibuffer_input);
        let positions: Vec<(usize, Vec<usize>)> = search(&self.files, &self.minibuffer_input);
        // , &self.cursor);

        match self.pending {
            Some(Status::Search) => {
                if positions.is_empty() {
                    self.pending = None;
                    self.message = MessageStatus::DrawMessage(String::from("No result found"));
                    return Ok(());
                } else {
                    for (row_index, row) in self.files.iter_mut().enumerate() {
                        if let Some(cursor_row) = positions.get(&row_index) {
                            row.update_syntax(Some((
                                cursor_row.to_vec(),
                                self.minibuffer_input.len(),
                            )));
                        } else {
                            row.update_syntax(None);
                        }
                    }

                    return Ok(());
                }
            }
            _ => {
                return Ok(());
            }
        }
    } */

    pub fn process_search_keypress(
        &mut self,
        key: &Key,
        positions: &HashMap<usize, Vec<usize>>,
    ) -> Windows {
        match key {
            ctrl!('s') => Windows::Current,
            Key::Normal(RegularKey::Char('n')) => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                'outer: for new_y in pos_y..self.files.len() as u16 {
                    if let Some(vec_x) = positions.get(&(new_y as usize)) {
                        let mut row_follow = 0;
                        'inner: loop {
                            if let Some(new_x) = vec_x.get(row_follow) {
                                if pos_y == new_y {
                                    if new_x.to_owned() <= pos_x.into() {
                                        row_follow = row_follow + 1;
                                        continue 'inner;
                                    } else {
                                        self.cursor.set_pos((new_x.to_owned() as u16, pos_y));
                                        break 'outer;
                                    }
                                } else {
                                    self.cursor.set_pos((new_x.to_owned() as u16, new_y));
                                    break 'outer;
                                }
                            } else {
                                continue 'outer;
                            }
                        }
                    } else {
                        continue 'outer;
                    }
                }
                let (lastest_x, lastest_y) = self.cursor.get_pos();
                if pos_x == lastest_x && pos_y == lastest_y {
                    self.message = MessageStatus::DrawMessage("Search stops here".to_string());
                }
                Windows::Current
            }

            Key::Shift(RegularKey::Char('N')) => {
                // normal!('N') => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                'outer: for new_y in (0..=pos_y).rev() {
                    if let Some(vec_x) = positions.get(&(new_y as usize)) {
                        'inner: for new_x in vec_x.iter().rev() {
                            if pos_y == new_y {
                                if pos_x as usize <= new_x.to_owned() {
                                    continue 'inner;
                                } else {
                                    self.cursor.set_pos((new_x.to_owned() as u16, pos_y));
                                    break 'outer;
                                }
                            } else {
                                self.cursor.set_pos((new_x.to_owned() as u16, new_y));
                                break 'outer;
                            }
                        }
                    } else {
                        continue 'outer;
                    }
                }
                let (lastest_x, lastest_y) = self.cursor.get_pos();
                if pos_x == lastest_x && pos_y == lastest_y {
                    self.message = MessageStatus::DrawMessage("Search stops here".to_string());
                }
                Windows::Current
            }

            ctrl!('g') | Key::Normal(RegularKey::Enter) | Key::Normal(RegularKey::Esc) => {
                for row in self.files.iter_mut() {
                    row.update_syntax();
                }

                self.pending = None;
                Windows::Current
            }
            _ => Windows::Current,
        }
    }

    pub fn is_empty_cursor_positions_search_status(&self) -> bool {
        self.search_locations.is_none()
    }

    pub fn keypress_handling(
        &mut self,
        key: &Key,
        comb_key: Option<Key>,
        cursor: Position,
    ) -> Windows {
        self.cursor = cursor;

        self.comb_key = comb_key;
        // match self.pending {
        //     Some(Status::Search) => {
        //         let positions: HashMap<usize, Vec<usize>> =
        //             search(&self.files, &self.minibuffer_input);
        //         // , &self.cursor);
        //         if positions.is_empty() {
        //             self.pending = None;
        //             self.message =
        //                 MessageStatus::DrawMessage(String::from("vamshi!! no result found"));
        //             return Windows::Current;
        //         }

        //         return self.process_search_keypress(key, &positions);
        //     }
        // None =>
        match self.comb_key {
            None => self.process_keypress(key),
            Some(Key::Ctrl(RegularKey::Char('x'))) => self.process_ctrl_x_keypress(key),
            _ => Windows::Current,
        }
        // _ => Windows::Current,
        // }
    }

    // current row length
    pub fn cur_row_len(&self) -> u16 {
        let total_len = self.files.len();
        let (_, pos_y) = self.cursor.get_pos();
        if !self.cursor.above(total_len) {
            0
        } else {
            self.files[pos_y as usize].len as u16
        }
    }
    // prev row length
    pub fn prev_row_len(&self) -> u16 {
        let total_len = self.files.len();
        let (_, pos_y) = self.cursor.get_pos();
        if !self.cursor.above(total_len) {
            0
        } else {
            self.files[pos_y.saturating_sub(1) as usize].len as u16
        }
    }

    // next row length
    pub fn next_row_len(&self) -> u16 {
        let total_len = self.files.len();
        let (_, pos_y) = self.cursor.get_pos();
        if !self.cursor.above(total_len) {
            0
        } else if pos_y.saturating_add(1) as usize == total_len {
            0
        } else {
            self.files[pos_y.saturating_add(1) as usize].len as u16
        }
    }

    pub fn handle_shift_arrow_keys(&mut self, reg_key: &RegularKey) {
        match reg_key {
            RegularKey::Up | RegularKey::Down | RegularKey::Left | RegularKey::Right => {
                let cur_pos = self.cursor.get_pos_as_pos();
                self.handle_arrow_keys(reg_key);
                let end_pos = self.cursor.get_pos_as_pos();
                match self.highlight {
                    Some((start, _)) => {
                        self.highlight = Some((start, end_pos));
                    }
                    None => {
                        self.highlight = Some((cur_pos, end_pos));
                    }
                }
            }
            _ => (),
        }
    }

    pub fn handle_arrow_keys(&mut self, reg_key: &RegularKey) {
        let rowlen = self.cur_row_len();
        match reg_key {
            RegularKey::Up => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let prev_rowlen = self.prev_row_len();
                if pos_x > prev_rowlen {
                    self.cursor.set_pos((prev_rowlen, pos_y.saturating_sub(1)))
                } else {
                    self.cursor.set_pos((pos_x, pos_y.saturating_sub(1)))
                }
            }
            RegularKey::Down => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let next_rowlen = self.next_row_len();

                if usize::from(pos_y) >= self.files.len() - 1 {
                    // Ok(())
                } else if pos_x > next_rowlen {
                    self.cursor.set_pos((next_rowlen, pos_y.saturating_add(1)))
                } else {
                    self.cursor.set_pos((pos_x, pos_y.saturating_add(1)))
                }
            }
            RegularKey::Right => {
                let (pos_x, pos_y) = self.cursor.get_pos();

                if pos_x >= rowlen {
                    if usize::from(pos_y) >= self.files.len() - 1 {
                        // Ok(())
                    } else {
                        self.cursor.set_pos((0, pos_y + 1))
                    }
                } else {
                    self.cursor.set_pos((pos_x + 1, pos_y))
                }
            }
            RegularKey::Left => {
                let (pos_x, pos_y) = self.cursor.get_pos();
                let prev_rowlen = self.prev_row_len();
                if pos_x == 0 {
                    if pos_y > 0 {
                        self.cursor.set_pos((prev_rowlen, pos_y.saturating_sub(1)))
                    } else {
                        // Ok(())
                    }
                } else {
                    self.cursor.set_pos((pos_x - 1, pos_y))
                }
            }
            _ => (),
        }
    }

    pub fn draw_cursor(
        &mut self,
        screen: &mut Screen,
        pass_display: &PassDisplay,
        // cursor: Position,
        // origin: Position,
        // width: u16,
        // height: u16,
        // rowoff: u16,
        // coloff: u16,
    ) -> Result<()> {
        self.rowoff = pass_display.rowoff;
        self.coloff = pass_display.coloff;
        // self.width = width;
        // self.height = height;
        self.cursor = pass_display.cursor;
        let (pos_x, pos_y) = self.cursor.get_pos();
        let (og_x, og_y) = pass_display.origin.get_pos();
        let new_pos_y = og_y + pos_y - self.rowoff;
        let new_pos_x = og_x + pos_x - self.coloff;
        let custom_pos = Position::new((new_pos_x, new_pos_y));

        screen.render_cursor(&custom_pos)?;
        screen.flush()
    }

    pub fn operation_of_return_key(&mut self) -> Windows {
        let (_pos_x, pos_y) = self.cursor.get_pos();

        if pos_y == 0 {
            return Windows::Current;
        }

        let current_string = self.files[pos_y as usize].get_string();
        if current_string == "." {
            self.dir = std::fs::canonicalize(&self.dir).expect("Failed to canonicalize");
            Windows::Current
        } else if current_string == ".." {
            self.dir = get_directory_from_file_path(&self.dir).unwrap();
            self.dir = std::fs::canonicalize(&self.dir).expect("Failed to canonicalize");

            let mut ls_files = path_buf_list_files_in_directory(&self.dir).unwrap();
            let mandatory_elem = vec![
                self.dir.display().to_string(),
                String::from("."),
                String::from(".."),
            ];
            ls_files.splice(0..0, mandatory_elem);
            let mut rows = Vec::new();
            for row in ls_files {
                rows.push(Row::new_with_lang(row, Language::Dired));
            }

            self.files = rows;
            self.cursor.set_pos((0, 0));

            Windows::Current
        } else if current_string.ends_with("/") {
            // self.dir = add_dir(&self.dir, current_string).unwrap();
            // self.dir = std::fs::canonicalize(&self.dir).expect("Failed to canonicalize");
            // match path_buf_list_files_in_directory(&self.dir) {
            //     Ok(mut res_files) => {
            //         let mandatory_elem = vec![
            //             self.dir.display().to_string(),
            //             String::from("."),
            //             String::from(".."),
            //         ];
            //         res_files.splice(0..0, mandatory_elem);
            //         let mut rows = Vec::new();
            //         for row in res_files {
            //             rows.push(Row::new_with_lang(row, Language::Dired));
            //         }

            //         self.files = rows;
            //         self.cursor.set_pos((0, 0));
            //     }
            //     Err(_) => {}
            // }
            let parent_dir = add_dir(&self.dir, current_string).unwrap();
            Windows::FileManager(parent_dir)
        } else {
            let new_file = add_file(&self.dir, current_string).unwrap();
            Windows::TextEditor(new_file.into())
        }
    }

    pub fn doc_len(&self) -> usize {
        return self.files.len();
    }

    pub fn get_doc_range(&self, start: usize, end: usize) -> &[Row] {
        return &self.files[start..=end];
    }

    // function for scrolling rows
    pub fn scroll(&mut self, screen: &Screen) {
        let (pos_x, pos_y) = self.cursor.get_pos();
        let (size_x, size_y) = screen.bounds().get_pos();
        if pos_y < self.rowoff {
            self.rowoff = pos_y;
        } else if pos_y >= self.rowoff + size_y {
            self.rowoff = pos_y - size_y + 1;
        }
        if pos_x < self.coloff {
            self.coloff = pos_x;
        } else if pos_x >= self.coloff + size_x {
            self.coloff = pos_x - size_x + 1;
        }
    }

    fn goto_previous_dir(&mut self) -> Windows {
        self.dir = get_directory_from_file_path(&self.dir).unwrap();
        self.dir = std::fs::canonicalize(&self.dir).expect("Failed to canonicalize");

        let mut ls_files = path_buf_list_files_in_directory(&self.dir).unwrap();
        let mandatory_elem = vec![
            self.dir.display().to_string(),
            String::from("."),
            String::from(".."),
        ];
        ls_files.splice(0..0, mandatory_elem);
        let mut rows = Vec::new();
        for row in ls_files {
            rows.push(Row::new_with_lang(row, Language::Dired));
        }

        self.files = rows;
        self.cursor.set_pos((0, 0));

        Windows::Current
    }

    pub fn process_ctrl_x_keypress(&mut self, key: &Key) -> Windows {
        match key {
            ctrl!('g') => {
                self.comb_key = None;
                Windows::Current
            }

            ctrl!('f') => {
                self.comb_key = None;
                Windows::Current
            }
            Key::Normal(RegularKey::Char('j')) => {
                self.comb_key = None;
                self.goto_previous_dir()
            }

            _ => {
                self.comb_key = None;
                Windows::Current
            }
        }
    }

    pub fn goto_beginning(&mut self) {
        let total_len = self.files.len();
        if self.cursor.above(total_len) {
            self.cursor.set_x(0)
        }
    }

    pub fn goto_start(&mut self) {
        self.cursor.set_pos((0, 0))
    }

    pub fn goto_end_of_file(&mut self) {
        self.cursor.set_pos((0, (self.files.len() - 1) as u16))
    }

    pub fn goto_end(&mut self) {
        let cur_row_length = self.cur_row_len();
        if cur_row_length > 0 && !self.cursor.get_x() == cur_row_length {
            self.cursor.set_x(cur_row_length)
        }
    }

    pub fn handle_backspace(&mut self) {
        let (pos_x, pos_y) = self.cursor.get_pos();

        if pos_x != 0 {
            self.cursor.set_pos((pos_x - 1, pos_y))
        }
    }

    pub fn process_keypress(&mut self, key: &Key) -> Windows {
        match key {
            meta!('<') => {
                self.goto_start();
                Windows::Current
            }
            ctrl!('n') => {
                self.message = MessageStatus::ClearMessage;
                self.handle_arrow_keys(&RegularKey::Down);
                Windows::Current
            }
            ctrl!('p') => {
                self.handle_arrow_keys(&RegularKey::Up);
                Windows::Current
            }
            ctrl!('b') => {
                self.handle_arrow_keys(&RegularKey::Left);
                Windows::Current
            }
            ctrl!('f') => {
                self.handle_arrow_keys(&RegularKey::Right);
                Windows::Current
            }
            meta!('>') => {
                self.goto_end_of_file();
                Windows::Current
            }
            ctrl!('g') | ctrl!('q') => Windows::Exit,
            // ctrl!('s') => {
            // self.pending = Some(Status::Search);

            // self.is_focus = false;

            // Windows::Minibuffer(MiniResult::Prompt(vec!["Find something: ".to_string()]))
            // }
            // KeyEvent {
            //     code: KeyCode::Char('f'),
            //     modifiers: KeyModifiers::CONTROL,
            //     ..
            // } => Windows::Current,
            //
            // KeyEvent {
            //     code: KeyCode::Char('x'),
            //     modifiers: KeyModifiers::CONTROL,
            //     kind: key_kind,
            //     state: key_state,
            // } => {
            ctrl!('x') => {
                // self.comb_key = Some(KeyEvent {
                //     code: KeyCode::Char('x'),
                //     modifiers: KeyModifiers::CONTROL,
                //     kind: *key_kind,
                //     state: *key_state,
                // });
                self.comb_key = Some(ctrl!('x'));
                Windows::Current
            }
            Key::Normal(RegularKey::Char(_)) => {
                self.highlight = None;
                Windows::Current
            }

            Key::Normal(RegularKey::Enter) => self.operation_of_return_key(),
            ctrl!(Backspace) => {
                self.handle_backspace();
                Windows::Current
            }
            // KeyEvent {
            //     code: KeyCode::Home,
            //     ..
            // } => {
            //     self.goto_beginning();
            //     Windows::Current
            // }
            //
            // KeyEvent {
            //     code: KeyCode::End, ..
            // } => {
            //     self.goto_end();
            //     Windows::Current
            // }

            // KeyEvent {
            //     code: key,
            //     modifiers: KeyModifiers::SHIFT,
            //     ..
            // } => {
            Key::Shift(reg_key) => {
                self.handle_shift_arrow_keys(reg_key);
                Windows::Current
            }

            Key::Normal(reg_key) => {
                // KeyEvent { code: key, .. } => {
                self.handle_arrow_keys(reg_key);
                self.highlight = None;
                Windows::Current
            }
            _ => Windows::Current,
        }
    }
}

fn add_dir(path_buf: &PathBuf, additional_path: String) -> Option<PathBuf> {
    let mut new_path = path_buf.clone();
    new_path.push(additional_path);
    if new_path.is_dir() {
        Some(new_path)
    } else {
        None
    }
}

fn add_file(path_buf: &PathBuf, additional_path: String) -> Option<PathBuf> {
    let mut new_path = path_buf.clone();
    new_path.push(additional_path);
    if new_path.is_file() {
        Some(new_path)
    } else {
        None
    }
}

pub fn path_buf_list_files_in_directory(directory_path: &PathBuf) -> Result<Vec<String>> {
    // Read the contents of the directory
    let entries = fs::read_dir(directory_path)?;
    let mut file_names = Vec::new();

    for entry in entries {
        let entry_unwrapped = entry?;
        let mut file_name = entry_unwrapped.file_name().to_string_lossy().into_owned();
        if entry_unwrapped.file_type()?.is_dir() {
            file_name.push('/');
            file_names.push(file_name);
        } else {
            file_names.push(file_name);
        }
    }
    Ok(file_names)
}
