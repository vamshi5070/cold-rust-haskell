use crossterm::style::Print;
use crossterm::style::{
    self,
    Color::{Black, Blue},
    Colors, ResetColor, SetColors, Stylize,
};
use crossterm::terminal;

use ratatui::{
    style::{Color, Style},
    DefaultTerminal,
};

use ratatui::{prelude::*, widgets::*};

use std::io::{stdout, Stdout, Write};

use crate::display_window::PassDisplay;
use crate::my_lib;
use crate::my_lib::Position;
use crate::my_lib::{LineCell, RenderBuffer};

use crossterm::ExecutableCommand;
use crossterm::{cursor, QueueableCommand, Result};

fn convert_buffer_to_text<'a>(renderbuffer: &'a RenderBuffer) -> Text<'a> {
    let para_text: Vec<Line> = renderbuffer
        .cells
        .iter()
        .map(|cell| convert_cell_to_line(cell))
        .collect();

    Text::from(para_text).style(
        Style::default()
            .fg(Color::Rgb(128, 192, 255))
            .bg(Color::Black),
    )
}

fn loop_same(hl_vec: &[my_lib::Style], hl: my_lib::Style, start: usize) -> usize {
    let mut end = start + 1;
    loop {
        if end >= hl_vec.len() {
            break;
        }
        if hl != hl_vec[end].clone() {
            break;
        }
        end += 1;
    }
    end
}

fn make_status_bar_rect(pass_display: &PassDisplay) -> Rect {
    let x = pass_display.origin.get_x();
    let y = pass_display.origin.get_y() + pass_display.height;
    let width = pass_display.width;
    let height = 1;
    Rect::new(x, y, width, height)
}

fn make_rect(pass_display: &PassDisplay) -> Rect {
    let x = pass_display.origin.get_x();
    let y = pass_display.origin.get_y();
    let width = pass_display.width;
    let height = pass_display.height;
    Rect::new(x, y, width, height)
}

fn get_cursor_position(pass_display: &PassDisplay) -> ratatui::layout::Position {
    let (pos_x, pos_y) = pass_display.cursor.get_pos();
    let (og_x, og_y) = pass_display.origin.get_pos();

    let new_pos_y = og_y + pos_y - pass_display.rowoff;
    let new_pos_x = og_x + pos_x - pass_display.coloff;
    ratatui::layout::Position {
        x: new_pos_x,
        y: new_pos_y,
    }
}

fn convert_cell_to_line<'a>(render_cell: &'a LineCell) -> Line<'a> {
    let mut current = 0;
    let mut vec_span_styled: Vec<Span> = Vec::new();
    loop {
        if current >= render_cell.styles.len() {
            break;
        }
        let current_style = render_cell.styles[current].clone();
        let end = loop_same(&render_cell.styles, current_style.clone(), current);
        let str_value = &render_cell.line[current..end];

        let local_span = Span::styled(
            str_value,
            Style::new()
                .fg(crossterm_to_ratatui_color(current_style.fg))
                .bg(crossterm_to_ratatui_color(current_style.bg)),
        );
        vec_span_styled.push(local_span);
        current = end;
    }
    return Line::from(vec_span_styled);
}

fn crossterm_to_ratatui_color(crossterm_color: crossterm::style::Color) -> ratatui::style::Color {
    match crossterm_color {
        crossterm::style::Color::Black => ratatui::style::Color::Black,
        crossterm::style::Color::Red => ratatui::style::Color::Red,
        crossterm::style::Color::Green => ratatui::style::Color::Green,
        crossterm::style::Color::Yellow => ratatui::style::Color::Yellow,
        crossterm::style::Color::Blue => ratatui::style::Color::Blue,
        crossterm::style::Color::Magenta => ratatui::style::Color::Magenta,
        crossterm::style::Color::Cyan => ratatui::style::Color::Cyan,
        crossterm::style::Color::Grey => ratatui::style::Color::Gray,
        crossterm::style::Color::White => ratatui::style::Color::White,
        crossterm::style::Color::Rgb {
            r: red,
            g: green,
            b: blue,
        } => ratatui::style::Color::Rgb(red, green, blue),
        _ => ratatui::style::Color::Reset, // Default case
    }
}

pub struct Screen {
    stdout: Stdout,
    terminal: DefaultTerminal,
    screen_width: u16,
    screen_height: u16,
}

impl Screen {
    pub fn new() -> Result<Self> {
        let (columns, rows) = terminal::size()?;
        Ok(Self {
            stdout: stdout(),
            terminal: ratatui::init(),
            screen_width: columns,
            screen_height: rows,
        })
    }

    pub fn enter_alternate_screen(&mut self) -> Result<()> {
        self.stdout.execute(terminal::EnterAlternateScreen)?;
        Ok(())
    }

    pub fn leave_alternate_screen(&mut self) -> Result<()> {
        self.stdout.execute(terminal::LeaveAlternateScreen)?;
        Ok(())
    }

    pub fn grand_render(
        &mut self,
        renderbuffer: &RenderBuffer,
        status_bar: &LineCell,
        pass_display: &PassDisplay,
    ) {
        let widget_area = make_rect(pass_display);
        let my_paragraph = convert_buffer_to_text(renderbuffer);

        let status_widget_area = make_status_bar_rect(pass_display);
        let status_line = convert_cell_to_line(status_bar).style(
            Style::default()
                .fg(Color::Rgb(16, 16, 16))
                .bg(Color::Rgb(92, 92, 128)),
        );
        let rat_position = get_cursor_position(pass_display);
        self.terminal
            .draw(|frame| {
                frame.render_widget(my_paragraph.clone(), widget_area);
                frame.render_widget(status_line.clone(), status_widget_area);
                frame.set_cursor_position(rat_position);
            })
            .expect("failed to draw");
    }

    pub fn grand_render_list(
        &mut self,
        list: &[(RenderBuffer, LineCell, PassDisplay)],
        cursor_index: usize,
    ) {
        self.terminal
            .draw(|frame| {
                let mut cur_index = 1;
                for (renderbuffer, status_bar, pass_display) in list {
                    let widget_area = make_rect(pass_display);
                    let my_text = convert_buffer_to_text(renderbuffer);

                    let status_widget_area = make_status_bar_rect(pass_display);
                    let status_line = convert_cell_to_line(status_bar).style(
                        Style::default()
                            .fg(Color::Rgb(16, 16, 16))
                            .bg(Color::Rgb(92, 92, 128)),
                    );

                    frame.render_widget(my_text.clone(), widget_area);
                    frame.render_widget(status_line.clone(), status_widget_area);
                    if cursor_index == cur_index {
                        let rat_position = get_cursor_position(pass_display);
                        frame.set_cursor_position(rat_position);
                    }

                    cur_index += 1;
                }
            })
            .expect("failed to draw");
    }

    pub fn render_minibuffer_with_buffer(
        &mut self,
        renderbuffer: &RenderBuffer,
        status_bar: &LineCell,
        pass_display: &mut PassDisplay,
        minibuffer: &RenderBuffer,
        minibuffer_pd: &PassDisplay,
        terminal_size_y: usize,
    ) {
        let minibuffer_rect = Rect::new(
            0,
            terminal_size_y as u16 - minibuffer_pd.height,
            minibuffer_pd.width,
            // terminal_size_x as u16,
            minibuffer_pd.height,
        );
        pass_display.height = pass_display.height - minibuffer_pd.height + 1;
        let widget_area = make_rect(pass_display);
        let my_paragraph = convert_buffer_to_text(renderbuffer);
        let status_widget_area = make_status_bar_rect(pass_display);
        let rat_position = get_cursor_position(pass_display);
        let minibuffer_line = convert_buffer_to_text(minibuffer).style(
            Style::default()
                .fg(Color::Rgb(16, 16, 16))
                .bg(Color::Rgb(92, 92, 128)),
        );

        let status_line = convert_cell_to_line(status_bar).style(
            Style::default()
                .fg(Color::Rgb(16, 16, 16))
                .bg(Color::Rgb(92, 92, 128)),
        );

        let _ = self.terminal.draw(|frame| {
            frame.render_widget(minibuffer_line, minibuffer_rect);
            frame.render_widget(my_paragraph.clone(), widget_area);
            frame.render_widget(status_line.clone(), status_widget_area);
            frame.set_cursor_position(rat_position);
        });
    }

    pub fn grand_render_list_msg(
        &mut self,
        list: &[(RenderBuffer, LineCell, PassDisplay)],
        cursor_index: usize,
        msg: &LineCell,
        terminal_size_y: usize,
    ) {
        let msg_rect = Rect::new(0, terminal_size_y as u16 - 1, terminal_size_y as u16, 1);
        let msg_line = convert_cell_to_line(msg).style(
            Style::default(), // .fg(Color::Rgb(16, 16, 16))
                              // .bg(Color::Rgb(92, 92, 128)),
        );

        let mut cur_index = 1;

        self.terminal
            .draw(|frame| {
                for (renderbuffer, status_bar, pass_display) in list {
                    let widget_area = make_rect(pass_display);
                    let my_paragraph = convert_buffer_to_text(renderbuffer);

                    let status_widget_area = make_status_bar_rect(pass_display);
                    let status_line = convert_cell_to_line(status_bar).style(
                        Style::default()
                            .fg(Color::Rgb(16, 16, 16))
                            .bg(Color::Rgb(92, 92, 128)),
                    );

                    frame.render_widget(my_paragraph.clone(), widget_area);
                    frame.render_widget(status_line.clone(), status_widget_area);
                    if cursor_index == cur_index {
                        let rat_position = get_cursor_position(pass_display);
                        frame.set_cursor_position(rat_position);
                    }

                    cur_index += 1;
                }
                frame.render_widget(msg_line, msg_rect);
            })
            .expect("failed to draw");
    }

    pub fn render(&mut self, renderbuffer: &RenderBuffer, pass_display: &PassDisplay) {
        let widget_area = make_rect(pass_display);
        let my_paragraph = convert_buffer_to_text(renderbuffer);
        self.terminal
            .draw(|frame| frame.render_widget(my_paragraph.clone(), widget_area))
            .expect("failed to draw");
        // frame.area()))
    }

    pub fn render_status_bar(&mut self, status_bar: &LineCell, pass_display: &PassDisplay) {
        let widget_area = make_status_bar_rect(pass_display);
        let status_line = convert_cell_to_line(status_bar);
        self.terminal
            .draw(|frame| frame.render_widget(status_line.clone(), widget_area))
            .expect("failed to draw");
    }

    pub fn render_pure_crossterm(&mut self, render_buffer: &RenderBuffer) -> Result<()> {
        for (cell_index, cell) in render_buffer.cells.iter().enumerate() {
            self.stdout.execute(cursor::MoveTo(0, cell_index as u16))?;
            self.render_cell_pure_crossterm(&cell)?;
        }
        Ok(())
    }

    fn render_cell_pure_crossterm(&mut self, render_cell: &LineCell) -> Result<()> {
        let mut current = 0;
        loop {
            if current >= render_cell.styles.len() {
                break;
            }
            let current_style = render_cell.styles[current].clone();
            // let end = current + 1;
            let end = self.loop_same(&render_cell.styles, current_style.clone(), current);
            // let bg = current_style.bg;
            self.stdout
                .queue(style::SetBackgroundColor(current_style.bg))?
                .queue(style::SetForegroundColor(current_style.fg))?;
            if current_style.bold {
                self.stdout
                    .execute(Print(render_cell.line[current..end].to_string().bold()))?;
            } else {
                self.stdout
                    .execute(Print(render_cell.line[current..end].to_string()))?;
            }
            current = end;
        }
        Ok(())
    }

    pub fn clear_given_line(&mut self, pos_x: u16, pos_y: u16) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?;
        return Ok(());
    }

    fn loop_same(&self, hl_vec: &[my_lib::Style], hl: my_lib::Style, start: usize) -> usize {
        let mut end = start + 1;
        loop {
            if end >= hl_vec.len() {
                break;
            }
            if hl != hl_vec[end].clone() {
                break;
            }
            end += 1;
        }
        return end;
    }

    /*
       pub fn render_text_syntax(
           &mut self,
           rowoff: u16,
           screen_pos_y: u16,
           raw_str: &str,
           hl_vec: &[my_lib::Style],
           _coloff: u16,
           og_x: u16,
           maybe_highlight: Option<(Position, Position)>,
           maybe_locations: Option<(&[usize], usize)>,
       ) -> Result<()> {
           let mut hl_index = 0;
           let pos_y = screen_pos_y + rowoff;
           let transparent = Color::Rgb {
               r: 155,
               g: 155,
               b: 155,
           };

           let search_cyan = Color::Rgb {
               r: 205,
               g: 205,
               b: 155,
           };

           // let mut my_bg = Black;
           if let Some((start_cursor, end_cursor)) = maybe_highlight {
               let (first_x, first_y) = start_cursor.get_pos();
               let (second_x, second_y) = end_cursor.get_pos();
               // if pos_y > first_y && pos_y < second_y {
               // let my_bg = transparent;
               // }

               if pos_y == first_y && pos_y == second_y {
                   loop {
                       if hl_index >= hl_vec.len() {
                           break;
                       }
                       let hl = hl_vec[hl_index].clone();
                       let fg_color = hl.fg;
                       let end = hl_index + 1;
                       let bg = if hl_index >= first_x as usize && hl_index <= second_x as usize {
                           transparent
                       } else {
                           Black
                       };
                       self.stdout
                           .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                           .queue(SetColors(Colors::new(fg_color, bg)))?
                           .queue(Print(raw_str[hl_index..end].to_owned()))?
                           .queue(ResetColor)?;
                       hl_index = end;
                   }
                   return Ok(());
               }
               if pos_y == first_y {
                   loop {
                       if hl_index >= hl_vec.len() {
                           break;
                       }
                       let hl = hl_vec[hl_index].clone();
                       let fg_color = hl.fg;
                       let end = hl_index + 1;
                       let bg = if hl_index >= first_x as usize {
                           transparent
                       } else {
                           Black
                       };
                       self.stdout
                           .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                           .queue(SetColors(Colors::new(fg_color, bg)))?
                           .queue(Print(raw_str[hl_index..end].to_owned()))?
                           .queue(ResetColor)?;
                       hl_index = end;
                   }
                   return Ok(());
               }
               if pos_y == second_y {
                   loop {
                       if hl_index >= hl_vec.len() {
                           break;
                       }
                       let hl = hl_vec[hl_index].clone();
                       let fg_color = hl.fg;
                       let end = hl_index + 1;
                       let bg = if hl_index <= second_x as usize {
                           transparent
                       } else {
                           Black
                       };
                       self.stdout
                           .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                           .queue(SetColors(Colors::new(fg_color, bg)))?
                           .queue(Print(raw_str[hl_index..end].to_owned()))?
                           .queue(ResetColor)?;
                       hl_index = end;
                   }
                   return Ok(());
               }
           }
           loop {
               if hl_index >= hl_vec.len() {
                   break;
               }
               let hl = hl_vec[hl_index].clone();
               let fg_color = hl.fg;
               let bg_color = hl.bg;
               let end = self.loop_same(hl_vec, hl, hl_index);
               self.stdout
                   .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                   .queue(SetColors(Colors::new(fg_color, bg_color)))?
                   .queue(Print(raw_str[hl_index..end].to_owned()))?
                   .queue(ResetColor)?;
               hl_index = end;
           }

           if let Some((vec_in_row, len)) = maybe_locations {
               for start_index in vec_in_row {
                   hl_index = *start_index;
                   loop {
                       if hl_index >= hl_vec.len() || hl_index >= start_index + len {
                           break;
                       }
                       let hl = hl_vec[hl_index].clone();
                       let fg_color = hl.fg;
                       // let end = self.loop_same(hl_vec, hl, hl_index);
                       let end = hl_index + 1;
                       self.stdout
                           .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                           .queue(SetColors(Colors::new(fg_color, search_cyan)))?
                           .queue(Print(raw_str[hl_index..end].to_owned()))?
                           .queue(ResetColor)?;
                       hl_index = end;
                   }
               }
           }
           Ok(())
       }

       pub fn render_text_syntax_on_hl(
           &mut self,
           rowoff: u16,
           screen_pos_y: u16,
           raw_str: &str,
           hl_vec: &[my_lib::Style],
           _coloff: u16,
           og_x: u16,
           maybe_highlight: Option<(Position, Position)>,
       ) -> Result<()> {
           let mut hl_index = 0;
           let transparent = Color::Rgb {
               r: 155,
               g: 155,
               b: 155,
           };
           let _global_bg = if let Some((beg_cursor, end_cursor)) = maybe_highlight {
               let pos_y = screen_pos_y + rowoff;
               let (_, first_y) = beg_cursor.get_pos();
               let (_, second_y) = end_cursor.get_pos();
               if pos_y > first_y && pos_y < second_y {
                   transparent
               } else {
                   Black
               }
           } else {
               Black
           };
           // let my_bg = global_bg;

           loop {
               if hl_index >= hl_vec.len() {
                   break;
               }
               let hl = hl_vec[hl_index].clone();
               let fg_color = hl.fg;
               let bg_color = hl.bg;
               let end = self.loop_same(hl_vec, hl, hl_index);
               self.stdout
                   .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                   .queue(SetColors(Colors::new(fg_color, bg_color)))?
                   .queue(Print(raw_str[hl_index..end].to_owned()))?
                   .queue(ResetColor)?;
               hl_index = end;
           }
           Ok(())
       }

       pub fn draw_strings_vertical(
           &mut self,
           rows: &[String],
           coloff: u16,
           height: u16,
       ) -> Result<()> {
           for (row_index, row) in rows.iter().enumerate() {
               let start = coloff as usize;
               let end = if row.len() > coloff as usize + self.screen_width as usize {
                   coloff as usize + self.screen_width as usize
               } else {
                   row.len()
               };
               let to_be_rendered = match row.get(start..end) {
                   Some(sub_row) => sub_row.to_string(),
                   None => "".to_string(),
               };
               self.stdout
                   .queue(cursor::MoveTo(
                       0,
                       (row_index as u16 + height).try_into().unwrap(),
                   ))?
                   .queue(terminal::Clear(terminal::ClearType::CurrentLine))?
                   .queue(Print(to_be_rendered))?;
           }
           Ok(())
       }

    */

    pub fn draw_strings_vertical_minibuffer(
        &mut self,
        prompt: &str,
        rows: &[String],
        pass_display: PassDisplay,
    ) -> Result<()> {
        self.hide_cursor();
        self.stdout
            .queue(cursor::MoveTo(
                0,
                // pass_display.origin.get_x(),
                pass_display.origin.get_y(),
            ))?
            .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
            .queue(Print(prompt.to_owned()))?;

        for (row_index, row) in rows.iter().enumerate() {
            let start = pass_display.coloff as usize;
            let end = if row.len() > pass_display.coloff as usize + self.screen_width as usize {
                pass_display.coloff as usize + self.screen_width as usize
            } else {
                row.len()
            };
            let to_be_rendered = match row.get(start..end) {
                Some(sub_row) => sub_row.to_string(),
                None => "".to_string(),
            };
            self.stdout
                .queue(cursor::MoveTo(
                    pass_display.origin.get_x(),
                    row_index as u16 + pass_display.origin.get_y(),
                    // (row_index as u16 + pass_display.height).try_into().unwrap(),
                ))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
                .queue(Print(to_be_rendered))?;
        }
        self.show_cursor();
        Ok(())
    }

    pub fn show_cursor(&mut self) {
        self.stdout.queue(cursor::Show).unwrap();
    }

    pub fn hide_cursor(&mut self) {
        self.stdout.queue(cursor::Hide).unwrap();
    }

    pub fn draw_message(&mut self, cursor_y: u16, msg: &String) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(0, cursor_y))?
            .queue(Print(msg))?;
        Ok(())
    }

    pub fn draw_on_status_vertical(&mut self, msg: &str, pos_y: u16, pos_x: u16) -> Result<()> {
        let start = 0;
        let end = if self.screen_width as usize <= msg.len() {
            self.screen_width as usize
        } else {
            msg.len()
        };
        let printable_str = msg[start..end].to_owned();
        self.stdout
            .queue(SetColors(Colors::new(Black, Blue)))?
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(Print(printable_str))?
            .queue(ResetColor)?;
        Ok(())
    }

    pub fn draw_status_bar_vertical(&mut self, pos_x: u16, pos_y: u16, width: u16) -> Result<()> {
        let mut status: String = String::from("");
        status.push_str(&" ".repeat(width.saturating_sub(0).into()));
        self.stdout
            .queue(SetColors(Colors::new(Blue, Black)))?
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(Print(status))?
            .queue(ResetColor)?;
        Ok(())
    }

    pub fn clear_all(&mut self) -> Result<()> {
        self.stdout
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?;
        Ok(())
    }

    pub fn clear_row(&mut self, cursor_y: u16) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(0, cursor_y))?
            .queue(terminal::Clear(terminal::ClearType::CurrentLine))?;

        Ok(())
    }

    pub fn flush(&mut self) -> Result<()> {
        self.stdout.flush()
    }

    pub fn cursor_position(&self) -> Result<(u16, u16)> {
        cursor::position()
    }

    // jump to given position
    pub fn render_cursor(&mut self, pos: &Position) -> Result<()> {
        let (posx, posy) = pos.get_pos();
        self.stdout.queue(cursor::MoveTo(posx, posy))?;
        Ok(())
    }

    pub fn bounds(&self) -> Position {
        Position::new((self.screen_width, self.screen_height))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::my_lib::Theme;
    // use crossterm::terminal;
    #[test]
    fn test_paragraph() {
        let renderbuffer = RenderBuffer::new(Theme, 10, 2);
        convert_buffer_to_text(&renderbuffer);
        // Screen::new()
        // assert_eq!(lhs, cursor);
    }
}
