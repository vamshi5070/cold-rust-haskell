use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

use crate::display_window::PassDisplay;
use crate::file_system::*;
// use crate::occur::OccurMode;
use crate::text_mode::*;
use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

use crossterm::style::Color;

pub type StdResult<T, E> = std::result::Result<T, E>;

pub enum ResultEditor {
    KeyReadFail,
}

#[derive(Clone, PartialEq)]
pub enum MessageStatus {
    DrawMessage(String),
    ClearMessage,
    NoChange,
}

#[derive(Default, Clone, Copy, PartialEq, Debug)]
pub struct Position {
    // x is horizontal cursor position with respect to the text editor (not physical screen)
    x: u16,
    // y is vertical cursor position with respect to the text editor (not physical screen)
    y: u16,
}

impl Position {
    pub fn above(&self, row: usize) -> bool {
        self.y < row as u16
    }

    // pub fn get_pos(&self) -> ()
    pub fn new((row, col): (u16, u16)) -> Self {
        Self { x: row, y: col }
    }

    pub fn set_x(&mut self, cursor_x: u16) {
        self.x = cursor_x;
    }

    pub fn set_y(&mut self, cursor_y: u16) {
        self.y = cursor_y;
    }

    pub fn get_x(&self) -> u16 {
        return self.x;
    }

    pub fn get_y(&self) -> u16 {
        return self.y;
    }

    pub fn set_pos(&mut self, (row, col): (u16, u16)) {
        self.x = row;
        self.y = col;
    }

    pub fn get_pos(&self) -> (u16, u16) {
        (self.x, self.y)
    }

    pub fn get_pos_as_pos(&self) -> Self {
        Self {
            x: self.x,
            y: self.y,
        }
    }

    pub fn is_equal(&self, another: Self) -> bool {
        self.x == another.get_x() && self.y == another.get_y()
    }

    pub fn compare(&self, another: Self) -> (Self, Self) {
        let (another_x, another_y) = another.get_pos();
        let first_pos;
        let second_pos;
        if self.y < another_y {
            first_pos = self.to_owned();
            second_pos = another;
        } else if self.y > another_y {
            first_pos = another;
            second_pos = self.to_owned();
        } else {
            if self.x < another_x {
                first_pos = self.to_owned();
                second_pos = another;
            } else {
                first_pos = another;
                second_pos = self.to_owned();
            }
        }
        (first_pos, second_pos)
    }

    pub fn set_pos_as_pos(&mut self, another: Self) {
        let (another_x, another_y) = another.get_pos();
        self.set_x(another_x);
        self.set_y(another_y);
    }
}

// #[derive(PartialEq, Debug)]
// pub struct PromptWithKind {
//     prompt: Vec<String>,
//     kind: MinibufferType,
// }
//
#[derive(PartialEq, Debug)]
pub enum MinibufferType {
    Multiple,
    Single,
}

// #[derive(PartialEq, Debug)]
// pub enum MiniResult {
//     Prompt(PromptWithKind),
//     Result(String),
//     Cancel,
// }

#[derive(Debug, PartialEq)]
pub enum Windows {
    TextEditor(PathBuf),
    FileManager(PathBuf),
    // Occur()
    Current,
    Exit,
}

pub enum Status {
    Search,
    // FileSave,
}

pub enum EditorOperation {
    Insert(Position, char),
    Delete(Position),
}

#[derive(Clone)]
pub enum WindManager {
    TextEditorMode(Rc<RefCell<TextMode>>),
    FileManagerMode(Rc<RefCell<FileSystem>>),
    // OccurMode(Rc<RefCell<OccurMode>>),
    Null,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Language {
    Rust,
    C,
    Fundemental,
    Dired,
}

impl Default for Language {
    fn default() -> Self {
        Language::Fundemental
    }
}

impl Language {
    pub fn show(&self) -> String {
        match self {
            Language::C => "C lang".to_string(),
            Language::Rust => "rust".to_string(),
            Language::Fundemental => String::from("Fundemental"),
            Language::Dired => String::from("Dired"),
        }
    }
}
// pub fn get_directory_from_file_path(file_path: &PathBuf) -> Option<PathBuf> {
//     if let Some(parent) = file_path.parent() {
//         Some(parent.to_path_buf())
//     } else {
//         None
//     }
// }

// function for scrolling rows
pub fn scroll(pass_display: &PassDisplay) -> (u16, u16) {
    let (pos_x, pos_y) = pass_display.cursor.get_pos();

    let mut new_rowoff: u16 = pass_display.rowoff;
    let mut new_coloff: u16 = pass_display.coloff;

    let size_x = pass_display.width;
    let size_y = pass_display.height;

    if pos_y < pass_display.rowoff {
        new_rowoff = pos_y;
    } else if pos_y >= pass_display.rowoff + size_y {
        new_rowoff = pos_y - size_y + 1;
    }
    if pos_x < pass_display.coloff {
        new_coloff = pos_x;
    } else if pos_x >= pass_display.coloff + size_x {
        new_coloff = pos_x - size_x + 1;
    }

    (new_rowoff, new_coloff)
}

#[derive(Clone, Copy)]
pub enum RegularKey {
    Char(char),
    Up,
    Down,
    Enter,
    Right,
    Left,
    Backspace,
    Home,
    End,
    Esc,
    None,
}

#[derive(Clone, Copy)]
pub enum Key {
    Normal(RegularKey),
    Shift(RegularKey),
    Ctrl(RegularKey),
    Meta(RegularKey),
}

pub fn helper(key_code: KeyCode) -> RegularKey {
    return match key_code {
        KeyCode::Char(ch) => RegularKey::Char(ch),
        KeyCode::Up => RegularKey::Up,
        KeyCode::Down => RegularKey::Down,
        KeyCode::Enter => RegularKey::Enter,
        KeyCode::Left => RegularKey::Left,
        KeyCode::Right => RegularKey::Right,
        KeyCode::Backspace => RegularKey::Backspace,
        KeyCode::Home => RegularKey::Home,
        KeyCode::End => RegularKey::End,
        KeyCode::Esc => RegularKey::Esc,
        _ => RegularKey::None,
    };
}

pub fn cross_key_to_cold(key_event: KeyEvent) -> Key {
    match key_event.modifiers {
        KeyModifiers::NONE => Key::Normal(helper(key_event.code)),
        KeyModifiers::SHIFT => Key::Shift(helper(key_event.code)),
        KeyModifiers::ALT => Key::Meta(helper(key_event.code)),
        KeyModifiers::CONTROL => Key::Ctrl(helper(key_event.code)),
        _ => Key::Normal(RegularKey::Enter),
    }
}

pub fn is_escape(key: &Key) -> bool {
    match key {
        Key::Ctrl(RegularKey::Char('g')) => true,
        _ => false,
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Highlight {
    Normal,
    NonAlphaNum,
    HlString, // since String is already there
    Number,
    Match,
    Comment,
    Keyword,
    Directory,
    Types,
    // Select,
}

pub fn my_is_seperator(c: char) -> bool {
    c.is_whitespace()
}

impl Highlight {
    /*    pub fn _syntax_to_color(&self) -> Style {
            match self {
                Highlight::Keyword => Style {
                    fg: Color::Green,
                    bg: Color::Black,
                    bold: false,
                    italic: false,
                },
                Highlight::Normal => Style {
                    fg: Color::White,
                    bg: Color::Black,
                    bold: false,
                    italic: false,
                },
                Highlight::NonAlphaNum => Style {
                    fg: Color::Magenta,
                    bg: Color::Black,
                    bold: true,
                    italic: false,
                },

                Highlight::Number => Style {
                    fg: Color::Red,
                    bg: Color::Black,
                    bold: false,
                    italic: false,
                },
                Highlight::Match => Style {
                    fg: Color::White,
                    bg: Color::Cyan,
                    bold: false,
                    italic: false,
                },
                // Color::White,
                Highlight::HlString => Style {
                    fg: Color::Magenta,
                    bg: Color::White,
                    bold: false,
                    italic: false,
                },
                // Color::Magenta,
                Highlight::Comment => Style {
                    fg: Color::Yellow,
                    bg: Color::White,
                    bold: false,
                    italic: false,
                },
                // Color::Yellow,
                Highlight::Directory => Style {
                    fg: Color::Blue,
                    bg: Color::White,
                    bold: false,
                    italic: false,
                },
                // Color::Blue,
                // Highlight::Select => Color::Rgb {
                // r: 155,
                // g: 155,
                // b: 155,
                // },
            }
        }
    */
    pub fn is_normal(&self) -> bool {
        self == &Highlight::Normal
    }
}

#[derive(PartialEq, Clone, Debug)]
pub struct Style {
    pub fg: Color,
    pub bg: Color,
    pub bold: bool,
    pub italic: bool,
}

#[derive(PartialEq, Debug, Clone)]
pub struct StyleInfo {
    pub start: usize,
    pub end: usize,
    pub style: Style,
}

#[derive(Debug, Clone)]
pub struct TokenStyle {
    #[allow(unused)]
    pub name: Option<String>,
    pub scope: Vec<String>,
    pub style: Style,
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct Theme;

impl Theme {
    pub fn default(&self) -> Style {
        Style {
            fg: Color::White,
            bg: Color::Black,
            bold: false,
            italic: false,
        }
    }

    pub fn msg_line(&self) -> Style {
        Style {
            fg: Color::White,
            bg: Color::Black,
            bold: true,
            italic: false,
        }
    }
    pub fn status_line(&self) -> Style {
        Style {
            fg: Color::Black,
            bg: Color::Blue,
            bold: true,
            italic: false,
        }
    }

    pub fn transparent_fg(&self) -> Style {
        Style {
            fg: Color::Rgb {
                r: 155,
                g: 155,
                b: 155,
            },
            bg: Color::Black,
            bold: false,
            italic: false,
        }
    }

    pub fn transparent_bg(&self) -> Style {
        Style {
            fg: Color::White,
            bg: Color::Rgb {
                r: 155,
                g: 155,
                b: 155,
            },
            bold: false,
            italic: false,
        }
    }

    pub fn search(&self) -> Style {
        Style {
            fg: Color::White,
            bg: Color::Yellow,
            bold: true,
            italic: false,
        }
    }

    pub fn hl_to_style(&self, hl: Highlight) -> Style {
        match hl {
            Highlight::Normal => Style {
                fg: Color::White,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            Highlight::Number => Style {
                fg: Color::Red,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            Highlight::Types => Style {
                fg: Color::Green,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            Highlight::Keyword => Style {
                fg: Color::Rgb {
                    r: 255,
                    g: 165,
                    b: 0,
                },
                // Green,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            Highlight::NonAlphaNum => Style {
                fg: Color::Cyan,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            Highlight::Comment => self.transparent_fg(),
            Highlight::HlString => Style {
                fg: Color::Magenta,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            _ => Style {
                fg: Color::White,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
        }
    }

    pub fn get_style(&self, what: &str) -> Style {
        match what {
            "string" => Style {
                fg: Color::Magenta,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
            _ => Style {
                fg: Color::White,
                bg: Color::Black,
                bold: false,
                italic: false,
            },
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct LineCell {
    pub line: String,
    pub styles: Vec<Style>,
    pub width: usize,
}

impl LineCell {
    pub fn new(text: &str, style: &Style, width: usize) -> LineCell {
        let mut styles = Vec::new();
        for _ in 0..text.len() {
            styles.push(style.clone());
        }
        Self {
            line: text.to_string(),
            styles,
            width,
        }
    }

    pub fn push_str(&mut self, text: &str, style: &Style) {
        if text.len() + self.line.len() > self.width {
            return;
        }
        self.line.push_str(text);
        for _ in 0..text.len() {
            self.styles.push(style.clone());
        }
    }

    pub fn replace_style_only_at(&mut self, styles: &[Style], at: usize) {
        for (x_index, x) in styles.iter().enumerate() {
            self.styles[at + x_index] = x.clone();
        }
        // let slice_1 = &self.styles[0..at];
        // let slice_2 = &self.styles[at..self.styles.len()];
        // let mut result: Vec<Style> = Vec::new();
        // result.extend(slice_1);
        // result.extend(styles);
        // result.extend(slice_2);
        // self.styles = result;
        // self.styles = slice_1 + styles + slice_2;
    }
}

#[derive(Clone)]
pub struct RenderBuffer {
    pub cells: Vec<LineCell>,
    pub height: usize,
    pub width: usize,
}

impl RenderBuffer {
    pub fn new(_theme: Theme, width: usize, height: usize) -> Self {
        let line_cells = Vec::new();

        Self {
            cells: line_cells,
            width,
            height,
        }
    }

    pub fn push_str_at(&mut self, text: &str, y: usize, style: &Style) {
        self.cells[y].push_str(text, style);
    }

    pub fn push_str_last(&mut self, text: &str, style: &Style) {
        let y = self.cells.len() - 1;
        self.cells[y].push_str(text, style);
    }

    pub fn replace_last_style_only_at(&mut self, styles: &[Style], at: usize) {
        let length = self.cells.len();

        self.cells[length - 1].replace_style_only_at(styles, at);
    }

    pub fn last_row_len(&self) -> usize {
        let length = self.cells.len();
        return self.cells[length - 1].line.len();
    }

    pub fn new_line(&mut self, text: &str, style: &Style) {
        if self.cells.len() > self.height {
            return;
        }
        let line_cell = LineCell::new(text, style, self.width);
        self.cells.push(line_cell);
    }
}

#[derive(PartialEq)]
pub enum Change {
    First,
    NoChange,
    ChangedLine(u16),
    Enter(u16),
    // ChangeCharAt(Position),
}

// dired
pub fn get_directory_from_file_path(file_path: &PathBuf) -> Option<PathBuf> {
    if let Some(parent) = file_path.parent() {
        Some(parent.to_path_buf())
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_new_buffer() {
        let fresh = RenderBuffer::new(Theme, 2, 2);
        let lhs_cells: Vec<LineCell> = vec![
            // LineCell::new("  ", &Theme::default(&Theme), 2),
            // LineCell::new("  ", &Theme::default(&Theme), 2),
        ];
        assert_eq!(lhs_cells.len(), fresh.cells.len());
        assert_eq!(lhs_cells, fresh.cells);
    }
    // }
}

// pub struct OccurMode {
//     text_rows:
// }
