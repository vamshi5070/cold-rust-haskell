use crate::display_window::*;
use crate::file_system::*;
use crate::keyboard::*;
use crate::minibuffer::*;
use crate::my_lib::*;
use crate::row::*;
use crate::search::*;
use crate::telescope::Telescope;
use errno::errno;
use std::cell::RefCell;
// use std::collections::HashMap;

use std::path::PathBuf;
use std::rc::Rc;

use crate::binary_tree::Tree;
use crate::screen::Screen;

use crate::text_mode::*;
use crossterm::terminal;

use crossterm::event::KeyEvent;

use crossterm::Result;

pub struct Editor {
    screen: Screen,     // handles screen events
    keyboard: Keyboard, // gives keyboard input
    // buffers: HashMap<PathBuf, WindManager>, // stores buffers with filename(pathbuf) as key
    message: MessageStatus,        // stores message
    comb_key: Option<Key>,         // None if no comb key else Some(comb_key)
    displays: Tree<DisplayWindow>, // binary tree of displays on screen
    telescope: Telescope,
    signal: Windows,             // kind of window message currently
    cursor_index: usize,         // where is the window cursor_index lying
    current_buffer: WindManager, // stores current buffer so that extraction is O(1)
    current_cursor: Position,
    size_y: u16,
    size_x: u16,
}

pub fn get_buffer_file(buffer: &WindManager) -> Option<PathBuf> {
    match &buffer {
        WindManager::TextEditorMode(tm) => Some(tm.borrow().get_file_path().to_path_buf()),
        WindManager::FileManagerMode(fm) => Some(fm.borrow().get_file_path().to_path_buf()),
        _ => None,
    }
}

fn call_displays(
    screen: &mut Screen,
    t: Tree<DisplayWindow>,
    cur_index: u16,
    cursor_index: u16,
    temp_cursor: Position,
) -> (u16, Tree<DisplayWindow>) {
    match t {
        Tree::Leaf(mut cur_display) => {
            if cur_index == cursor_index {
                let _ = cur_display.display(screen, Some(temp_cursor));
            } else {
                let _ = cur_display.display(screen, None);
            }
            (cur_index + 1, Tree::Leaf(cur_display))
        }
        Tree::Node { left, right } => {
            let (l_index, l_tree) =
                call_displays(screen, *left, cur_index, cursor_index, temp_cursor);
            let (r_index, r_tree) =
                call_displays(screen, *right, l_index, cursor_index, temp_cursor);
            (
                r_index,
                Tree::Node {
                    left: Box::new(l_tree),
                    right: Box::new(r_tree),
                },
            )
        }
    }
}

impl Editor {
    pub fn new(new_signal: Windows, terminal_size_y: u16, terminal_size_x: u16) -> Result<Self> {
        let mut default_cursor = Position::default();
        let initial_buffer: WindManager = match new_signal {
            Windows::TextEditor(abs_path) => WindManager::TextEditorMode(Rc::new(RefCell::new(
                TextMode::new_with_file(abs_path),
            ))),
            Windows::FileManager(abs_path) => WindManager::FileManagerMode(Rc::new(RefCell::new(
                FileSystem::new(abs_path, terminal_size_y - 3, terminal_size_x)?,
            ))),
            _ => WindManager::Null,
        };

        let default_buffer = match initial_buffer {
            WindManager::TextEditorMode(ref tm) => {
                default_cursor = tm.borrow().get_pos_as_pos();
                WindManager::TextEditorMode(Rc::clone(&tm))
            }
            WindManager::FileManagerMode(ref fm) => {
                default_cursor = fm.borrow().get_pos_as_pos();
                WindManager::FileManagerMode(Rc::clone(&fm))
            }
            _ => WindManager::Null,
        };

        // let help_buffer = match initial_buffer {
        //     WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
        //     WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
        //     _ => WindManager::Null,
        // };

        let height = terminal_size_y - 3;

        let default_display = DisplayWindow::new(
            default_buffer,
            true,
            PassDisplay {
                rowoff: 0,
                coloff: 0,
                width: terminal_size_x,
                height: height,
                origin: Position::new((0, 0)),
                cursor: default_cursor,
            },
        )?;

        // let help_display = DisplayWindow::new(
        //     help_buffer,
        //     true,
        //     Position::new((0, height / 2 + 1)),
        //     Position::new((0, 0)),
        //     height / 2,
        //     terminal_size_x,
        //     0,
        //     0,
        // )?;
        // .insert(help_display, 1),
        Ok(Self {
            screen: Screen::new()?,
            keyboard: Keyboard,
            // buffers: HashMap::new(),
            displays: Tree::new(default_display),
            message: MessageStatus::DrawMessage(String::from("")),
            signal: Windows::Current,
            telescope: Telescope::new(
                "is window coming?: ".to_string(),
                terminal_size_x,
                terminal_size_y,
            ),
            comb_key: None,
            cursor_index: 1,
            current_buffer: initial_buffer,
            current_cursor: default_cursor,
            size_y: terminal_size_y,
            size_x: terminal_size_x,
        })
    }

    fn _other_display(&mut self) -> Result<()> {
        let total_displays = self.displays.len();
        if self.cursor_index >= total_displays as usize {
            self.cursor_index = 1;
        } else {
            self.cursor_index = self.cursor_index + 1;
        }
        Ok(())
    }

    fn horizontal_split(&mut self) -> Result<()> {
        let splitted_display = self
            .displays
            .get_ith_node(self.cursor_index as u16)
            .unwrap()
            .split_horizontally()?;
        self.displays = self
            .displays
            .insert(splitted_display, self.cursor_index as u16);
        Ok(())
    }

    fn _delete_window(&mut self) -> Result<()> {
        if self.displays.is_leaf() {
            self.message =
                MessageStatus::DrawMessage("Sole window , not possible to delete".to_string());
        }
        Ok(())
    }

    pub fn draw_message(&mut self) -> Result<()> {
        let (_, rows) = terminal::size()?;
        let message_pos = rows - 1;
        match &self.message {
            MessageStatus::DrawMessage(msg) => {
                self.screen.clear_row(message_pos)?;
                self.screen.draw_message(message_pos, &msg)
            }
            MessageStatus::ClearMessage => self.screen.clear_row(message_pos),
            MessageStatus::NoChange => {
                let temp_msg = match &self.current_buffer {
                    WindManager::TextEditorMode(tm) => tm.borrow_mut().get_message(),
                    WindManager::FileManagerMode(tm) => tm.borrow_mut().get_message(),
                    _ => MessageStatus::NoChange,
                };
                match temp_msg {
                    MessageStatus::DrawMessage(msg) => {
                        self.screen.clear_row(message_pos)?;
                        self.screen.draw_message(message_pos, &msg)
                    }
                    MessageStatus::ClearMessage => self.screen.clear_row(message_pos),
                    MessageStatus::NoChange => Ok(()),
                }
            }
        }
    }

    pub fn handle_read_key(&mut self) -> Option<KeyEvent> {
        if let Ok(key_event) = self.keyboard.read() {
            Some(key_event)
        } else {
            self.die("Editor read failure");
            None
        }
    }

    pub fn die<S: Into<String>>(&mut self, message: S) {
        let _ = self.screen.clear_all();
        let _ = terminal::disable_raw_mode();
        eprintln!("{}: {}", message.into(), errno());
        std::process::exit(1);
    }

    fn handle_signal(&mut self) -> Result<bool> {
        match &self.signal {
            Windows::Exit => {
                return Ok(false);
            }
            Windows::Current => {
                return Ok(true);
            }
            Windows::FileManager(path) => {
                // let new_file_buffer = if let Some(file_buffer) = self.buffers.get(path) {
                //     match file_buffer {
                //         WindManager::TextEditorMode(ref tm) => {
                //             WindManager::TextEditorMode(Rc::clone(&tm))
                //         }
                //         WindManager::FileManagerMode(ref fm) => {
                //             WindManager::FileManagerMode(Rc::clone(&fm))
                //         }
                //         _ => WindManager::Null,
                //     }
                // } else {
                let new_file_buffer = WindManager::FileManagerMode(Rc::new(RefCell::new(
                    FileSystem::new(path.to_path_buf(), self.size_y - 3, self.size_x)?,
                )));

                let mut init_display = self
                    .displays
                    .get_ith_node(self.cursor_index as u16)
                    .unwrap();
                self.current_cursor =
                    FileSystem::new(path.to_path_buf(), self.size_y - 3, self.size_x)
                        .unwrap()
                        .get_pos_as_pos();

                init_display.replace_buffer(&new_file_buffer)?;

                let _file_path = get_buffer_file(&self.current_buffer).unwrap();
                let _new_buffer = match &self.current_buffer {
                    WindManager::TextEditorMode(ref tm) => {
                        WindManager::TextEditorMode(Rc::clone(&tm))
                    }
                    WindManager::FileManagerMode(ref fm) => {
                        WindManager::FileManagerMode(Rc::clone(&fm))
                    }
                    _ => WindManager::Null,
                };

                self.current_buffer = new_file_buffer;
                self.displays = self
                    .displays
                    .replace(init_display, self.cursor_index as u16);
                return Ok(true);
            }

            Windows::TextEditor(path) => {
                // let new_file_buffer = if let Some(file_buffer) = self.buffers.get(path) {
                //     match file_buffer {
                //         WindManager::TextEditorMode(ref tm) => {
                //             WindManager::TextEditorMode(Rc::clone(&tm))
                //         }
                //         WindManager::FileManagerMode(ref fm) => {
                //             WindManager::FileManagerMode(Rc::clone(&fm))
                //         }
                //         _ => WindManager::Null,
                //     }
                // } else {
                let new_file_buffer = WindManager::TextEditorMode(Rc::new(RefCell::new(
                    TextMode::new_with_file(path.to_path_buf()),
                )));
                let mut init_display = self
                    .displays
                    .get_ith_node(self.cursor_index as u16)
                    .unwrap();
                self.current_cursor = Position::new((0, 0));
                init_display.replace_buffer(&new_file_buffer).unwrap();

                let _file_path = get_buffer_file(&self.current_buffer).unwrap();
                let _new_buffer = match &self.current_buffer {
                    WindManager::TextEditorMode(ref tm) => {
                        WindManager::TextEditorMode(Rc::clone(&tm))
                    }
                    WindManager::FileManagerMode(ref fm) => {
                        WindManager::FileManagerMode(Rc::clone(&fm))
                    }
                    _ => WindManager::Null,
                };

                // self.buffers.insert(file_path, new_buffer);
                self.current_buffer = new_file_buffer;
                self.displays = self
                    .displays
                    .replace(init_display, self.cursor_index as u16);
                return Ok(true);
            }
        }
    }

    fn welcome(&mut self) -> Result<()> {
        self.screen.hide_cursor();
        let (terminal_size_x, terminal_size_y) = terminal::size()?;

        let mut welcome_rows = Vec::new();
        let welcome = "Welcome to \"cold\", window is coming!\n";
        let welcome_len_by_2 = (welcome.len() / 2) as u16;
        let padding = self.size_x / 2 - welcome_len_by_2;
        let mut padd_str = String::new();
        for _ in 0..padding {
            padd_str.push_str(" ");
        }
        let welcome_padded = format!("{}{}", padd_str, welcome);
        let enter_padded = format!("{}Press Enter to go into text editor", padd_str);
        let exit_padded = format!("{}Press 'q' to exit out of text editor", padd_str);

        for y in 0..self.size_y {
            if y == (self.size_y / 2) - 1 {
                welcome_rows.push(Row::new(welcome_padded.clone()));
                welcome_rows.push(Row::new(enter_padded.clone()));
                welcome_rows.push(Row::new(exit_padded.clone()));
            } else {
                welcome_rows.push(Row::new("\n".to_string()));
            }
        }

        let pass_display = PassDisplay {
            rowoff: 0,
            coloff: 0,
            width: terminal_size_x,
            height: terminal_size_y,
            cursor: Position::new((0, 0)),
            origin: Position::new((0, 0)),
        };
        loop {
            self.screen
                .draw_lines(&welcome_rows, &pass_display, None, None)?;
            self.screen.flush()?;
            let key_event = self.handle_read_key().unwrap();
            let key = cross_key_to_cold(key_event);
            match key {
                Key::Normal(RegularKey::Char('q')) => {
                    self.signal = Windows::Exit;
                    break;
                }
                Key::Normal(RegularKey::Enter) => {
                    break;
                }
                _ => continue,
            }
        }
        self.screen.show_cursor();
        Ok(())
    }

    fn isearch(&mut self, d_window: &mut DisplayWindow, is_forward: bool) -> Option<Key> {
        let mut is_backward = !is_forward;
        let (_size_x, size_y) = terminal::size().unwrap();
        let prompt = String::from("Search something: ");
        let default_input = String::from("");
        let mut cursor = self.current_cursor;
        let mut minibuffer =
            MinibufferPrompt::new(Vec::new(), prompt, default_input, 0, size_y - 2);

        loop {
            minibuffer.display(&mut self.screen).unwrap();
            d_window.display_cursor(&mut self.screen, cursor).unwrap();
            self.screen.flush().unwrap();
            let key_event = self.handle_read_key().unwrap();
            let key = cross_key_to_cold(key_event);
            match key {
                Key::Normal(RegularKey::Esc) | Key::Ctrl(RegularKey::Char('g')) => {
                    break;
                }
                Key::Normal(RegularKey::Enter) => {
                    self.current_cursor = cursor;

                    break;
                }
                Key::Ctrl(RegularKey::Char('s')) => match d_window.buffer {
                    WindManager::TextEditorMode(ref tm) => {
                        tm.borrow_mut().go_2_next_nearest_search();
                        cursor = tm.borrow().get_pos_as_pos();
                        is_backward = false;
                    }
                    WindManager::FileManagerMode(ref fm) => {
                        fm.borrow_mut().go_2_next_nearest_search();
                        cursor = fm.borrow().get_pos_as_pos();
                        is_backward = false;
                    }
                    _ => {}
                },
                Key::Ctrl(RegularKey::Char('r')) => match d_window.buffer {
                    WindManager::TextEditorMode(ref tm) => {
                        tm.borrow_mut().go_2_prev_nearest_search();
                        cursor = tm.borrow().get_pos_as_pos();
                        is_backward = true;
                    }
                    WindManager::FileManagerMode(ref fm) => {
                        fm.borrow_mut().go_2_prev_nearest_search();
                        cursor = fm.borrow().get_pos_as_pos();
                        is_backward = false;
                    }

                    _ => {}
                },
                Key::Normal(RegularKey::Char(_))
                | Key::Shift(RegularKey::Char(_))
                | Key::Normal(RegularKey::Backspace) => {
                    let input = minibuffer.keypress_handling(&key);
                    match d_window.buffer {
                        WindManager::TextEditorMode(ref tm) => {
                            let positions: Vec<(usize, Vec<usize>)> =
                                search(tm.borrow().get_text_rows_imm_ref(), &input);
                            tm.borrow_mut()
                                .adjust_search_cursor(input, positions)
                                .unwrap();
                            if !is_backward {
                                tm.borrow_mut().go_2_current_or_next_nearest_search();
                            } else {
                                tm.borrow_mut().go_2_current_or_prev_nearest_search();
                            }
                            cursor = tm.borrow().get_pos_as_pos();
                        }
                        WindManager::FileManagerMode(ref fm) => {
                            let positions: Vec<(usize, Vec<usize>)> =
                                search(fm.borrow().get_text_rows_imm_ref(), &input);
                            fm.borrow_mut()
                                .adjust_search_cursor(input, positions)
                                .unwrap();
                            if !is_backward {
                                fm.borrow_mut().go_2_current_or_next_nearest_search();
                            } else {
                                fm.borrow_mut().go_2_current_or_prev_nearest_search();
                            }
                            cursor = fm.borrow().get_pos_as_pos();
                        }
                        _ => (),
                    };
                }
                _ => {
                    self.current_cursor = cursor;
                    return Some(key);
                }
            }
            d_window.display(&mut self.screen, Some(cursor)).unwrap();
            self.screen.flush().unwrap();
        }
        match d_window.buffer {
            WindManager::TextEditorMode(ref tm) => tm
                .borrow_mut()
                .process_keypress(&Key::Ctrl(RegularKey::Char('g'))),
            WindManager::FileManagerMode(ref fm) => fm
                .borrow_mut()
                .process_keypress(&Key::Ctrl(RegularKey::Char('g'))),
            _ => Windows::Current,
        };

        self.screen.clear_given_line(0, size_y - 2).unwrap();
        return None;
    }

    fn view(&mut self) -> DisplayWindow {
        self.screen.hide_cursor();
        self.draw_message().unwrap();
        let (_, new_displays) = call_displays(
            &mut self.screen,
            self.displays.clone(),
            1,
            self.cursor_index as u16,
            self.current_cursor,
        );
        self.displays = new_displays;

        self.screen.show_cursor();
        let mut head_display = self
            .displays
            .get_ith_node((self.cursor_index) as u16)
            .unwrap();
        head_display
            .display_cursor(&mut self.screen, self.current_cursor)
            .unwrap();
        head_display
    }

    fn keypress_handling(&mut self, key: &Key) {
        match &self.current_buffer {
            WindManager::TextEditorMode(tm) => {
                self.signal =
                    tm.borrow_mut()
                        .keypress_handling(&key, self.comb_key, self.current_cursor);
                self.message = MessageStatus::NoChange;
                self.comb_key = None;
                self.current_cursor = tm.borrow_mut().get_cursor_pos();
            }
            WindManager::FileManagerMode(fm) => {
                self.signal =
                    fm.borrow_mut()
                        .keypress_handling(&key, self.comb_key, self.current_cursor);
                self.comb_key = None;
                self.current_cursor = fm.borrow_mut().get_cursor_pos();
            }
            _ => {}
        }
    }

    pub fn start(&mut self) -> Result<()> {
        let (size_x, size_y) = terminal::size()?;
        self.screen.enter_alternate_screen()?;
        terminal::enable_raw_mode()?;
        self.welcome()?;
        loop {
            if !self.handle_signal()? {
                break;
            }
            let mut head_display = self.view();

            let key_event = self.handle_read_key().unwrap();
            let mut key = cross_key_to_cold(key_event);
            match self.comb_key {
                None => match key {
                    Key::Ctrl(RegularKey::Char('x')) => {
                        self.signal = Windows::Current;
                        self.message =
                            MessageStatus::DrawMessage(String::from("Global key: C-x- "));
                        self.comb_key = Some(Key::Ctrl(RegularKey::Char('x')));
                        continue;
                    }
                    Key::Ctrl(RegularKey::Char('s')) => {
                        if let Some(ret_key) = self.isearch(&mut head_display, true) {
                            key = ret_key;
                        } else {
                            self.signal = Windows::Current;
                            continue;
                        }
                    }
                    Key::Ctrl(RegularKey::Char('r')) => {
                        if let Some(ret_key) = self.isearch(&mut head_display, false) {
                            key = ret_key;
                        } else {
                            self.signal = Windows::Current;
                            continue;
                        }
                    }
                    Key::Ctrl(RegularKey::Char('/')) => {
                        break;
                    }
                    _ => {}
                },
                Some(Key::Ctrl(RegularKey::Char('x'))) => match key {
                    Key::Normal(RegularKey::Char('b')) => {
                        self.telescope.start(&mut self.screen, &self.keyboard)?;
                    }
                    Key::Ctrl(RegularKey::Char('c')) => {
                        break;
                    }
                    Key::Ctrl(RegularKey::Char('h')) => {
                        break;
                    }
                    Key::Ctrl(RegularKey::Char('2')) => {
                        self.signal = Windows::Current;
                        self.horizontal_split()?;
                    }
                    _ => {}
                },
                _ => {}
            }
            self.keypress_handling(&key);
        }
        self.screen.clear_all().unwrap();
        terminal::disable_raw_mode()?;
        self.screen.leave_alternate_screen()?;
        println!("\n\n***********************Debug: ******************************");
        let (temp_x, temp_y) = self.current_cursor.get_pos();
        println!("x: {temp_x} y:{temp_y}");
        println!("size_x: {size_x} size_y:{size_y}");
        println!("*****************************************************");
        println!("\n\n");
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_scroll() {
        use super::*;
        let (size_x, size_y) = terminal::size().unwrap();
        let cursor = Position::new((0, 57));
        let origin = Position::new((0, 0));
        let pass_display = PassDisplay {
            rowoff: 0,
            coloff: 0,
            width: size_x,
            height: size_y - 3,
            origin: origin,
            cursor: cursor,
        };
        let (new_rowoff, _) = scroll(&pass_display);
        // 0, 57, 0, 0, size_x, size_y - 3);
        let lhs = 57 - (size_y - 3) + 1;
        assert_eq!(lhs, new_rowoff);
    }

    #[test]

    fn file_manager_search() {
        use super::*;
        let (size_x, size_y) = terminal::size().unwrap();
        let dir_path = String::from("/home/vamshi/rust/cold-rust-haskell/test-src/").into();
        let file_path = String::from("/home/vamshi/rust/cold-rust-haskell/test-src/main.rs").into();

        let signal = Windows::FileManager(dir_path);
        let mut my_editor = Editor::new(signal, size_y, size_x).unwrap();

        assert_eq!(my_editor.current_cursor, Position::new((0, 3)));
        my_editor.signal = Windows::TextEditor(file_path);
        my_editor.handle_signal().unwrap();
        assert_eq!(my_editor.current_cursor, Position::new((0, 0)));
        let _head_display = my_editor
            .displays
            .get_ith_node((my_editor.cursor_index) as u16)
            .unwrap();
        my_editor.screen.enter_alternate_screen().unwrap();
        terminal::enable_raw_mode().unwrap();
        // my_editor.isearch(&mut head_display, true);
        terminal::disable_raw_mode().unwrap();
        my_editor.screen.leave_alternate_screen().unwrap();
        assert_eq!(Position::default(), my_editor.current_cursor);
    }
    // #[test]
    // fn test_editor() -> crossterm::Result<()> {
    //     let file_path = String::from("test-src/main.rs").into();
    // let (size_x, size_y) = crossterm::terminal::size()?;
    // let editor = Editor::new(Windows::TextEditor(file_path), size_y, size_x);
    // editor.
    //     return Ok(());
    // }
}
