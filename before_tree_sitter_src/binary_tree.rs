use std::fmt;

// #[derive(Debug,Clone)]
pub enum Tree<T> {
    Leaf(T),
    Node {
        left: Box<Tree<T>>,
        right: Box<Tree<T>>,
    },
}

impl<T: fmt::Display> fmt::Display for Tree<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Tree::Leaf(value) => write!(f, "Leaf: {}", value),
            Tree::Node { left, right } => {
                write!(f, "Node({},{})", left, right)
            }
        }
    }
}

impl<T: Clone> Clone for Tree<T> {
    fn clone(&self) -> Self {
        match self {
            Tree::Leaf(value) => *Box::new(Tree::Leaf(value.clone())),
            Tree::Node { left, right } => *Box::new(Tree::Node {
                left: left.clone(),
                right: right.clone(),
            }),
        }
    }
}

impl<T> Tree<T>
where
    T: Clone,
{
    pub fn new(new_value: T) -> Self {
        return Tree::Leaf(new_value);
    }
    pub fn len(&self) -> u16 {
        self.len_aux(0)
    }
    fn len_aux(&self, cur_index: u16) -> u16 {
        match self {
            Tree::Leaf(_) => cur_index + 1,
            Tree::Node { left, right } => {
                let l_len = left.len_aux(cur_index);
                right.len_aux(l_len)
            }
        }
    }

    pub fn is_leaf(&self) -> bool {
        match self {
            Tree::Leaf(_) => true,
            _ => false,
        }
    }

    pub fn tree_length_aux(&self, cur_index: u16) -> u16 {
        match self {
            Tree::Leaf(_) => cur_index + 1,
            Tree::Node { left, right } => {
                let l_index = left.tree_length_aux(cur_index);
                right.tree_length_aux(l_index)
            }
        }
    }

    pub fn remove_ith(self, target_index: u16) -> Option<Self> {
        let (_, maybe_tree) = self.remove_ith_aux(1, target_index);
        maybe_tree
    }

    fn remove_ith_aux(self, cur_index: u16, target_index: u16) -> (u16, Option<Self>) {
        match self {
            Tree::Leaf(value) => {
                if cur_index == target_index {
                    return (cur_index + 1, None);
                }
                (cur_index + 1, Some(Tree::Leaf(value)))
            }
            Tree::Node { left, right } => {
                let (l_index, l_tree) = left.remove_ith_aux(cur_index, target_index);
                if l_tree.is_none() {
                    let (r_index, r_tree) = right.remove_ith_aux(l_index, target_index);
                    return (r_index, r_tree);
                }

                let (r_index, r_tree) = right.remove_ith_aux(l_index, target_index);
                if r_tree.is_none() {
                    return (r_index, l_tree);
                } else {
                    (
                        r_index,
                        Some(Tree::Node {
                            left: Box::new(l_tree.unwrap()),
                            right: Box::new(r_tree.unwrap()),
                        }),
                    )
                }
            }
        }
    }

    pub fn get_ith_node(&self, target_index: u16) -> Option<T> {
        let (_, option_value) = self.get_ith_node_aux(1, target_index, None);
        option_value
    }

    fn get_ith_node_aux(
        &self,
        cur_index: u16,
        target_index: u16,
        maybe_value: Option<T>,
    ) -> (u16, Option<T>) {
        if let Some(res) = maybe_value {
            (cur_index, Some(res))
        } else {
            match self {
                Tree::Leaf(value) => {
                    if cur_index == target_index {
                        (cur_index + 1, Some(value.clone()))
                    } else {
                        (cur_index + 1, None)
                    }
                }
                Tree::Node { left, right } => {
                    let (l_index, l_maybe) =
                        left.get_ith_node_aux(cur_index, target_index, maybe_value);
                    right.get_ith_node_aux(l_index, target_index, l_maybe)
                }
            }
        }
    }

    pub fn to_vec(&self) -> Vec<T> {
        self.to_vec_aux(vec![])
    }

    fn to_vec_aux(&self, mut collector: Vec<T>) -> Vec<T> {
        match self {
            Tree::Leaf(value) => {
                collector.push(value.clone());
                collector
            }
            Tree::Node { left, right } => {
                let l_collector = left.to_vec_aux(collector);
                let r_collector = right.to_vec_aux(l_collector);
                r_collector
            }
        }
    }

    pub fn replace(&self, new_value: T, target_index: u16) -> Tree<T> {
        let (_, tree_res) = self.replace_aux(new_value, 1, target_index);
        tree_res
    }

    fn replace_aux(&self, new_value: T, cur_index: u16, target_index: u16) -> (u16, Tree<T>) {
        match self {
            Tree::Leaf(value) => {
                if cur_index == target_index {
                    (cur_index + 1, Tree::Leaf(new_value.clone()))
                } else {
                    (cur_index + 1, Tree::Leaf(value.clone()))
                }
            }
            Tree::Node { left, right } => {
                let (l_index, l_tree) =
                    left.replace_aux(new_value.clone(), cur_index, target_index);
                let (r_index, r_tree) = right.replace_aux(new_value, l_index, target_index);

                (
                    r_index,
                    Tree::Node {
                        left: Box::new(l_tree),
                        right: Box::new(r_tree),
                    },
                )
            }
        }
    }

    pub fn insert(&self, new_value: T, target_index: u16) -> Tree<T> {
        let (_, tree_res) = self.insert_aux(new_value, 1, target_index);
        tree_res
    }

    fn insert_aux(&self, new_value: T, cur_index: u16, target_index: u16) -> (u16, Tree<T>) {
        match self {
            Tree::Leaf(value) => {
                if cur_index == target_index {
                    (
                        cur_index + 2,
                        Tree::Node {
                            left: Box::new(Tree::Leaf(value.clone())),
                            right: Box::new(Tree::Leaf(new_value)),
                        },
                    )
                } else {
                    (cur_index + 1, Tree::Leaf(value.clone()))
                }
            }
            Tree::Node { left, right } => {
                let (l_index, l_tree) = left.insert_aux(new_value.clone(), cur_index, target_index);
                let (r_index, r_tree) = right.insert_aux(new_value, l_index, target_index);

                (
                    r_index,
                    Tree::Node {
                        left: Box::new(l_tree),
                        right: Box::new(r_tree),
                    },
                )
            }
        }
    }
}
