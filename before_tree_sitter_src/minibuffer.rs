// use crate::keyboard::*;
use crate::display_window::PassDisplay;
use crate::my_lib::*;
use crate::screen::Screen;
use crossterm::{
    // event::{KeyCode, KeyEvent, KeyModifiers},
    terminal,
    Result,
};
use errno::errno;

pub struct MinibufferPrompt {
    is_focus: bool,
    prompt: String,
    pass_display: PassDisplay,
    text_rows: Vec<String>,
    input: String,
}

impl MinibufferPrompt {
    pub fn new(
        text_rows: Vec<String>,
        prompt: String,
        input: String,
        _og_x: u16,
        og_y: u16,
    ) -> Self {
        let mut my_text_rows = text_rows.clone();
        my_text_rows.insert(0, input.clone());
        Self {
            is_focus: false,
            prompt: prompt.clone(),
            pass_display: PassDisplay {
                rowoff: 0,
                coloff: 0,
                height: my_text_rows.len() as u16,
                width: prompt.len() as u16 + 15,
                cursor: Position::new((0, 0)),
                origin: Position::new((prompt.len() as u16 + 1, og_y)),
            },
            input: input,
            text_rows: my_text_rows,
        }
    }

    // function for scrolling rows
    pub fn scroll(&mut self, screen: &Screen) {
        let (pos_x, pos_y) = self.pass_display.cursor.get_pos();
        let (size_x, size_y) = screen.bounds().get_pos();
        if pos_y < self.pass_display.rowoff {
            self.pass_display.rowoff = pos_y;
        } else if pos_y >= self.pass_display.rowoff + size_y {
            self.pass_display.rowoff = pos_y - size_y + 1;
        }
        if pos_x < self.pass_display.coloff {
            self.pass_display.coloff = pos_x;
        } else if pos_x >= self.pass_display.coloff + size_x {
            self.pass_display.coloff = pos_x - size_x + 1;
        }
    }

    pub fn display(&mut self, screen: &mut Screen) -> Result<()> {
        self.text_rows[0] = self.input.clone();
        // = self.prompt.clone() + &self.input;
        self.scroll(screen);

        screen.draw_strings_vertical_minibuffer(
            &self.prompt,
            &self.text_rows,
            self.pass_display, //.coloff,
                               // self.pass_display, //.height,
        )?;

        // let (pos_x, pos_y) = self.pass_display.cursor.get_pos();
        // let (og_x, og_y) = self.pass_display.origin.get_pos();
        // let new_pos_y = og_y + pos_y - self.pass_display.rowoff;
        // let new_pos_x = og_x + pos_x - self.pass_display.coloff; // - self.input.len() as u16;
        // + self.text_rows[0].len() as u16

        // let custom_pos = Position::new((new_pos_x, new_pos_y));
        // screen.hide_cursor();
        // screen.render_cursor(&Position::default())?;
        screen.flush()
    }

    pub fn keypress_handling(&mut self, key: &Key) -> String {
        self.process_keypress(key)
    }

    pub fn get_input(&self) -> String {
        self.input.clone()
    }

    pub fn set_input(&mut self, new_input: &str) {
        self.input = new_input.to_string();
    }

    pub fn set_cursor_pos(&mut self, (row, col): (u16, u16)) {
        self.pass_display.cursor.set_pos((row, col));
    }

    pub fn get_cursor_pos(&mut self) -> (u16, u16) {
        self.pass_display.cursor.get_pos()
    }

    pub fn get_cursor_pos_as_pos(&mut self) -> Position {
        self.pass_display.cursor.get_pos_as_pos()
    }

    pub fn get_focus(&self) -> bool {
        self.is_focus
    }

    pub fn process_keypress(&mut self, key: &Key) -> String {
        match key {
            // Key::Ctrl(RegularKey::Char('g')) => {
            //     self.is_focus = false;
            //     self.prompt = "".to_string();
            //     self.cursor = Position::new((0, self.pass_display.height));
            //
            //     self.input = String::from("");
            //     Windows::Minibuffer(MiniResult::Cancel)
            // }
            Key::Normal(RegularKey::Char(ch)) | Key::Shift(RegularKey::Char(ch)) => {
                // match self.kind {
                // MinibufferType::Multiple => {
                // self.insert_char(*ch);
                // }
                // MinibufferType::Single => {
                self.insert_char(*ch);
                // }
                // }
            }
            // Key::Normal(RegularKey::Enter) => {
            //     self.is_focus = false;
            //
            //     self.cursor = Position::new((0, self.pass_display.height));
            //     if self.input.is_empty() {
            //         Windows::Minibuffer(MiniResult::Cancel)
            //     } else {
            //         let temp = self.input.clone();
            //         self.input = String::from("");
            //         Windows::Minibuffer(MiniResult::Result(temp))
            //     }
            // }
            Key::Normal(RegularKey::Backspace) => {
                self.handle_backspace();
            }
            _ => (),
        }
        self.input.clone()
    }

    fn insert_char(&mut self, ch: char) {
        let (pos_x, pos_y) = self.pass_display.cursor.get_pos();
        if usize::from(pos_x) >= self.input.len() {
            self.input.push(ch);
        } else {
            self.input.insert(pos_x.into(), ch);
        }
        self.pass_display.cursor.set_pos((pos_x + 1, pos_y))
    }

    pub fn backspace(&mut self, at: usize) {
        let current_row = self.input.clone();

        let index = at;
        let mut result = String::with_capacity(current_row.len() - 1);

        if index > 0 {
            result.push_str(&current_row[0..index]);
        }

        if index < current_row.len() - 1 {
            result.push_str(&current_row[index + 1..]);
        }
        self.input = result.clone();
    }

    pub fn handle_backspace(&mut self) {
        let (pos_x, pos_y) = self.pass_display.cursor.get_pos();

        if pos_x == 0 {
            return;
        } else {
            self.backspace((pos_x - 1) as usize);
            self.pass_display.cursor.set_pos((pos_x - 1, pos_y))
        }
    }

    pub fn get_text(&mut self) -> String {
        self.prompt.clone() + &self.input
    }

    pub fn set_prompt(&mut self, prompt_text: String) {
        self.prompt = prompt_text;
    }

    pub fn die<S: Into<String>>(&mut self, message: S, screen: &mut Screen) {
        let _ = screen.clear_all();
        let _ = terminal::disable_raw_mode();
        eprintln!("{}: {}", message.into(), errno());
        std::process::exit(1);
    }
    pub fn operation_of_return_key(&mut self) -> (bool, String) {
        (true, "".to_string())
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_minibuffer_backspace() {
        let (_size_x, size_y) = terminal::size().unwrap();
        let prompt = String::from("Search something: ");
        let default_input = String::from("");
        let mut minibuffer =
            MinibufferPrompt::new(Vec::new(), prompt, default_input, 0, size_y - 3);
        let backspace = Key::Normal(RegularKey::Backspace);
        // minibuffer.
        minibuffer.keypress_handling(&backspace);
        minibuffer.keypress_handling(&backspace);
        let cursor = minibuffer.get_cursor_pos_as_pos();
        let lhs = Position::default();
        assert_eq!(lhs, cursor);
    }
}

/*
#[cfg(test)]
mod tests {
    use std::borrow::BorrowMut;
    #[test]
    fn test_minibuffer_flip_focus() {
        use super::*;
        let (size_y, _) = terminal::size().unwrap();
        let mut minibuffer = MinibufferPrompt::new(size_y - 3 + 1);
        let before_focus = minibuffer.get_focus();
        minibuffer.borrow_mut().flip_is_focus();
        let after_focus = minibuffer.get_focus();
        assert_eq!(before_focus, !after_focus);
    }

    #[test]
    fn test_miniresult_empty_return() {
        use super::*;
        let (size_y, _) = terminal::size().unwrap();
        let mut minibuffer = MinibufferPrompt::new(size_y - 3 + 1);
        // let key_event_enter = KeyEvent {
        //     code: KeyCode::Enter,
        //     modifiers: KeyModifiers::empty(),
        //     kind: crossterm::event::KeyEventKind::Press,
        //     state: crossterm::event::KeyEventState::empty(),
        // };
        let key = Key::Normal(RegularKey::Enter);
        let msg_windows = minibuffer.keypress_handling(&key);
        assert_eq!(Windows::Minibuffer(MiniResult::Cancel), msg_windows);
    }
}
*/
