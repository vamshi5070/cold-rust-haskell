use crate::my_lib::*;
use crate::screen::Screen;

use crossterm::Result;
use std::rc::Rc;

pub struct DisplayWindow {
    pub buffer: WindManager,
    is_focus: bool,
    //     origin: Position,
    //     cursor: Position,
    //     width: u16,
    //     height: u16,
    //     rowoff: u16,
    //     coloff: u16,
    pass_display: PassDisplay,
}

#[derive(Clone, Copy)]
pub struct PassDisplay {
    pub rowoff: u16,
    pub coloff: u16,
    pub width: u16,
    pub height: u16,
    pub origin: Position,
    pub cursor: Position,
}

impl Clone for DisplayWindow {
    fn clone(&self) -> Self {
        let cloned_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };

        Self {
            buffer: cloned_buffer,
            is_focus: self.is_focus,
            pass_display: self.pass_display, //     origin: self.origin,
                                             //     cursor: self.cursor,
                                             //     width: self.width,
                                             //     height: self.height,
                                             //     rowoff: self.rowoff,
                                             //     coloff: self.coloff,
        }
    }
}

impl DisplayWindow {
    pub fn new(
        buffer: WindManager,
        is_focus: bool,
        pass_display: PassDisplay,
        // origin: Position,
        // cursor: Position,
        // width: u16,
        // height: u16,
        // rowoff: u16,
        // coloff: u16,
    ) -> Result<Self> {
        Ok(Self {
            buffer,
            is_focus,
            pass_display,
            // origin,
            // cursor,
            // width,
            // height,
            // rowoff,
            // coloff,
        })
    }

    pub fn split_horizontally(&mut self) -> Result<DisplayWindow> {
        let _og_height = self.pass_display.height;
        let og_width = self.pass_display.width;
        let half = og_width / 2;
        let (one_width, sec_width) = (half + (og_width % 2), half - 1);
        self.pass_display.width = one_width;
        let (og_x, og_y) = self.pass_display.origin.get_pos();
        let shared_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };
        let new_display_origin = Position::new((og_x, self.pass_display.width + og_y + 1));
        DisplayWindow::new(
            shared_buffer,
            false,
            PassDisplay {
                origin: new_display_origin,
                cursor: self.pass_display.cursor,
                width: sec_width,
                height: self.pass_display.height,
                rowoff: self.pass_display.rowoff,
                coloff: self.pass_display.coloff,
            },
        )
    }

    pub fn split_vertically(&mut self) -> Result<DisplayWindow> {
        let prev_height = self.pass_display.height;
        let original_display_height = prev_height / 2 + prev_height % 2;
        self.pass_display.height = original_display_height;
        let split_display_height = prev_height / 2;
        let (og_x, og_y) = self.pass_display.origin.get_pos();
        let shared_buffer = match self.buffer {
            WindManager::TextEditorMode(ref tm) => WindManager::TextEditorMode(Rc::clone(&tm)),
            WindManager::FileManagerMode(ref fm) => WindManager::FileManagerMode(Rc::clone(&fm)),
            _ => WindManager::Null,
        };

        let new_display_origin = Position::new((og_x + original_display_height, og_y));

        DisplayWindow::new(
            shared_buffer,
            false,
            PassDisplay {
                origin: new_display_origin,
                cursor: self.pass_display.cursor,
                width: self.pass_display.width,
                height: split_display_height,
                rowoff: self.pass_display.rowoff,
                coloff: self.pass_display.coloff,
            },
        )
    }

    pub fn get_cursor(&self) -> Position {
        self.pass_display.cursor
    }

    pub fn get_limit(&self) -> (u16, u16) {
        (self.pass_display.height, self.pass_display.width)
    }

    pub fn replace_buffer(&mut self, new_buffer: &WindManager) -> Result<()> {
        let shared_buffer = match new_buffer {
            WindManager::TextEditorMode(ref tm) => {
                self.pass_display.cursor = tm.borrow().get_pos_as_pos();
                WindManager::TextEditorMode(Rc::clone(&tm))
            }

            WindManager::FileManagerMode(ref fm) => {
                self.pass_display.cursor = fm.borrow().get_pos_as_pos();
                WindManager::FileManagerMode(Rc::clone(&fm))
            }
            _ => WindManager::Null,
        };
        self.buffer = shared_buffer;
        self.is_focus = true;

        // Position::new((0, 0));
        self.pass_display.rowoff = 0;
        self.pass_display.coloff = 0;

        Ok(())
    }

    pub fn get_buffer(&self) -> &WindManager {
        &self.buffer
    }

    pub fn display_cursor(&mut self, screen: &mut Screen, cursor: Position) -> Result<()> {
        match &self.buffer {
            WindManager::TextEditorMode(tm) => {
                self.pass_display.cursor = cursor;
                let mut head_value = tm.borrow_mut();
                head_value.draw_cursor(screen, &self.pass_display)
            }
            WindManager::FileManagerMode(fm) => {
                let mut head_value = fm.borrow_mut();
                head_value.draw_cursor(
                    screen,
                    // cursor,
                    &self.pass_display, // self.pass_display.origin,
                                        // self.pass_display.height,
                                        // self.pass_display.width,
                                        // self.pass_display.rowoff,
                                        // self.pass_display.coloff,
                )
            }
            _ => Ok(()),
        }
    }

    pub fn display(&mut self, screen: &mut Screen, temp_cursor: Option<Position>) -> Result<()> {
        self.pass_display.cursor = if let Some(new_cursor) = temp_cursor {
            new_cursor
        } else {
            self.pass_display.cursor
        };

        let (new_rowoff, new_coloff) = scroll(&self.pass_display);

        self.pass_display.rowoff = new_rowoff;
        self.pass_display.coloff = new_coloff;
        match &self.buffer {
            WindManager::TextEditorMode(tm) => {
                let mut head_value = tm.borrow_mut();
                head_value.display(screen, self.is_focus, &self.pass_display)?;
            }
            WindManager::FileManagerMode(fm) => {
                let mut head_value = fm.borrow_mut();
                head_value.display(screen, self.is_focus, &self.pass_display)?;
            }
            _ => {}
        }
        Ok(())
    }

    pub fn set_width(&mut self, width: u16) {
        self.pass_display.width = width;
        // Ok(())
    }
}
