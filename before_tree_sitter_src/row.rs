use crate::add_syntax::Syntax;
use crate::my_lib::*;
// use crossterm::style::Color;

// use crate::my_lib::Highlight;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct Row {
    string: String,
    pub len: usize,
    hl: Vec<Highlight>,
    major_mode: Language,
}

impl Row {
    pub fn new(line: String) -> Self {
        let mut result = Self {
            string: line.clone(),
            len: line.len(),
            hl: Vec::new(),
            major_mode: Language::C,
        };

        result.update_syntax(None);
        result
    }

    pub fn new_str_ref(line: &str) -> Self {
        let mut result = Self {
            string: line.to_owned().clone(),
            len: line.len(),
            hl: Vec::new(),
            major_mode: Language::C,
        };

        result.update_syntax(None);
        result
    }

    pub fn new_with_lang(line: String, lang: Language) -> Self {
        let mut result = Self {
            string: line.clone(),
            len: line.len(),
            hl: Vec::new(),
            major_mode: lang,
        };

        result.update_syntax(None);
        result
    }

    pub fn get_string(&self) -> String {
        self.string.clone()
    }

    pub fn get_chars(&self) -> &str {
        &self.string
    }

    pub fn hl_vec(&self) -> &[Highlight] {
        &self.hl
    }

    pub fn get_hl(&self, at: usize) -> Option<Highlight> {
        self.hl.get(at).copied()
    }

    pub fn insert_char(&mut self, at: usize, c: char) {
        if at >= self.string.len() {
            self.string.push(c);
        } else {
            self.string.insert(at, c);
        }
        self.len = self.len + 1;
        self.update_syntax(None);
    }

    pub fn render_str(&self) -> String {
        let mut result = String::new();
        for ch in self.string.chars() {
            result.push(ch);
        }
        result
    }

    pub fn split_row(&mut self, at: usize) -> Self {
        let current_row = self.string.clone();
        let lang = self.major_mode.clone();
        let position_to_split = at;
        self.string = (&current_row[0..position_to_split]).to_string();
        self.len = self.string.len();
        self.update_syntax(None);
        let second_part = &current_row[position_to_split..];
        Row::new_with_lang(second_part.to_string(), lang)
    }

    pub fn remove_range(&mut self, start: usize, end: usize) -> String {
        let removed: String = self.string.drain(start..end).collect();
        self.len -= end - start;
        self.update_syntax(None);
        return removed;
    }

    pub fn backspace(&mut self, at: usize) -> char {
        let current_row = self.string.clone();
        let ch = current_row.chars().nth(at).unwrap();
        let index = at;
        let mut result = String::with_capacity(self.len - 1);
        if index > 0 {
            result.push_str(&current_row[0..index]);
        }

        if index < self.len - 1 {
            result.push_str(&current_row[index + 1..]);
        }
        self.string = result.clone();
        self.len = result.len();
        self.update_syntax(None);
        return ch;
    }

    pub fn add_row(&mut self, new_row: &Row) {
        let new_str = &new_row.string;
        self.string.push_str(&new_str);
        self.len = self.string.len();
        self.update_syntax(None);
    }

    fn update_dired(&mut self, maybe_locations: Option<(Vec<usize>, usize)>) {
        self.hl = Vec::new();
        if let Some((cursors, len)) = maybe_locations {
            let mut cur_index = 0;
            for (ch_index, _) in self.string.chars().enumerate() {
                if cur_index < cursors.len() && ch_index == cursors.get(cur_index).unwrap() + len {
                    cur_index = cur_index + 1;
                }

                if cur_index < cursors.len()
                    && ch_index >= *cursors.get(cur_index).unwrap()
                    && ch_index <= *cursors.get(cur_index).unwrap() + len
                {
                    self.hl.push(Highlight::Match)
                } else if self.string.ends_with("/") {
                    self.hl.push(Highlight::Directory)
                } else {
                    self.hl.push(Highlight::Normal)
                }
            }
        } else {
            if self.string.ends_with("/") {
                for _ in self.string.chars() {
                    self.hl.push(Highlight::Directory)
                }
            } else {
                for _ in self.string.chars() {
                    self.hl.push(Highlight::Normal)
                }
            }
        }
    }

    fn update_c_or_rust(&mut self, _maybe_locations: Option<(Vec<usize>, usize)>) {
        self.hl = Vec::new();
        if self.len == 0 {
            return;
        }
        // let mut in_string_quote: Option<char> = None;
        // let scs = String::from("//");
        // let _scs_len = scs.len();
        // let _ch = self.string.as_bytes()[0] as char;

        // let prev_hl: Highlight = if ch.is_numeric() {
        //     Highlight::Number
        // } else if ch == '"' {
        //     Highlight::HlString
        // } else {
        //     Highlight::Normal
        // };

        // self.hl.push(prev_hl);
        // let prev_sep = ch.is_whitespace();
        self.hl = Syntax::new(&self.string).get_hl_syntax();
        // let mut current = 1;
        // 'outer: loop {
        //     if let Some(mut ch) = char_iter.next() {
        //         if scs_len != 0 && in_string_quote.is_none() && ch == '/' {
        //             if let Some(new_ch) = char_iter.next() {
        //                 if new_ch == '/' {
        //                     'inner: loop {
        //                         self.hl.push(Highlight::Comment);
        //                         self.hl.push(Highlight::Comment);
        //                         if let Some(_) = char_iter.next() {
        //                             self.hl.push(Highlight::Comment);
        //                             // prev_hl = Some(Highlight::Comment);
        //                             continue 'inner;
        //                         } else {
        //                             break 'outer;
        //                         }
        //                     }
        //                 } else {
        //                     ch = new_ch.to_owned();
        //                 }
        //             } else {
        //                 break 'outer;
        //             }
        //         }
        //         if let Some(c) = in_string_quote {
        //             self.hl.push(Highlight::HlString);
        //             prev_hl = Some(Highlight::HlString);
        //             ch_index = ch_index + 1;
        //             if ch == '\\' && ch_index < self.len {
        //                 let _ = char_iter.next();
        //                 self.hl.push(Highlight::HlString);
        //                 prev_hl = Some(Highlight::HlString);
        //                 ch_index = ch_index + 1;
        //                 prev_sep = my_is_seperator(ch);
        //                 continue;
        //             }
        //             if c == ch {
        //                 in_string_quote = None;
        //                 prev_sep = true;
        //             }
        //             continue; // not needed for this particular continue
        //         } else {
        //             if ch == '"' || ch == '\'' {
        //                 in_string_quote = Some(ch);
        //                 self.hl.push(Highlight::HlString);
        //                 prev_hl = Some(Highlight::HlString);
        //                 ch_index = ch_index + 1;
        //                 prev_sep = my_is_seperator(ch);
        //                 continue;
        //             }
        //         }
        //         if let Some(prev_hl_unwrapped) = prev_hl {
        //             match prev_hl_unwrapped {
        //                 Highlight::Number => {
        //                     if ch == '.' || ch.is_digit(10) {
        //                         self.hl.push(Highlight::Number);
        //                         prev_hl = Some(Highlight::Number);
        //                         // prev_sep = false;
        //                         ch_index = ch_index + 1;
        //                         prev_sep = my_is_seperator(ch);
        //                         continue;
        //                     } else {
        //                         self.hl.push(Highlight::Normal);
        //                         prev_hl = Some(Highlight::Normal);
        //                         ch_index = ch_index + 1;
        //                         prev_sep = my_is_seperator(ch);
        //                         continue;
        //                     }
        //                 }
        //                 _ => {
        //                     if ch.is_digit(10) && prev_sep {
        //                         self.hl.push(Highlight::Number);
        //                         prev_hl = Some(Highlight::Number);
        //                         // prev_sep = false;
        //                         ch_index = ch_index + 1;
        //                         prev_sep = my_is_seperator(ch);
        //                         continue;
        //                     } else {
        //                         self.hl.push(Highlight::Normal);
        //                         prev_hl = Some(Highlight::Normal);
        //                         ch_index = ch_index + 1;
        //                         prev_sep = my_is_seperator(ch);
        //                         continue;
        //                     }
        //                 }
        //             }
        //         } else {
        //             if ch.is_digit(10) && prev_sep {
        //                 self.hl.push(Highlight::Number);
        //                 prev_hl = Some(Highlight::Number);
        //                 ch_index = ch_index + 1;
        //                 prev_sep = my_is_seperator(ch);
        //                 continue;
        //             } else {
        //                 self.hl.push(Highlight::Normal);
        //                 prev_hl = Some(Highlight::Normal);
        //                 ch_index = ch_index + 1;
        //                 prev_sep = my_is_seperator(ch);
        //                 continue;
        //             }
        //         }
        //     } else {
        //         break;
        //     }
        // }
        // // }
    }

    pub fn update_syntax(
        &mut self,
        maybe_locations: Option<(Vec<usize>, usize)>,
        // maybe_highlight: Option<(Position, Position)>,
    ) {
        match self.major_mode {
            Language::C => self.update_c_or_rust(maybe_locations),
            Language::Rust => self.update_c_or_rust(maybe_locations),
            Language::Dired => self.update_dired(maybe_locations),
            Language::Fundemental => {
                for _ in self.string.chars() {
                    self.hl.push(Highlight::Normal)
                }
            } // _ => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use Highlight::*;
    #[test]
    fn test_str_len_equals_hl() {
        // empty
        let empty_row = Row::new(String::from(""));
        let empty_str_len = empty_row.len;
        let empty_hl_vec_len = empty_row.hl.len();
        let empty_hl_vec_lhs: Vec<Highlight> = Vec::new();
        assert_eq!(empty_str_len, empty_hl_vec_len);
        assert_eq!(empty_hl_vec_lhs, empty_row.hl);

        let two_problem = "                let welcome_msg = String::from(\"Welcome to cold, winter is coming!!\")".to_string();
        let _one_problem = "            } else if (term_row / 3) == screen_y {".to_string();
        let problem_stmt_row = Row::new(two_problem);
        let problem_stmt_str_len = problem_stmt_row.len;
        let problem_stmt_hl_vec_len = problem_stmt_row.hl.len();

        assert_eq!(problem_stmt_str_len, problem_stmt_hl_vec_len);
    }

    #[test]
    fn test_number() {
        // use Highlight::*;
        let one_problem = "1234".to_string();
        let one_row = Row::new(one_problem);
        let lhs = one_row.len;
        let rhs = one_row.hl.len();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_problem_row() {
        // use Highlight::*;
        let one_problem = "            } else if (term_row / 3) == screen_y {".to_string();
        let one_row = Row::new(one_problem);
        let lhs = one_row.len;
        let rhs = one_row.hl.len();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_unequal_row() {
        let one_problem = "            } else if (term_row / 3) == screen_y {".to_string();
        let _problem_stmt_row = Row::new(one_problem.clone());
        let vec_hl_space_lhs: Vec<Highlight> = vec![
            Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal,
            Normal, // space
        ];
        let one_problem_space = one_problem[0..12].to_string();
        let space_hl = Row::new(one_problem_space);
        assert_eq!(vec_hl_space_lhs, space_hl.hl);
    }

    #[test]
    fn test_unequal_row_mine() {
        let one_problem = "            } else if (term_row / 3) == screen_y {".to_string();
        let vec_hl_until_num_lhs: Vec<Highlight> = vec![
            Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal,
            Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal, Normal,
        ];
        let until_number = one_problem[12..12 + 22].to_string();
        let until_number_hl = Row::new(until_number);
        assert_eq!(vec_hl_until_num_lhs.len(), until_number_hl.hl.len());
    }
}
