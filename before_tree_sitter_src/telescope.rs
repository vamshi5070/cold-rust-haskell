use crate::keyboard::Keyboard;
use crate::my_lib::*;
use crate::row::Row;
use crate::screen::Screen;
use crossterm::event::KeyEvent;
use crossterm::Result;

use crate::display_window::PassDisplay;

pub struct Telescope {
    prompt: String,
    // height: u16,
    // width: u16,
    // origin: Position,
    // cursor: Position,
    // rowoff: u16,
    // coloff: u16,
    pass_display: PassDisplay,
}

impl Telescope {
    pub fn new(prompt: String, terminal_size_width: u16, terminal_size_height: u16) -> Self {
        let width_space = terminal_size_width / 10;
        let height_space = terminal_size_height / 10;
        Telescope {
            prompt: prompt.clone(),
            pass_display: PassDisplay {
                height: terminal_size_height - (height_space * 2),
                width: terminal_size_width - width_space * 2,
                origin: Position::new((width_space, height_space)),
                cursor: Position::new((width_space + prompt.len() as u16 + 1, height_space)),
                rowoff: 0,
                coloff: 0,
            },
        }
    }

    pub fn display(&mut self, screen: &mut Screen) -> Result<()> {
        let row = Row::new(self.prompt.clone());
        let vec_row = vec![row];
        screen.draw_lines(&vec_row, &self.pass_display, None, None)?;
        screen.flush().unwrap();
        return Ok(());
    }

    pub fn handle_read_key(&mut self, keyboard: &Keyboard) -> Option<KeyEvent> {
        if let Ok(key_event) = keyboard.read() {
            Some(key_event)
        } else {
            None
        }
    }

    pub fn start(&mut self, screen: &mut Screen, keyboard: &Keyboard) -> Result<()> {
        loop {
            self.display(screen)?;
            let key_event = self.handle_read_key(keyboard).unwrap();
            let key = cross_key_to_cold(key_event);
            match key {
                Key::Normal(RegularKey::Char('q')) => {
                    break Ok(());
                }
                _ => {
                    continue;
                }
            }
        }
    }
}
