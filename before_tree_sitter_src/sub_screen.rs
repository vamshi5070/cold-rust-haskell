                if let Some((beg_cursor, end_cursor)) = maybe_highlight {
                    let (first_x, first_y) = beg_cursor.get_pos();
                    let (second_x, second_y) = end_cursor.get_pos();

                    if first_y == pos_y && second_y == pos_y {
                        // if last_index_unwrapped == 0 {
                        // counter = counter + last_index_unwrapped + 1;
                        // return Ok(());
                        // }
                        let x1 = first_x as usize;
                        let x2 = second_x as usize;
                        let y1 = counter;
                        let y2 = counter + last_index_unwrapped;
                        if y1 <= x1 && x2 <= y2 {
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[y1..x1].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[x1..x2].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x2 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[x2..y2].to_owned()))?
                                .queue(ResetColor)?;
                            counter = counter + last_index_unwrapped;
                            return Ok(());
                        }
                        if x2 >= y1 && x2 <= y2 {
                            // x2 is in between
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[y1..x2].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x2 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[x2..y2].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + last_index_unwrapped;
                            return Ok(());
                        }
                        if y1 <= x1 && y2 >= x1 {
                            // x1 is in between
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[y1..x1].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[x1..y2].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + last_index_unwrapped;
                            return Ok(());
                        }

                        if x1 <= y1 && y2 <= x2 {
                            // y1 and y2 are is in between

                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + y1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[y1..y2].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + last_index_unwrapped;
                            return Ok(());
                        }
                        self.stdout
                            .queue(cursor::MoveTo(
                                line_num_str_len + og_x + counter as u16,
                                screen_pos_y,
                            ))?
                            .queue(SetColors(Colors::new(fg_color, Black)))?
                            .queue(Print(
                                raw_str[counter..counter + last_index_unwrapped].to_owned(),
                            ))?
                            .queue(ResetColor)?;
                        counter = counter + last_index_unwrapped;
                        return Ok(());
                    }
                    if first_y == pos_y {
                        let x1 = first_x as usize;
                        // let x2 = second_x as usize;
                        let y1 = counter;
                        let y2 = counter + last_index_unwrapped;
                        self.stdout
                            .queue(cursor::MoveTo(
                                line_num_str_len + og_x + y1 as u16,
                                screen_pos_y,
                            ))?
                            .queue(SetColors(Colors::new(fg_color, transparent)))?
                            .queue(Print(raw_str[y1..x1].to_owned()))?
                            .queue(ResetColor)?;
                        self.stdout
                            .queue(cursor::MoveTo(
                                line_num_str_len + og_x + y1 as u16,
                                screen_pos_y,
                            ))?
                            .queue(SetColors(Colors::new(fg_color, Black)))?
                            .queue(Print(raw_str[x1..y2].to_owned()))?
                            .queue(ResetColor)?;

                        counter = counter + last_index_unwrapped;
                        return Ok(());
                    }
                }


                /*if let Some((beg_cursor, end_cursor)) = maybe_highlight {
                            let (first_x, first_y) = beg_cursor.get_pos();
                            let (second_x, second_y) = end_cursor.get_pos();

                            if pos_y == first_y && pos_y == second_y {
                                // let pos_x = counter;
                                let x1 = first_x as usize;
                                let x2 = second_x as usize;
                                let y1 = counter;
                                let y2 = raw_str.len() - 1;

                                /*if x1 <= y1 && x2 <= y2 {
                                    self.stdout
                                        .queue(cursor::MoveTo(
                                            line_num_str_len + og_x + counter as u16,
                                            screen_pos_y,
                                        ))?
                                        .queue(SetColors(Colors::new(fg_color, transparent)))?
                                        // .queue(Print(raw_str[y1..=x2].to_owned()))?
                                        .queue(ResetColor)?;
                                    self.stdout
                                        .queue(cursor::MoveTo(
                                            line_num_str_len + og_x + x2 as u16,
                                            screen_pos_y,
                                        ))?
                                        .queue(SetColors(Colors::new(fg_color, Black)))?
                                        // .queue(Print(raw_str[x2..=y2].to_owned()))?
                                        .queue(ResetColor)?;
                                    return Ok(());
                                } */
                            }
                } */
                if let Some((beg_cursor, end_cursor)) = maybe_highlight {
                    let (first_x, first_y) = beg_cursor.get_pos();
                    let (second_x, second_y) = end_cursor.get_pos();

                    if first_y == pos_y && second_y == pos_y {
                        // if raw_str.len() - 1 == 0 {
                        // counter = counter + raw_str.len() - 1 + 1;
                        // return Ok(());
                        // }
                        let x1 = first_x as usize;
                        let x2 = second_x as usize;
                        let y1 = counter;
                        let y2 = counter + raw_str.len() - 1;
                        if y1 <= x1 && x2 <= y2 {
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[y1..x1].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[x1..x2].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x2 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[x2..y2].to_owned()))?
                                .queue(ResetColor)?;
                            counter = counter + raw_str.len() - 1;
                            return Ok(());
                        }
                        if x2 >= y1 && x2 <= y2 {
                            // x2 is in between
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[y1..x2].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x2 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[x2..].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + raw_str.len() - 1;
                            return Ok(());
                        }
                        if y1 <= x1 && y2 >= x1 {
                            // x1 is in between
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + counter as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, Black)))?
                                .queue(Print(raw_str[y1..x1].to_owned()))?
                                .queue(ResetColor)?;
                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + x1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[x1..y2].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + raw_str.len() - 1;
                            return Ok(());
                        }

                        if x1 <= y1 && y2 <= x2 {
                            // y1 and y2 are is in between

                            self.stdout
                                .queue(cursor::MoveTo(
                                    line_num_str_len + og_x + y1 as u16,
                                    screen_pos_y,
                                ))?
                                .queue(SetColors(Colors::new(fg_color, transparent)))?
                                .queue(Print(raw_str[y1..y2].to_owned()))?
                                .queue(ResetColor)?;

                            counter = counter + raw_str.len() - 1;
                            return Ok(());
                        }
                    }
                }
