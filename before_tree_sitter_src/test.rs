fn main() {
    let cur_line = String::from("#include <stdio.h>");
    let x = cur_line.len();
    // for
    for (index, ch) in cur_line.chars().rev().enumerate() {
        if !ch.is_alphabetic() && index < x {
            println!("ch: {ch},x: {x},len - index: {index}");
            break;
        }
    }
}
