use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

use crate::display_window::PassDisplay;
use crate::file_system::*;
use crate::text_mode::*;
use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

use crossterm::style::Color;

pub type StdResult<T, E> = std::result::Result<T, E>;

pub enum ResultEditor {
    KeyReadFail,
}

#[derive(Clone)]
pub enum MessageStatus {
    DrawMessage(String),
    ClearMessage,
    NoChange,
}

#[derive(Default, Clone, Copy, PartialEq, Debug)]
pub struct Position {
    // x is horizontal cursor position with respect to the text editor (not physical screen)
    x: u16,
    // y is vertical cursor position with respect to the text editor (not physical screen)
    y: u16,
}

impl Position {
    pub fn above(&self, row: usize) -> bool {
        self.y < row as u16
    }

    // pub fn get_pos(&self) -> ()
    pub fn new((row, col): (u16, u16)) -> Self {
        Self { x: row, y: col }
    }

    pub fn set_x(&mut self, cursor_x: u16) {
        self.x = cursor_x;
    }

    pub fn set_y(&mut self, cursor_y: u16) {
        self.y = cursor_y;
    }

    pub fn get_x(&self) -> u16 {
        return self.x;
    }

    pub fn get_y(&self) -> u16 {
        return self.y;
    }

    pub fn set_pos(&mut self, (row, col): (u16, u16)) {
        self.x = row;
        self.y = col;
    }

    pub fn get_pos(&self) -> (u16, u16) {
        (self.x, self.y)
    }

    pub fn get_pos_as_pos(&self) -> Self {
        Self {
            x: self.x,
            y: self.y,
        }
    }

    pub fn is_equal(&self, another: Self) -> bool {
        self.x == another.get_x() && self.y == another.get_y()
    }

    pub fn compare(&self, another: Self) -> (Self, Self) {
        let (another_x, another_y) = another.get_pos();
        let first_pos;
        let second_pos;
        if self.y < another_y {
            first_pos = self.to_owned();
            second_pos = another;
        } else if self.y > another_y {
            first_pos = another;
            second_pos = self.to_owned();
        } else {
            if self.x < another_x {
                first_pos = self.to_owned();
                second_pos = another;
            } else {
                first_pos = another;
                second_pos = self.to_owned();
            }
        }
        (first_pos, second_pos)
    }

    pub fn set_pos_as_pos(&mut self, another: Self) {
        let (another_x, another_y) = another.get_pos();
        self.set_x(another_x);
        self.set_y(another_y);
    }
}

// #[derive(PartialEq, Debug)]
// pub struct PromptWithKind {
//     prompt: Vec<String>,
//     kind: MinibufferType,
// }
//
#[derive(PartialEq, Debug)]
pub enum MinibufferType {
    Multiple,
    Single,
}

// #[derive(PartialEq, Debug)]
// pub enum MiniResult {
//     Prompt(PromptWithKind),
//     Result(String),
//     Cancel,
// }

#[derive(Debug, PartialEq)]
pub enum Windows {
    TextEditor(PathBuf),
    FileManager(PathBuf),
    // MinibufferWant,
    Current,
    Exit,
}

pub enum Status {
    Search,
    // FileSave,
}

pub enum EditorOperation {
    Insert(Position, char),
    Delete(Position),
}

// #[derive(Clone,Copy)]
pub enum WindManager {
    TextEditorMode(Rc<RefCell<TextMode>>),
    FileManagerMode(Rc<RefCell<FileSystem>>),
    Null,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Language {
    Rust,
    C,
    Fundemental,
    Dired,
}

impl Default for Language {
    fn default() -> Self {
        Language::Fundemental
    }
}

impl Language {
    pub fn show(&self) -> String {
        match self {
            Language::C => "C lang".to_string(),
            Language::Rust => "rust".to_string(),
            Language::Fundemental => String::from("Fundemental"),
            Language::Dired => String::from("Dired"),
        }
    }
}
pub fn get_directory_from_file_path(file_path: &PathBuf) -> Option<PathBuf> {
    if let Some(parent) = file_path.parent() {
        Some(parent.to_path_buf())
    } else {
        None
    }
}

// function for scrolling rows
pub fn scroll(pass_display: &PassDisplay) -> (u16, u16) {
    let (pos_x, pos_y) = pass_display.cursor.get_pos();
    let mut new_rowoff: u16 = pass_display.rowoff;
    let mut new_coloff: u16 = pass_display.coloff;
    let size_x = pass_display.width;

    let size_y = pass_display.height;
    if pos_y < pass_display.rowoff {
        new_rowoff = pos_y;
    } else if pos_y >= pass_display.rowoff + size_y {
        new_rowoff = pos_y - size_y + 1;
    }
    if pos_x < pass_display.coloff {
        new_coloff = pos_x;
    } else if pos_x >= pass_display.coloff + size_x {
        new_coloff = pos_x - size_x + 1;
    }
    // if ()
    (new_rowoff, new_coloff)
}

#[derive(Clone, Copy)]
pub enum RegularKey {
    Char(char),
    Up,
    Down,
    Enter,
    Right,
    Left,
    Backspace,
    Home,
    End,
    Esc,
    None,
}

#[derive(Clone, Copy)]
pub enum Key {
    Normal(RegularKey),
    Shift(RegularKey),
    Ctrl(RegularKey),
    Meta(RegularKey),
}

pub fn helper(key_code: KeyCode) -> RegularKey {
    return match key_code {
        KeyCode::Char(ch) => RegularKey::Char(ch),
        KeyCode::Up => RegularKey::Up,
        KeyCode::Down => RegularKey::Down,
        KeyCode::Enter => RegularKey::Enter,
        KeyCode::Left => RegularKey::Left,
        KeyCode::Right => RegularKey::Right,
        KeyCode::Backspace => RegularKey::Backspace,
        KeyCode::Home => RegularKey::Home,
        KeyCode::End => RegularKey::End,
        KeyCode::Esc => RegularKey::Esc,
        _ => RegularKey::None,
    };
}

pub fn cross_key_to_cold(key_event: KeyEvent) -> Key {
    match key_event.modifiers {
        KeyModifiers::NONE => Key::Normal(helper(key_event.code)),
        KeyModifiers::SHIFT => Key::Shift(helper(key_event.code)),
        KeyModifiers::ALT => Key::Meta(helper(key_event.code)),
        KeyModifiers::CONTROL => Key::Ctrl(helper(key_event.code)),
        _ => Key::Normal(RegularKey::Enter),
    }
}

pub fn is_escape(key: &Key) -> bool {
    match key {
        Key::Ctrl(RegularKey::Char('g')) => true,
        _ => false,
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Highlight {
    Normal,
    HlString, // since String is already there
    Number,
    Match,
    Comment,
    Directory,
    // Select,
}

pub fn my_is_seperator(c: char) -> bool {
    c.is_whitespace()
}

impl Highlight {
    pub fn syntax_to_color(&self) -> Color {
        match self {
            Highlight::Normal => Color::White,
            Highlight::Number => Color::Red,
            Highlight::Match => Color::White,
            Highlight::HlString => Color::Magenta,
            Highlight::Comment => Color::Yellow,
            Highlight::Directory => Color::Blue,
            // Highlight::Select => Color::Rgb {
            // r: 155,
            // g: 155,
            // b: 155,
            // },
        }
    }

    pub fn is_normal(&self) -> bool {
        self == &Highlight::Normal
    }
}

#[derive(Clone)]
pub struct Style {
    fg: Option<Color>,
    bg: Option<Color>,
    pub bold: bool,
    pub italic: bool,
}
