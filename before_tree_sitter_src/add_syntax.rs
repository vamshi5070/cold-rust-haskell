use crate::my_lib::Highlight;
use crate::my_lib::Highlight::*;

pub struct Syntax<'a> {
    source: &'a str,
    // len: usize,
    // start: usize,
    current: usize,
    // maybe_locations: Option<(Vec<usize>, usize)>, // prev_sep: bool,
    // prev_hl: Highlight,
    // hl: Vec<Highlight>,
}

impl<'a> Syntax<'a> {
    pub fn new(input: &'a str) -> Self {
        // , maybe_locations: Option<(Vec<usize>, usize)>
        Self {
            source: input,
            // len: input.len(),
            // start: 0,
            current: 0,
            // prev_sep: prev_sep,
            // prev_hl: prev_hl,
        }
    }

    fn peek(&self) -> Option<char> {
        if self.is_at_end() {
            None
        } else {
            Some(self.source.as_bytes()[self.current] as char)
        }
    }

    fn _peek_next(&self) -> Option<char> {
        if self.is_at_end() {
            None
        } else {
            Some(self.source.as_bytes()[self.current + 1] as char)
        }
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.source.len()
    }

    fn advance(&mut self) -> char {
        let ch = self.source.as_bytes()[self.current] as char;
        self.current += 1;
        return ch;
    }

    pub fn get_hl_syntax(&mut self) -> Vec<Highlight> {
        let mut hl = Vec::new();
        let mut prev_sep = true;
        // let mut prev_hl = Normal;
        loop {
            let ch = self.advance();
            if ch.is_numeric() && prev_sep {
                hl.push(Highlight::Number);
                self.lex_number(&mut hl);
                // prev_hl = Number;
                prev_sep = false;
            } else if ch.is_whitespace() {
                hl.push(Highlight::Normal);
                // prev_hl = Normal;
                prev_sep = true;
            } else if ch == '"' {
                hl.push(Highlight::HlString);
                self.lex_string(&mut hl);
                prev_sep = false;
                // prev_hl = HlString;
            } else if ch == '/' {
                self.lex_comment(&mut hl);
                prev_sep = false;
                // prev_hl = Normal;
            } else {
                hl.push(Highlight::Normal);
                // prev_hl = Normal;
                prev_sep = false;
            }
            if self.is_at_end() {
                break;
            }
        }
        return hl;
    }

    fn lex_comment(&mut self, hl: &mut Vec<Highlight>) {
        if let Some(ch) = self.peek() {
            if ch == '/' {
                hl.push(Comment);
                hl.push(Comment);
                self.current += 1;
                loop {
                    if self.is_at_end() {
                        break;
                    } else {
                        self.advance();
                        hl.push(Comment);
                    }
                }
            } else {
                hl.push(Normal);
            }
        } else {
            hl.push(Normal);
        }
    }

    fn lex_string(&mut self, hl: &mut Vec<Highlight>) {
        if self.is_at_end() {
            return;
        }
        loop {
            if self.is_at_end() {
                break;
            }
            let ch = self.peek().unwrap();
            if ch == '"' {
                hl.push(HlString);
                // self.prev_hl = Normal;
                // self.prev_sep = false;
                self.current += 1;
                break;
            }
            if ch == '\\' {
                self.current += 1;
                hl.push(HlString);
                if self.peek().unwrap() == '"' {
                    self.current += 1;
                    hl.push(HlString);
                    // self.prev_hl = HlString;
                    // self.prev_sep = false;
                    continue;
                }
                // self.prev_hl = HlString;
                // self.prev_sep = false;
                continue;
            }
            hl.push(HlString);
            self.current += 1;
            // self.prev_hl = HlString;
            // self.prev_sep = false;
        }
    }

    fn lex_number(&mut self, hl: &mut Vec<Highlight>) {
        if self.is_at_end() {
            return;
        }

        loop {
            if self.is_at_end() {
                break;
            }
            let ch = self.peek().unwrap();
            if !ch.is_numeric() && ch != '.' {
                break;
            }
            hl.push(Highlight::Number);
            self.current += 1;
            // self.prev_hl = Number;
            // ch = self.advance();
            // self.prev_sep = false;
        }
    }
}
// }

#[cfg(test)]
mod tests {
    use super::*;
    // use crossterm::terminal;
    #[test]
    fn test_number_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1234"); //, false, Number);
        let lhs = vec![Number, Number, Number, Number];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_number_normal_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("34use"); //, false, Number);
        let lhs = vec![Number, Number, Normal, Normal, Normal];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_fractional_number_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1234.53"); //, false, Number);
        let lhs = vec![Number, Number, Number, Number, Number, Number, Number];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_spaces_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("1   "); //, false, Number);
        let lhs = vec![Number, Normal, Normal, Normal];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_paren_string_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("(\"\")"); //, false, Normal);
        let lhs = vec![Normal, HlString, HlString, Normal];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_backquotes_string_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("\"editor: \\\"emacs\\\""); //, false, HlString);
        let lhs = vec![
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString, HlString, HlString, HlString, HlString, HlString, HlString,
            HlString, HlString,
        ];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_string_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("\"emacs\""); //, false, HlString);
        let lhs = vec![
            HlString, HlString, HlString, HlString, HlString, HlString, HlString,
        ];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }

    #[test]
    fn test_comment_hl() {
        // empty
        use crate::my_lib::Highlight::*;
        let mut syntax = Syntax::new("//"); //, false, HlString);
        let lhs = vec![Comment, Comment];
        let rhs = syntax.get_hl_syntax();
        assert_eq!(lhs, rhs);
    }
}
