use crossterm::terminal;
use crossterm::Result;

mod add_syntax;
mod telescope;
mod binary_tree;
mod display_window;
mod document;
mod editor;
mod file_system;
mod keyboard;
mod minibuffer;
pub mod my_lib;
mod row;
mod screen;
mod search;
mod text_mode;

use editor::*;
use my_lib::*;
use std::fs;

// main is IO, the starting point
fn main() -> Result<()> {
    let mut cli_args = std::env::args();
    let (size_x, size_y) = terminal::size()?;

    if cli_args.len() > 2 {
        println!("Usage: Only one filename or no filename");
        return Ok(());
    } else if cli_args.len() == 1 {
        let abs_path = std::env::current_dir()?;
        let editor = Editor::new(Windows::FileManager(abs_path), size_y, size_x);
        editor?.start()
    } else {
        let file_name = cli_args.nth(1).unwrap();
        let abs_path = fs::canonicalize(file_name).unwrap();
        let editor = Editor::new(Windows::TextEditor(abs_path), size_y, size_x);
        editor?.start()
    }
}
