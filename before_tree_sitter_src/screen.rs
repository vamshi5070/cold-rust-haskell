use crossterm::style::Color;
use crossterm::style::Print;
use crossterm::style::{
    Color::{Black, Blue},
    // , Green, Magenta, Red, White, Yellow},
    Colors,
    ResetColor,
    SetBackgroundColor,
    SetColors,
};
use crossterm::terminal;
// use std::collections::HashMap;

use std::io::{stdout, Stdout, Write};
// use std::path::PathBuf;

use crate::display_window::PassDisplay;
use crate::my_lib::*;
use crate::row::*;

use crossterm::ExecutableCommand;
use crossterm::{cursor, QueueableCommand, Result};

pub struct Screen {
    stdout: Stdout,
    screen_width: u16,
    screen_height: u16,
}

impl Screen {
    pub fn new() -> Result<Self> {
        let (columns, rows) = terminal::size()?;
        Ok(Self {
            stdout: stdout(),
            screen_width: columns,
            screen_height: rows,
        })
    }

    pub fn enter_alternate_screen(&mut self) -> Result<()> {
        self.stdout.execute(terminal::EnterAlternateScreen)?;
        Ok(())
    }

    pub fn leave_alternate_screen(&mut self) -> Result<()> {
        self.stdout.execute(terminal::LeaveAlternateScreen)?;
        Ok(())
    }

    pub fn draw_lines(
        &mut self,
        rows: &[Row],
        pass_display: &PassDisplay,
        maybe_highlight: Option<(Position, Position)>,
        maybe_locations: Option<(Vec<(usize, Vec<usize>)>, usize)>,
    ) -> Result<()> {
        self.stdout.queue(cursor::Hide)?;
        let mut current_search_row_index = 0;
        let (og_x, og_y) = pass_display.origin.get_pos();
        if let Some((ref tup_vec, _length_search)) = maybe_locations {
            loop {
                if current_search_row_index >= tup_vec.len() {
                    break;
                }
                if pass_display.rowoff > tup_vec[current_search_row_index].0 as u16 {
                    current_search_row_index += 1;
                } else {
                    break;
                }
            }
        }
        for (row_index, row) in rows.iter().enumerate() {
            let pos_y = row_index + pass_display.rowoff as usize;
            let start = pass_display.coloff as usize;
            let end = if row.len > pass_display.coloff as usize + pass_display.width as usize {
                pass_display.coloff as usize + pass_display.width as usize
            } else {
                row.len
            };

            let to_be_rendered = match row.render_str().get(start..end) {
                Some(sub_row) => sub_row.to_string(),
                None => "".to_string(),
            };

            let hl_compressed = if start >= end {
                &Vec::new()
            } else {
                &row.hl_vec()[start..end]
            };

            self.stdout
                .queue(cursor::MoveTo(og_x, og_y + row_index as u16))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?;

            let maybe_locations_row = if let Some((ref tup_vec, length_search)) = maybe_locations {
                if current_search_row_index >= tup_vec.len() {
                    None
                } else {
                    let (search_y, search_xs) = &tup_vec[current_search_row_index];
                    if *search_y == pos_y {
                        let ret_value = Some((search_xs.as_ref(), length_search));
                        current_search_row_index += 1;
                        ret_value
                    } else {
                        None
                    }
                }
            } else {
                None
            };
            self.render_text_syntax(
                pass_display.rowoff,
                row_index as u16 + og_y,
                &to_be_rendered,
                &hl_compressed,
                pass_display.coloff,
                og_x,
                maybe_highlight,
                maybe_locations_row,
                // .get(row_index),
            )?;
        }

        for i in rows.len() as u16..pass_display.height {
            self.stdout
                .queue(cursor::MoveTo(og_x, og_y + i))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?;
        }
        Ok(())
    }

    pub fn change_bg(&mut self) -> Result<()> {
        self.stdout.queue(SetBackgroundColor(Color::Rgb {
            r: 155,
            g: 155,
            b: 155,
        }))?;
        return Ok(());
    }

    pub fn clear_given_line(&mut self, pos_x: u16, pos_y: u16) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?;
        return Ok(());
    }

    fn loop_same(&self, hl_vec: &[Highlight], hl: Highlight, start: usize) -> usize {
        let mut end = start + 1;
        loop {
            if end >= hl_vec.len() {
                break;
            }
            if hl != hl_vec[end] {
                break;
            }
            end += 1;
        }
        return end;
    }

    pub fn render_text_syntax(
        &mut self,
        rowoff: u16,
        screen_pos_y: u16,
        raw_str: &str,
        hl_vec: &[Highlight],
        _coloff: u16,
        og_x: u16,
        maybe_highlight: Option<(Position, Position)>,
        maybe_locations: Option<(&[usize], usize)>,
    ) -> Result<()> {
        let mut hl_index = 0;
        let pos_y = screen_pos_y + rowoff;
        let transparent = Color::Rgb {
            r: 155,
            g: 155,
            b: 155,
        };

        let search_cyan = Color::Rgb {
            r: 205,
            g: 205,
            b: 155,
        };

        let mut my_bg = Black;
        if let Some((start_cursor, end_cursor)) = maybe_highlight {
            let (first_x, first_y) = start_cursor.get_pos();
            let (second_x, second_y) = end_cursor.get_pos();
            if pos_y > first_y && pos_y < second_y {
                my_bg = transparent;
            }

            if pos_y == first_y && pos_y == second_y {
                loop {
                    if hl_index >= hl_vec.len() {
                        break;
                    }
                    let hl = hl_vec[hl_index];
                    let fg_color = hl.syntax_to_color();
                    let end = hl_index + 1;
                    let bg = if hl_index >= first_x as usize && hl_index <= second_x as usize {
                        transparent
                    } else {
                        Black
                    };
                    self.stdout
                        .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                        .queue(SetColors(Colors::new(fg_color, bg)))?
                        .queue(Print(raw_str[hl_index..end].to_owned()))?
                        .queue(ResetColor)?;
                    hl_index = end;
                }
                return Ok(());
            }
            if pos_y == first_y {
                loop {
                    if hl_index >= hl_vec.len() {
                        break;
                    }
                    let hl = hl_vec[hl_index];
                    let fg_color = hl.syntax_to_color();
                    let end = hl_index + 1;
                    let bg = if hl_index >= first_x as usize {
                        transparent
                    } else {
                        Black
                    };
                    self.stdout
                        .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                        .queue(SetColors(Colors::new(fg_color, bg)))?
                        .queue(Print(raw_str[hl_index..end].to_owned()))?
                        .queue(ResetColor)?;
                    hl_index = end;
                }
                return Ok(());
            }
            if pos_y == second_y {
                loop {
                    if hl_index >= hl_vec.len() {
                        break;
                    }
                    let hl = hl_vec[hl_index];
                    let fg_color = hl.syntax_to_color();
                    let end = hl_index + 1;
                    let bg = if hl_index <= second_x as usize {
                        transparent
                    } else {
                        Black
                    };
                    self.stdout
                        .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                        .queue(SetColors(Colors::new(fg_color, bg)))?
                        .queue(Print(raw_str[hl_index..end].to_owned()))?
                        .queue(ResetColor)?;
                    hl_index = end;
                }
                return Ok(());
            }
        }
        loop {
            if hl_index >= hl_vec.len() {
                break;
            }
            let hl = hl_vec[hl_index];
            let fg_color = hl.syntax_to_color();
            let end = self.loop_same(hl_vec, hl, hl_index);
            self.stdout
                .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                .queue(SetColors(Colors::new(fg_color, my_bg)))?
                .queue(Print(raw_str[hl_index..end].to_owned()))?
                .queue(ResetColor)?;
            hl_index = end;
        }
        if let Some((vec_in_row, len)) = maybe_locations {
            for start_index in vec_in_row {
                hl_index = *start_index;
                loop {
                    if hl_index >= hl_vec.len() || hl_index >= start_index + len {
                        break;
                    }
                    let hl = hl_vec[hl_index];
                    let fg_color = hl.syntax_to_color();
                    // let end = self.loop_same(hl_vec, hl, hl_index);
                    let end = hl_index + 1;
                    self.stdout
                        .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                        .queue(SetColors(Colors::new(fg_color, search_cyan)))?
                        .queue(Print(raw_str[hl_index..end].to_owned()))?
                        .queue(ResetColor)?;
                    hl_index = end;
                }
            }
        }
        Ok(())
    }

    pub fn render_text_syntax_on_hl(
        &mut self,
        rowoff: u16,
        screen_pos_y: u16,
        raw_str: &str,
        hl_vec: &[Highlight],
        _coloff: u16,
        og_x: u16,
        maybe_highlight: Option<(Position, Position)>,
    ) -> Result<()> {
        let mut hl_index = 0;
        let transparent = Color::Rgb {
            r: 155,
            g: 155,
            b: 155,
        };
        let global_bg = if let Some((beg_cursor, end_cursor)) = maybe_highlight {
            let pos_y = screen_pos_y + rowoff;
            let (_, first_y) = beg_cursor.get_pos();
            let (_, second_y) = end_cursor.get_pos();
            if pos_y > first_y && pos_y < second_y {
                transparent
            } else {
                Black
            }
        } else {
            Black
        };
        let my_bg = global_bg;
        // if let(st)
        loop {
            if hl_index >= hl_vec.len() {
                break;
            }
            let hl = hl_vec[hl_index];
            let fg_color = hl.syntax_to_color();
            let end = self.loop_same(hl_vec, hl, hl_index);
            self.stdout
                .queue(cursor::MoveTo(og_x + hl_index as u16, screen_pos_y))?
                .queue(SetColors(Colors::new(fg_color, my_bg)))?
                .queue(Print(raw_str[hl_index..end].to_owned()))?
                .queue(ResetColor)?;
            hl_index = end;
        }
        Ok(())
    }

    //     let mut render_nums = |local_counter: Option<usize>, fg_color, bg_color| -> Result<()> {
    //         let my_bg = bg_color;

    //         if let Some(last_index_unwrapped) = local_counter {
    //             self.stdout
    //                 .queue(cursor::MoveTo(
    //                     line_num_str_len + og_x + counter as u16,
    //                     screen_pos_y,
    //                 ))?
    //                 .queue(SetColors(Colors::new(fg_color, my_bg)))?
    //                 .queue(Print(
    //                     raw_str[counter..counter + last_index_unwrapped].to_owned(),
    //                 ))?
    //                 .queue(ResetColor)?;
    //             counter = counter + last_index_unwrapped;
    //             return Ok(());
    //         } else {
    //             self.stdout
    //                 .queue(cursor::MoveTo(
    //                     line_num_str_len + og_x + counter as u16,
    //                     screen_pos_y,
    //                 ))?
    //                 .queue(SetColors(Colors::new(fg_color, my_bg)))?
    //                 .queue(Print(raw_str[counter..].to_owned()))?
    //                 .queue(ResetColor)?;
    //             return Ok(());
    //         }
    //     };

    //     'outer: loop {
    //         match prev_color {
    //             Some(Highlight::Normal) => {
    //                 let mut local_counter = 1;
    //                 // if counter + local_counter >= raw_str_len {
    //                 // break 'outer;
    //                 // }
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     if width_restrict_counter > raw_str_len {
    //                         render_nums(None, White, global_bg)?;
    //                         break 'outer;
    //                     }
    //                     match temp {
    //                         Some(Highlight::Normal) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, White, global_bg)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             // local_counter = 1;
    //                             prev_color = temp;
    //                             render_nums(Some(local_counter), White, global_bg)?;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }

    //             Some(Highlight::Directory) => {
    //                 let mut local_counter = 1;
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     if width_restrict_counter > raw_str_len {
    //                         render_nums(None, Blue, global_bg)?;
    //                         break 'outer;
    //                     }
    //                     match temp {
    //                         Some(Highlight::Directory) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, Blue, global_bg)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             render_nums(Some(local_counter), Blue, global_bg)?;
    //                             prev_color = temp;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }

    //             Some(Highlight::Number) => {
    //                 let mut local_counter = 1;
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     if width_restrict_counter > raw_str_len {
    //                         render_nums(None, Red, global_bg)?;
    //                         break 'outer;
    //                     }
    //                     match temp {
    //                         Some(Highlight::Number) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, Red, global_bg)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             render_nums(Some(local_counter), Red, global_bg)?;
    //                             prev_color = temp;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }

    //             Some(Highlight::Match) => {
    //                 let mut local_counter = 1;
    //                 if width_restrict_counter > raw_str_len {
    //                     render_nums(None, White, Green)?;
    //                     break 'outer;
    //                 }
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     match temp {
    //                         Some(Highlight::Match) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, White, Green)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             render_nums(Some(local_counter), White, Green)?;
    //                             prev_color = temp;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }

    //             Some(Highlight::HlString) => {
    //                 let mut local_counter = 1;
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     if width_restrict_counter > raw_str_len {
    //                         render_nums(None, Magenta, global_bg)?;
    //                         break 'outer;
    //                     }

    //                     match temp {
    //                         Some(Highlight::HlString) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, Magenta, global_bg)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             render_nums(Some(local_counter), Magenta, global_bg)?;
    //                             prev_color = temp;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }

    //             Some(Highlight::Comment) => {
    //                 let mut local_counter = 1;
    //                 'inner: loop {
    //                     let temp = cur_row_iter.next();
    //                     if width_restrict_counter > raw_str_len {
    //                         render_nums(None, Yellow, global_bg)?;
    //                         break 'outer;
    //                     }

    //                     match temp {
    //                         Some(Highlight::Comment) => {
    //                             local_counter += 1;
    //                             width_restrict_counter += 1;
    //                         }
    //                         None => {
    //                             render_nums(None, Yellow, global_bg)?;
    //                             break 'outer;
    //                         }
    //                         _ => {
    //                             render_nums(Some(local_counter), Yellow, global_bg)?;
    //                             prev_color = temp;
    //                             break 'inner;
    //                         }
    //                     }
    //                 }
    //             }
    //             None => {
    //                 break 'outer;
    //             }
    //         }
    //     }
    //     Ok(())
    // }

    pub fn draw_strings_vertical(
        &mut self,
        rows: &[String],
        coloff: u16,
        height: u16,
    ) -> Result<()> {
        for (row_index, row) in rows.iter().enumerate() {
            let start = coloff as usize;
            let end = if row.len() > coloff as usize + self.screen_width as usize {
                coloff as usize + self.screen_width as usize
            } else {
                row.len()
            };
            let to_be_rendered = match row.get(start..end) {
                Some(sub_row) => sub_row.to_string(),
                None => "".to_string(),
            };
            self.stdout
                .queue(cursor::MoveTo(
                    0,
                    (row_index as u16 + height).try_into().unwrap(),
                ))?
                .queue(terminal::Clear(terminal::ClearType::CurrentLine))?
                .queue(Print(to_be_rendered))?;
        }
        Ok(())
    }

    pub fn draw_strings_vertical_minibuffer(
        &mut self,
        prompt: &str,
        rows: &[String],
        pass_display: PassDisplay,
    ) -> Result<()> {
        self.hide_cursor();
        self.stdout
            .queue(cursor::MoveTo(
                0,
                // pass_display.origin.get_x(),
                pass_display.origin.get_y(),
            ))?
            .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
            .queue(Print(prompt.to_owned()))?;

        for (row_index, row) in rows.iter().enumerate() {
            let start = pass_display.coloff as usize;
            let end = if row.len() > pass_display.coloff as usize + self.screen_width as usize {
                pass_display.coloff as usize + self.screen_width as usize
            } else {
                row.len()
            };
            let to_be_rendered = match row.get(start..end) {
                Some(sub_row) => sub_row.to_string(),
                None => "".to_string(),
            };
            self.stdout
                .queue(cursor::MoveTo(
                    pass_display.origin.get_x(),
                    row_index as u16 + pass_display.origin.get_y(),
                    // (row_index as u16 + pass_display.height).try_into().unwrap(),
                ))?
                .queue(terminal::Clear(terminal::ClearType::UntilNewLine))?
                .queue(Print(to_be_rendered))?;
        }
        self.show_cursor();
        Ok(())
    }

    pub fn show_cursor(&mut self) {
        self.stdout.queue(cursor::Show).unwrap();
    }

    pub fn hide_cursor(&mut self) {
        self.stdout.queue(cursor::Hide).unwrap();
    }

    pub fn draw_message(&mut self, cursor_y: u16, msg: &String) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(0, cursor_y))?
            .queue(Print(msg))?;
        Ok(())
    }

    pub fn draw_on_status_vertical(&mut self, msg: &str, pos_y: u16, pos_x: u16) -> Result<()> {
        let start = 0;
        let end = if self.screen_width as usize <= msg.len() {
            self.screen_width as usize
        } else {
            msg.len()
        };
        let printable_str = msg[start..end].to_owned();
        self.stdout
            .queue(SetColors(Colors::new(Black, Blue)))?
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(Print(printable_str))?
            .queue(ResetColor)?;
        Ok(())
    }

    pub fn draw_status_bar_vertical(&mut self, pos_x: u16, pos_y: u16, width: u16) -> Result<()> {
        let mut status: String = String::from("");
        status.push_str(&"█".repeat(width.saturating_sub(0).into()));
        self.stdout
            .queue(SetColors(Colors::new(Blue, Black)))?
            .queue(cursor::MoveTo(pos_x, pos_y))?
            .queue(Print(status))?
            .queue(ResetColor)?;
        Ok(())
    }

    pub fn clear_all(&mut self) -> Result<()> {
        self.stdout
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(cursor::MoveTo(0, 0))?;
        Ok(())
    }

    pub fn clear_row(&mut self, cursor_y: u16) -> Result<()> {
        self.stdout
            .queue(cursor::MoveTo(0, cursor_y))?
            .queue(terminal::Clear(terminal::ClearType::CurrentLine))?;

        Ok(())
    }

    pub fn flush(&mut self) -> Result<()> {
        self.stdout.flush()
    }

    pub fn cursor_position(&self) -> Result<(u16, u16)> {
        cursor::position()
    }

    // jump to given position
    pub fn render_cursor(&mut self, pos: &Position) -> Result<()> {
        let (posx, posy) = pos.get_pos();
        self.stdout.queue(cursor::MoveTo(posx, posy))?;
        Ok(())
    }

    pub fn bounds(&self) -> Position {
        Position::new((self.screen_width, self.screen_height))
    }
}

#[cfg(test)]
mod tests {
    // use std::borrow::BorrowMut;
    #[test]
    fn _test_skip_same_hl() {
        use super::*;
        let _screen = Screen::new();
        let _empty_row = Row::new("".to_string());
        // screen.loop_same(empty_row.get_hl(),)
        // screen.
        // let (size_y, _) = terminal::size().unwrap();
        // let mut minibuffer = MinibufferPrompt::new(size_y - 3 + 1);
        // let before_focus = minibuffer.get_focus();
        // minibuffer.borrow_mut().flip_is_focus();
        // let after_focus = minibuffer.get_focus();
        // assert_eq!(before_focus, !after_focus);
    }
}
