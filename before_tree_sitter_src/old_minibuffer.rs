// use crate::keyboard::*;
use crate::my_lib::*;
use crate::screen::Screen;
use crossterm::{
    // event::{KeyCode, KeyEvent, KeyModifiers},
    terminal,
    Result,
};
use errno::errno;

pub struct MinibufferPrompt {
    is_focus: bool,
    prompt: String,
    cursor: Position,
    coloff: u16,
    rowoff: u16,
    vertical_limit: u16,
    text_rows: Vec<String>,
    input: String,
    kind: MinibufferType,
}

impl MinibufferPrompt {
    pub fn new(size_y: u16) -> Self {
        Self {
            is_focus: false,
            prompt: String::from(""),
            rowoff: 0,
            coloff: 0,
            vertical_limit: size_y,
            input: String::from(""),
            cursor: Position::new((0, size_y)),
            text_rows: vec![],
            kind: MinibufferType::Multiple,
        }
    }

    pub fn assign_text_rows(&mut self, text_rows: Vec<String>) -> Result<()> {
        self.text_rows = text_rows;
        self.prompt = self.text_rows.get(0).unwrap().to_string();
        Ok(())
    }

    // function for scrolling rows
    pub fn scroll(&mut self, screen: &Screen) {
        let (pos_x, pos_y) = self.cursor.get_pos();
        let (size_x, size_y) = screen.bounds().get_pos();
        if pos_y < self.rowoff {
            self.rowoff = pos_y;
        } else if pos_y >= self.rowoff + size_y {
            self.rowoff = pos_y - size_y + 1;
        }
        if pos_x < self.coloff {
            self.coloff = pos_x;
        } else if pos_x >= self.coloff + size_x {
            self.coloff = pos_x - size_x + 1;
        }
    }

    pub fn display(&mut self, screen: &mut Screen) -> Result<()> {
        self.text_rows[0] = self.prompt.clone() + &self.input;
        self.scroll(screen);

        screen.draw_strings_vertical(
            &self.text_rows,
            // [0..=1],
            self.coloff,
            self.vertical_limit,
        )?;

        let (pos_x, pos_y) = self.cursor.get_pos();
        let new_pos_y = pos_y - self.rowoff;

        let new_pos_x =
            pos_x - self.coloff + self.text_rows[0].len() as u16 - self.input.len() as u16;

        let custom_pos = Position::new((new_pos_x, new_pos_y));
        screen.render_cursor(&custom_pos)?;
        screen.flush()
    }

    pub fn keypress_handling(&mut self, key: &Key) -> Windows {
        self.process_keypress(key)
    }

    pub fn flip_is_focus(&mut self) {
        self.is_focus = !self.is_focus;
    }

    pub fn set_focus(&mut self) {
        self.is_focus = true;
    }

    pub fn get_input(&self) -> String {
        self.input.clone()
    }

    pub fn set_minibuffer_type_single(&mut self) {
        self.kind = MinibufferType::Single;
    }

    pub fn set_minibuffer_type_multiple(&mut self) {
        self.kind = MinibufferType::Multiple;
    }

    pub fn set_input(&mut self, new_input: &str) {
        self.input = new_input.to_string();
    }

    pub fn set_cursor_pos(&mut self, (row, col): (u16, u16)) {
        self.cursor.set_pos((row, col));
    }

    pub fn get_cursor_pos(&mut self) -> (u16, u16) {
        self.cursor.get_pos()
    }

    pub fn get_focus(&self) -> bool {
        self.is_focus
    }

    pub fn process_keypress(&mut self, key: &Key) -> Windows {
        match key {
            // Key::Ctrl(RegularKey::Char('g')) => {
            //     self.is_focus = false;
            //     self.prompt = "".to_string();
            //     self.cursor = Position::new((0, self.vertical_limit));
            //
            //     self.input = String::from("");
            //     Windows::Minibuffer(MiniResult::Cancel)
            // }
            // Key::Normal(RegularKey::Char(ch)) | Key::Shift(RegularKey::Char(ch)) => {
            //     match self.kind {
            //         MinibufferType::Multiple => {
            //             self.insert_char(*ch);
            //             Windows::Current
            //         }
            //         MinibufferType::Single => {
            //             self.insert_char(*ch);
            //             Windows::Minibuffer(MiniResult::Result(ch.to_string()))
            //         }
            //     }
            // }
            // Key::Normal(RegularKey::Enter) => {
            //     self.is_focus = false;
            //
            //     self.cursor = Position::new((0, self.vertical_limit));
            //     if self.input.is_empty() {
            //         Windows::Minibuffer(MiniResult::Cancel)
            //     } else {
            //         let temp = self.input.clone();
            //         self.input = String::from("");
            //         Windows::Minibuffer(MiniResult::Result(temp))
            //     }
            // }
            // KeyEvent {
            //     code: KeyCode::Backspace,
            //     ..
            // } => {
            Key::Normal(RegularKey::Backspace) => {
                self.handle_backspace();
                Windows::Current
            }
            _ => Windows::Current,
            //     KeyEvent {
            //         code: KeyCode::Home,
            //         ..
            //     } => Windows::Current,
            //     KeyEvent {
            //         code: KeyCode::End, ..
            //     } => Windows::Current,
            //
            //     KeyEvent { .. } => Windows::Current, // _ => Windows::Current
        }
    }

    pub fn insert_char(&mut self, ch: char) {
        let (pos_x, pos_y) = self.cursor.get_pos();
        if usize::from(pos_x) >= self.input.len() {
            self.input.push(ch);
        } else {
            self.input.insert(pos_x.into(), ch);
        }
        self.cursor.set_pos((pos_x + 1, pos_y))
    }

    pub fn backspace(&mut self, at: usize) {
        let current_row = self.input.clone();

        let index = at;
        let mut result = String::with_capacity(current_row.len() - 1);

        if index > 0 {
            result.push_str(&current_row[0..index]);
        }

        if index < current_row.len() - 1 {
            result.push_str(&current_row[index + 1..]);
        }
        self.input = result.clone();
    }

    pub fn handle_backspace(&mut self) {
        let (pos_x, pos_y) = self.cursor.get_pos();

        if pos_x == 0 {
        } else {
            self.backspace((pos_x - 1) as usize);
            self.cursor.set_pos((pos_x - 1, pos_y))
        }
    }

    pub fn get_text(&mut self) -> String {
        self.prompt.clone() + &self.input
    }

    pub fn set_prompt(&mut self, prompt_text: String) {
        self.prompt = prompt_text;
    }

    pub fn die<S: Into<String>>(&mut self, message: S, screen: &mut Screen) {
        let _ = screen.clear_all();
        let _ = terminal::disable_raw_mode();
        eprintln!("{}: {}", message.into(), errno());
        std::process::exit(1);
    }
    pub fn operation_of_return_key(&mut self) -> (bool, String) {
        (true, "".to_string())
    }
}

/*
#[cfg(test)]
mod tests {
    use std::borrow::BorrowMut;
    #[test]
    fn test_minibuffer_flip_focus() {
        use super::*;
        let (size_y, _) = terminal::size().unwrap();
        let mut minibuffer = MinibufferPrompt::new(size_y - 3 + 1);
        let before_focus = minibuffer.get_focus();
        minibuffer.borrow_mut().flip_is_focus();
        let after_focus = minibuffer.get_focus();
        assert_eq!(before_focus, !after_focus);
    }

    #[test]
    fn test_miniresult_empty_return() {
        use super::*;
        let (size_y, _) = terminal::size().unwrap();
        let mut minibuffer = MinibufferPrompt::new(size_y - 3 + 1);
        // let key_event_enter = KeyEvent {
        //     code: KeyCode::Enter,
        //     modifiers: KeyModifiers::empty(),
        //     kind: crossterm::event::KeyEventKind::Press,
        //     state: crossterm::event::KeyEventState::empty(),
        // };
        let key = Key::Normal(RegularKey::Enter);
        let msg_windows = minibuffer.keypress_handling(&key);
        assert_eq!(Windows::Minibuffer(MiniResult::Cancel), msg_windows);
    }
}
*/
