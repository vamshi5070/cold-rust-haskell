// Simple text viewer using rope 'ropey' crate as core data structre 

use crossterm;
use crossterm::cursor;
use crossterm::cursor::RestorePosition;
use crossterm::event;
use crossterm::event::Event;
use crossterm::event::KeyEvent;
use crossterm::queue;
use crossterm::style;
use crossterm::terminal;
use crossterm::ExecutableCommand;
use crossterm::QueueableCommand;
use crossterm::Result;
use std::fs::File;
use std::path::Path;

use ropey::Rope;
use ropey::RopeBuilder;

use std::io::{self, Write};
use std::io::{BufReader, BufWriter};

fn arrow_keys(key_code: event::KeyCode, my_cursor: &mut (u16, u16), term_row: u16, term_col: u16) {
    match key_code {
        event::KeyCode::Right => {
            if my_cursor.0 < term_col - 1 {
                my_cursor.0 += 1;
            }
        }
        event::KeyCode::Left => {
            if my_cursor.0 > 0 {
                my_cursor.0 -= 1;
            }
        }
        event::KeyCode::Up => {
            if my_cursor.1 > 0 {
                my_cursor.1 -= 1;
            }
        }
        event::KeyCode::Down => {
            if my_cursor.1 < term_row - 1 {
                my_cursor.1 += 1;
            }
        }
        _ => (),
    }
}

fn main() -> Result<()> {
    let mut stdout = io::stdout();
    let (term_col, term_row) = terminal::size()?;
    let filename = "test-src/main.rs";
    // let path = Path::new("test-src/main.rs");
    // let test_file = File::open(&path)?;
    // let mut
    // let first_line = String::new();

    let mut text = Rope::from_reader(BufReader::new(File::open(filename)?))?;

    let start_idx = text.line_to_char(0);
    let end_idx = text.line_to_char(1);
    // println!("Execution starts here:   \n\n\n");
    let first_line = text.slice(start_idx..end_idx);
    // println!("{}", first_line.slice(0..5));
    // println!("\n\n");
    // return Ok(());

    terminal::enable_raw_mode()?;
    stdout.execute(crossterm::terminal::EnterAlternateScreen)?;
    stdout.execute(terminal::Clear(terminal::ClearType::All))?;
    let mut my_cursor: (u16, u16) = (0, 0);
    stdout.execute(cursor::MoveTo(my_cursor.0, my_cursor.1))?;
    loop {
        let mut screen_y = 0;
        queue!(stdout, cursor::Hide)?;
        // let mut local_builder = RopeBuilder::new();
        loop {
            if screen_y > term_row - 2 {
                break;
            } else {
                queue!(stdout, terminal::Clear(terminal::ClearType::UntilNewLine))?;
                queue!(stdout, cursor::MoveTo(0, screen_y))?;
                queue!(stdout, crossterm::style::Print(first_line))?;
            }
            screen_y += 1;
        }
        // let rope = local_builder.finish();
        // stdout
        //     .queue(cursor::Hide)?
        //     .queue(terminal::Clear(terminal::ClearType::All))?
        //     .queue(cursor::MoveTo(0, 0))?
        //     .queue(crossterm::style::Print(rope))?
        queue!(stdout, cursor::MoveTo(my_cursor.0, my_cursor.1))?;
        queue!(stdout, cursor::Show)?;
        stdout.flush()?;
        if let Event::Key(KeyEvent {
            code, modifiers, ..
        }) = crossterm::event::read()?
        {
            match code {
                event::KeyCode::Esc => break,
                event::KeyCode::Right => arrow_keys(code, &mut my_cursor, term_row, term_col),
                event::KeyCode::Left => arrow_keys(code, &mut my_cursor, term_row, term_col),
                event::KeyCode::Up => arrow_keys(code, &mut my_cursor, term_row, term_col),
                event::KeyCode::Down => arrow_keys(code, &mut my_cursor, term_row, term_col),
                _ => (),
            }
            match modifiers {
                crossterm::event::KeyModifiers::CONTROL => match code {
                    event::KeyCode::Char('a') => {
                        my_cursor.0 = 0;
                    }
                    event::KeyCode::Char('e') => {
                        my_cursor.0 = term_col - 1;
                    }
                    _ => (),
                },
                _ => (),
            }
        }
    }
    stdout.execute(RestorePosition)?;
    stdout.execute(crossterm::terminal::LeaveAlternateScreen)?;
    terminal::disable_raw_mode()?;
    Ok(())
}
