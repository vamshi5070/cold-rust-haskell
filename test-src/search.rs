Position
use crate::my_lib::*;
use crate::row::*;

pub fn search(text_rows: &Vec<Row>, searching_str: &str, cursor: &Position) -> Position {
    let (pos_x, pos_y) = cursor.get_pos();
    let cur_row = text_rows[pos_y as usize].render_str();
    let main_string = &cur_row[(pos_x as usize)..];
    let mut to_be_returned: Position = Position::new((pos_x, pos_y));
    if let Some(index) = main_string.find(&searching_str) {
        let new_x = index as u16 + pos_x;
        if new_x == pos_x {
            to_be_returned = search(
                text_rows,
                &searching_str,
                &Position::new((pos_x + 1, pos_y)),
            );
        } else {
            to_be_returned = Position::new((new_x, pos_y));
        }
    } else {
        for (row_index,row) in text_rows.iter().skip(pos_y as usize + 1).enumerate() {
            let current_str = row.render_str();
            if let Some(index) = current_str.find(&searching_str) {
                to_be_returned = Position::new((index as u16, (pos_y + 1) + row_index as u16));
		break;
            } else {
                continue;
            }
        }

    }
    to_be_returned
//emacs is cool
}
