
fn main() {
    let str_mine = "            } else if (term_row / 3) == screen_y {".to_string();
}

fn update_c_or_rust(&mut self, maybe_locations: Option<(Vec<usize>, usize)>) {
    if self.len == 0 {
        return;
    }
    self.hl = Vec::new();
    let mut prev_sep = false;
    let mut in_string_quote: Option<char> = None;
    let scs = String::from("//");
    let scs_len = scs.len();
    if let Some((cursors, len)) = maybe_locations {
        let mut cur_index = 0;
        // for (ch_index, ch) in self.string.chars().enumerate() {
        for (ch_index, ch) in self.render_str().chars().enumerate() {
            let prev_hl = if ch_index == 0 {
                &Highlight::Normal
            } else {
                self.hl.get(ch_index - 1).unwrap()
            };
            prev_sep = my_is_seperator(ch);
            if cur_index < cursors.len() && ch_index == cursors.get(cur_index).unwrap() + len {
                cur_index = cur_index + 1;
            }
            if cur_index < cursors.len()
                && ch_index >= *cursors.get(cur_index).unwrap()
                && ch_index <= *cursors.get(cur_index).unwrap() + len
            {
                self.hl.push(Highlight::Match)
            } else if ch == '.' && prev_hl.to_owned() == Highlight::Number {
                self.hl.push(Highlight::Number)
            } else if ch.is_digit(10) && (prev_sep || prev_hl.to_owned() == Highlight::Number) {
                self.hl.push(Highlight::Number)
            } else {
                self.hl.push(Highlight::Normal)
            }
        }
    } else {
        let mut char_iter = self.string.chars();
        let mut ch_index = 0;
        let mut prev_hl: Option<Highlight> = None;
        'outer: loop {
            if let Some(mut ch) = char_iter.next() {
                if scs_len != 0 && in_string_quote.is_none() && ch == '/' {
                    if let Some(new_ch) = char_iter.next() {
                        if new_ch == '/' {
                            'inner: loop {
                                self.hl.push(Highlight::Comment);
                                self.hl.push(Highlight::Comment);
                                if let Some(_) = char_iter.next() {
                                    self.hl.push(Highlight::Comment);
                                    // prev_hl = Some(Highlight::Comment);
                                    continue 'inner;
                                } else {
                                    break 'outer;
                                }
                            }
                        } else {
                            ch = new_ch.to_owned();
                        }
                    } else {
                        break 'outer;
                    }
                }
                if let Some(c) = in_string_quote {
                    self.hl.push(Highlight::HlString);
                    prev_hl = Some(Highlight::HlString);
                    ch_index = ch_index + 1;
                    if ch == '\\' && ch_index < self.len {
                        let _ = char_iter.next();
                        self.hl.push(Highlight::HlString);
                        prev_hl = Some(Highlight::HlString);
                        ch_index = ch_index + 1;
                        prev_sep = my_is_seperator(ch);
                        continue;
                    }
                    if c == ch {
                        in_string_quote = None;
                        prev_sep = true;
                    }
                    continue; // not needed for this particular continue
                } else {
                    if ch == '"' || ch == '\'' {
                        in_string_quote = Some(ch);
                        self.hl.push(Highlight::HlString);
                        prev_hl = Some(Highlight::HlString);
                        ch_index = ch_index + 1;
                        prev_sep = my_is_seperator(ch);
                        continue;
                    }
                }
                if let Some(prev_hl_unwrapped) = prev_hl {
                    match prev_hl_unwrapped {
                        Highlight::Number => {
                            if ch == '.' || ch.is_digit(10) {
                                self.hl.push(Highlight::Number);
                                prev_hl = Some(Highlight::Number);
                                // prev_sep = false;
                                ch_index = ch_index + 1;
                                prev_sep = my_is_seperator(ch);
                                continue;
                            } else {
                                self.hl.push(Highlight::Normal);
                                prev_hl = Some(Highlight::Normal);
                                ch_index = ch_index + 1;
                                prev_sep = my_is_seperator(ch);
                                continue;
                            }
                        }
                        _ => {
                            if ch.is_digit(10) && prev_sep {
                                self.hl.push(Highlight::Number);
                                prev_hl = Some(Highlight::Number);
                                // prev_sep = false;
                                ch_index = ch_index + 1;
                                prev_sep = my_is_seperator(ch);
                                continue;
                            } else {
                                self.hl.push(Highlight::Normal);
                                prev_hl = Some(Highlight::Normal);
                                ch_index = ch_index + 1;
                                prev_sep = my_is_seperator(ch);
                                continue;
                            }
                        }
                    }
                } else {
                    if ch.is_digit(10) && prev_sep {
                        self.hl.push(Highlight::Number);
                        prev_hl = Some(Highlight::Number);
                        ch_index = ch_index + 1;
                        prev_sep = my_is_seperator(ch);
                        continue;
                    } else {
                        self.hl.push(Highlight::Normal);
                        prev_hl = Some(Highlight::Normal);
                        ch_index = ch_index + 1;
                        prev_sep = my_is_seperator(ch);
                        continue;
                    }
                }
            } else {
                break;
            }
        }
    }
    // }
}

pub fn render_text_syntax_hl(
    &mut self,
    rowoff: u16,
    screen_pos_y: u16,
    raw_str: &str,
    cur_row: &Row,
    og_x: u16,
    highlight: (Position, Position),
) -> Result<()> {
    let line_num_str_len = 4;
    let mut bg_color;
    let mut cur_row_iter = cur_row.hl_vec().iter();
    let (first_x, first_y) = highlight.0.get_pos();
    let (second_x, second_y) = highlight.1.get_pos();
    let pos_y = screen_pos_y + rowoff;
    let transparent = Color::Rgb {
        r: 155,
        g: 155,
        b: 155,
    };

    if pos_y > first_y && pos_y < second_y {
        let fg_color = Color::White;
        bg_color = transparent;
        self.stdout
            .queue(cursor::MoveTo(
                line_num_str_len + og_x + 0 as u16,
                screen_pos_y,
            ))?
            .queue(SetColors(Colors::new(fg_color, bg_color)))?
            .queue(Print(raw_str))?
            .queue(ResetColor)?;
        return Ok(());
    }

    if pos_y == first_y && pos_y == second_y {
        for (ch_index, ch) in raw_str.chars().enumerate() {
            let cur_hl = cur_row_iter.next().unwrap_or(&Highlight::Normal);
            if ch_index >= first_x.into() && ch_index <= second_x.into() {
                bg_color = transparent;
            } else {
                bg_color = Black;
            }
            self.stdout
                .queue(cursor::MoveTo(
                    line_num_str_len + og_x + ch_index as u16,
                    screen_pos_y,
                ))?
                .queue(SetColors(Colors::new(cur_hl.syntax_to_color(), bg_color)))?
                .queue(Print(ch))?
                .queue(ResetColor)?;
        }

        return Ok(());
    }
    if pos_y == first_y {
        for (ch_index, ch) in raw_str.chars().enumerate() {
            let cur_hl = cur_row_iter.next().unwrap_or(&Highlight::Normal);
            if ch_index >= first_x.into() {
                bg_color = transparent;
            } else {
                bg_color = Black;
            }
            self.stdout
                .queue(cursor::MoveTo(
                    line_num_str_len + og_x + ch_index as u16,
                    screen_pos_y,
                ))?
                .queue(SetColors(Colors::new(cur_hl.syntax_to_color(), bg_color)))?
                .queue(Print(ch))?
                .queue(ResetColor)?;
        }
        return Ok(());
    }

    if pos_y == second_y {
        for (ch_index, ch) in raw_str.chars().enumerate() {
            let cur_hl = cur_row_iter.next().unwrap_or(&Highlight::Normal);
            if ch_index <= second_x.into() {
                bg_color = transparent;
            } else {
                bg_color = Black;
            }
            self.stdout
                .queue(cursor::MoveTo(
                    line_num_str_len + og_x + ch_index as u16,
                    screen_pos_y,
                ))?
                .queue(SetColors(Colors::new(cur_hl.syntax_to_color(), bg_color)))?
                .queue(Print(ch))?
                .queue(ResetColor)?;
        }

        return Ok(());
    }
    return Ok(());
}
