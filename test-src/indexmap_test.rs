use indexmap::IndexMap;

fn main() {
    let mut index_map: IndexMap<usize, &str> = IndexMap::new();
    index_map.insert(34, "emacs");
    index_map.insert(112, "cold");

    let key = index_map.get_index(1);
    println!("key: {}",key.unwrap().1);
}
