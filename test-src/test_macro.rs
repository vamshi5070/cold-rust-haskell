macro_rules! create_vec{
        ($($str:expr),*) => {
            {
                let mut v = Vec::new();
                $(v.push(String::from($str));)*
                    v
            }
        };
    }

fn main() {
    let my_vec = create_vec!("hello", "enacs");
    println!("{:?}", my_vec);
}
