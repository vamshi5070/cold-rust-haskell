This is a text editor which is born out of my frustations with existing text editors.
Cold is how imagined emacs could be if it were written from scratch in today's times.

Main libraries used: crossterm,ratatui

# Configurable, DIY(Do it yourself) editor in haskell (With low level implemented in rust)
- Only rust part is done
- [x] First make gnu nano clone

Now, it is a simple text editor with splits and file selecter(dired like).
-  The main component is: editor.rs(./src/editor.rs)
-  The text editor handler for a given buffer: text_mode.rs(./src/text_mode.rs)
-  The file manager handler for a given buffer: file_system.rs(./src/file_system.rs)
-  Search component: search.rs(./src/search.rs)
-  Minibuffer handler: minibuffer.rs(./src/minibuffer.rs)
-  Individual row is handled by: row.rs(./src/row.rs)
-  Screen(crossterm) handled by: screen.rs(./src/screen.rs)
-  Custom data types : my_lib.rs(./src/my_lib.rs)
-  Multiple displays are handled by: display_window.rs(./src/display_window.rs)
